/**
 * Before insert data action
 */
Like.before.insert((userId, doc) => {
    doc.creator = Meteor.userId();
});

/**
 * Like publish
 */
Meteor.publish('likePublish', function (objectType, objectId) {
    return Like.find({
        objectType: objectType,
        objectId: objectId
    });
});

/**
 * Like publish for owner
 */
Meteor.publish('gameLikePublishForCreator', function () {
    return Like.find({
        objectType: Common.constants.like.type.game,
        creator: this.userId
    });
});

/**
 * Like publish for owner
 */
Meteor.publish('thingLikePublishForCreator', function () {
    return Like.find({
        objectType: Common.constants.like.type.thing,
        creator: this.userId
    });
});