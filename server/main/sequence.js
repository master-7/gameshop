/**
 * Type sequence
 * @type {{robokassaTransaction: string}}
 */
Sequence.type = {
    robokassaTransaction: "robokassaTransaction"
};

/**
 * Get next sequence id in collections
 * @param name
 * @returns {number}
 */
Sequence.getNextSequence = (name) => {
    var ret = Sequence.findAndModify(
        {
            query: { name: name },
            update: { $inc: { seq: 1 } },
            upsert: true
        }
    );
    return ret.seq;
};