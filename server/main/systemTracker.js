/**
 * Event type enum
 * @type {{edit: {profile: number}}}
 */
SystemTracker.type = {
    edit: {
        profile: 100
    },
    error: {
        systemError: 1000,
        userError: 1100
    }
};
/**
 * Event operation status
 * @type {{fail: number, success: number, disallow: number}}
 */
SystemTracker.operationStatus = {
    fail: 0,
    success: 1,
    disallow: 2
};
/**
 * Allow actions rules
 */
SystemTracker.allow({
    insert: (userId) => {
        return false;
    },
    remove: (userId, doc) => {
        return false;
    },
    update: (userId, doc) => {
        return false;
    }
});
/**
 * Before insert data action
 */
SystemTracker.before.insert(function (userId, doc) {
    if(Meteor.user())
        doc.creator = Meteor.userId();
    doc.createdAt = Date.now();
});

Meteor.methods({
    /**
     * Create user error
     * @param error
     * @param type
     * @returns {*}
     */
    "sendError": (error) => {
        SystemTracker.insert({
            type: SystemTracker.type.error.userError,
            status: SystemTracker.operationStatus.fail,
            data: error
        });
    },
    /**
     * Create user explanations
     * @param errorId
     * @param msg
     */
    "userMsg": (errorId, msg) => {
        SystemTracker.update(
            {_id: errorId},
            {
                $set: {
                    "data.userMsg": TagStripper.strip(msg)
                }
            }
        );
    }
});