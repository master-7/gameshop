/**
 * Before insert data action
 */
Ticket.before.insert((userId, doc) => {
    if (!doc.creator)
        doc.creator = Meteor.userId();

    doc.status = Common.constants.ticket.status.created;
});


Ticket.before.update(function (userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    
    if(modifier.$set.status == Common.constants.ticket.status.moderatorAnswer) {
        /**
         * Create new event for current user
         */
        UserTracker.insert({
            type: Common.constants.userTracker.type.ticket.message,
            image: doc.images[0] || null,
            data: {
                _id: doc._id
            },
            creator: doc.creator
        });
    }
});

/**
 * Get ticket for creator
 */
Meteor.publish('ticketForCreator', function (limit, id) {
    let selector = {};
    if(id) {
        selector._id = id;
    }

    let calculatedLimit = Common.constants.loadParties.countItems + limit;

    return Ticket.find(
        Object.assign({},
            selector,
            {
                creator: this.userId
            }
        ),
        {
            sort: {status: -1},
            limit: calculatedLimit
        }
    );
});

/**
 * Get ticket for moderator
 */
Meteor.publish('ticketForModerator', function (limit, selector) {
    let condition = { };
    
    if(selector) {
        if(selector._id)
            condition._id = selector._id;

        if(selector.creator)
            condition.creator = selector.creator;

        if(Number.isInteger(selector.status))
            condition.status = selector.status;

        if(Number.isInteger(selector.category))
            condition.category = selector.category;
    }

    let calculatedLimit = Common.constants.loadParties.countItems + limit;

    if (Common.system.permissions.isModerator(this.userId)) {
        return Ticket.find(
            condition,
            {
                sort: {status: -1},
                limit: calculatedLimit
            }
        );
    }
    return this.ready();
});

/**
 * Public methods ticket model
 */
Meteor.methods({
    /**
     * Create comment for ticket
     * @param id
     * @param msg
     */
    "ticketCreateComment": (id, msg) => {
        check(id, String);
        check(msg, String);

        let ticketStatus = {};

        if(!Common.system.permissions.isModerator(Meteor.userId())) {
            let ticket = Ticket.findOne({_id: id});
            if(ticket.creator != Meteor.userId())
                throw new Meteor.Error(Common.constants.errorCode.http.forbidden,
                    "Not allowed create msg for ticket where you not owner!");
            ticketStatus = {
                status: Common.constants.ticket.status.userAnswer
            };
        }
        else {
            ticketStatus = {
                status: Common.constants.ticket.status.moderatorAnswer
            };
        }

        if (!Meteor.userId())
            throw new Meteor.Error(Common.constants.errorCode.http.unauthorized, "Not allowed unauthorized user!");

        Ticket.update(
            {_id: id},
            {
                $set: ticketStatus,
                $push: {
                    comments: {
                        _id: new Meteor.Collection.ObjectID(),
                        msg: TagStripper.strip(msg),
                        author: Meteor.userId(),
                        createdAt: Date.now()
                    }
                }
            }
        );
    }
});
