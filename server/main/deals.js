Deals.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        targetForSale: {
            type: String,
            index: true,
            optional: true
        },
        moneyTransactionsId: {
            type: String,
            optional: true
        },
        type: {
            type: Number,
            label: "Deal status",
            index: true
        },
        price: {
            type: Number,
            label: "Deal price",
            decimal: true
        },
        status: {
            type: Number,
            label: "Deal status",
            index: true
        },
        seller: {
            type: String,
            index: true
        },
        buyer: {
            type: String,
            index: true
        }
    },
    Common.system.schemas.commonFields
));

Deals.attachSchema(Deals.schema);

/**
 * Before insert data action
 */
Deals.before.insert(function (userId, deal) {
    if(!deal.status)
        deal.status = Common.constants.deal.status.created;
});

/**
 * After insert data action
 */
Deals.after.insert(function (userId, deal) {
    //If created new deal - create money transaction
    if (deal.type == Deals.types.game || deal.type == Deals.types.gameWithMediator) {
        let moneyTransactionsId = MoneyTransactions.insert({
            type: deal.type == Deals.types.game ?
                MoneyTransactions.types.deal.game : Deals.types.gameWithMediator,
            status: MoneyTransactions.statuses.inProcess,
            data: deal,
            sum: deal.price
        });
        
        Deals.update({_id: deal._id}, {$set: {moneyTransactionsId: moneyTransactionsId}});
    }
});

/**
 * Before update data action
 */
Deals.before.update(function (userId, deal, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};

    let successDeal = deal.status == Common.constants.deal.status.inProcess &&
        modifier.$set.status == Common.constants.deal.status.success;

    let failDeal = deal.status == Common.constants.deal.status.inProcess &&
        modifier.$set.status == Common.constants.deal.status.fail;

    if(successDeal) {
        MoneyTransactions.update(
            {
                _id: deal.moneyTransactionsId
            },
            {
                $set: {
                    type: MoneyTransactions.types.deal.gameWithMediator,
                    status: MoneyTransactions.statuses.success,
                    data: deal,
                    sum: deal.price
                }
            }
        );
    }
    if(failDeal) {
        MoneyTransactions.update(
            {
                _id: deal.moneyTransactionsId
            },
            {
                $set: {
                    type: MoneyTransactions.types.deal.gameWithMediator,
                    status: MoneyTransactions.statuses.fail,
                    data: deal,
                    sum: deal.price
                }
            }
        );
    }
});

Meteor.methods({
    /**
     * This method creates deals on server
     * @param gameId
     */
    "payGame": (gameId) => {
        check(gameId, String);

        if (!Meteor.userId())
            throw new Meteor.Error(Common.constants.errorCode.http.unauthorized, "Not allowed for non authorized users!");

        let gameAccount = GameAccount.findOne({_id: gameId, status: Common.constants.game.statuses.isPublic});

        if (!gameAccount)
            throw new Meteor.Error(Common.constants.errorCode.http.badRequest, "Not correct GameAccount!");

        if (Meteor.userId() == gameAccount.creator)
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allowed to buy own game!");

        let moneyAccountCurrentUser = MoneyAccount.findOne({owner: Meteor.userId()});

        let isEnoughMoney = moneyAccountCurrentUser && (moneyAccountCurrentUser.sum >= gameAccount.price);

        if(!isEnoughMoney)
            throw new Meteor.Error(Common.constants.errorCode.http.paymentRequired, "Not enough money!");

        let deal = {
            targetForSale: gameAccount._id,
            type: gameAccount.sellingMode == Common.constants.game.sellingMode.preApproval ?
                Deals.types.game : Deals.types.gameWithMediator,
            seller: gameAccount.creator,
            buyer: Meteor.userId(),
            price: gameAccount.price,
            status: Common.constants.deal.status.inProcess
        };

        Deals.insert(deal);

        /**
         * Send notification for user seller
         */
        UserTracker.insert({
            creator: gameAccount.creator,
            type: Common.constants.userTracker.type.deal.created,
            image: gameAccount.images[0],
            data: {
                _id: gameAccount._id
            }
        });
    }
});

/**
 * PayOutBid for admin publish
 */
Meteor.publish("dealsTrackingForModerator", function (limit, selectors) {
    if (Common.system.permissions.isModerator(this.userId)) {
        let calculatedLimit = Common.constants.loadParties.countItems + limit;
        let condition = {};

        if (selectors) {
            if (selectors._id)
                condition._id = selectors._id;

            if (selectors.targetForSale)
                condition.targetForSale = selectors.targetForSale;

            if (selectors.buyer)
                condition.buyer = selectors.buyer;

            if (selectors.seller)
                condition.seller = selectors.seller;

            if (selectors.status)
                condition.status = selectors.status;
        }

        return Deals.find(
            condition,
            {
                sort: {
                    createdAt: -1
                },
                limit: calculatedLimit
            }
        );
    }
    else {
        return this.ready()
    }
});