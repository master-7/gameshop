Meteor.publish('icons', function(id) {
	let selector = {};
	if(id)
		selector = {
			_id: id
		};
	return Icons.find(selector);
});

Meteor.publish('iconsThumb', function(ids) {
	let selector = {};
	if(ids) {
		selector = {
			originalStore: 'icons',
			originalId: {
				$in: Array.isArray(ids) ? ids : [ids]
			}
		};
	}
	return IconsThumb.find(selector);
});