Images.after.insert((userId, doc) => {
    delete doc.thumbnailBounds;
});

/**
 * Publish all images
 */
Meteor.publish('images', function () {
    return Images.find({});
});

/**
 * Get all images by array _id
 */
Meteor.publish("imagesByIdInArray", function (id) {
    if(id)
        return Images.find({
            _id: {
                $in: id
            }
        });
    return this.ready();
});

Meteor.publish('thumbs', function(ids) {
    return Thumbs.find({
        originalStore: 'images',
        originalId: {
            $in: ids
        }
    });
});

Meteor.publish('thumbsByIds', function(ids) {
    return Thumbs.find({
        _id: {
            $in: ids
        }
    });
});

Meteor.methods({
    /**
     * @param imageId
     * @param gameAccount
     */
    "removeImage": (imageId, gameAccount) => {
        if (gameAccount.images.length > 1) {
            Thumbs.remove({
                "originalId": imageId
            });
            Images.remove({_id: imageId});
        }
        else {
            throw new Meteor.Error(Common.constants.errorCode.http.badRequest, "Not allowed delete last image!");
        }
    }
});
