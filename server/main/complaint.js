/**
 * Before insert data action
 */
Complaint.before.insert(function (userId, doc) {
    doc.status = Common.constants.complaint.status.created;
    doc.creator = Meteor.userId();
});

/**
 * After insert data action
 */
Complaint.after.insert((userId, doc) => {
    let countComplaintOnBlock = 4;
    if(parseInt(doc.objectType) == Common.constants.complaint.objectType.comment) {
        let complaintOnObject = Complaint.find({
            objectType: Common.constants.complaint.objectType.comment,
            objectId: doc.objectId,
            commentId: doc.commentId,
            commentType: doc.commentType,
            type: doc.type
        });
        if(Common.system.countElementOnCollection(complaintOnObject) > countComplaintOnBlock) {
            switch (parseInt(doc.commentType)) {
                case Common.constants.complaint.objectType.commentType.game:
                    GameAccount.update(
                        {
                            _id: doc.objectId,
                            "comments._id": doc.commentId
                        },
                        {
                            $set: {
                                "comments.$.block": true
                            }
                        }
                    );
                    break;
                case Common.constants.complaint.objectType.commentType.thing:
                    GameThing.update(
                        {
                            _id: doc.objectId,
                            "comments._id": doc.commentId
                        },
                        {
                            $set: {
                                "comments.$.block": true
                            }
                        }
                    );
                    break;
            }
        }
    }
    else {
        let complaintOnObject = Complaint.find({
            type: doc.type,
            objectId: doc.objectId,
            objectType: doc.objectType
        });
        /**
         * If count complaint more then 5 block document
         */
        if(Common.system.countElementOnCollection(complaintOnObject) > countComplaintOnBlock) {
            switch (parseInt(doc.objectType)) {
                case Common.constants.complaint.objectType.game:
                    GameAccount.update(
                        {_id: doc.objectId},
                        {$set: {
                            status: Common.constants.game.statuses.onVerification
                        }},
                        {tx: true}
                    );
                    break;
                case Common.constants.complaint.objectType.thing:
                    GameThing.update(
                        {_id: doc.objectId},
                        {
                            $set: {
                                status: Common.constants.thing.statuses.onVerification
                            }
                        },
                        {tx: true}
                    );
                    break;
            }
        }
    }
});

/**
 * Before update data action
 */
Complaint.before.update(function (userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    doc.moderator = Meteor.userId();
});