/**
 * Job on schedule
 */
SyncedCron.add({
    name: 'Fix long money transaction',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 10 min');
    },
    job: function() {
        MoneyTransactions.update(
            {
                status: MoneyTransactions.statuses.inProcess,
                createdAt: {
                    $lt: (Date.now() - 1000 * 60 * 60 * 48)
                }
            },
            {
                $set: {
                    status: MoneyTransactions.statuses.fail
                }
            },
            {
                multi: true
            }
        );
    }
});
SyncedCron.start();