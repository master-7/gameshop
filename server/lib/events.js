/**
 * If user is blocked - logout
 */
Accounts.validateLoginAttempt((data) => {
    if (data.user && data.user.profile) {
        if (data.user.profile.blocked) {
            Meteor.users.update({_id: data.user._id}, {$set: {"services.resume.loginTokens": []}});
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "User is baned");
        }
    }
    return true;
});