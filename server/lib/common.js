/**
 * Common server methods
 */
Common.methods = {
    /**
     * Send text email server method
     * @param to
     * @param from
     * @param subject
     * @param text
     */
    sendTextEmail: function (to, from, subject, text) {
        check([to, from, subject, text], [String]);

        this.unblock();

        Email.send({
            to: to,
            from: from,
            subject: subject,
            text: text
        });
    },
    /**
     * Send HTML email server method
     * @param to
     * @param from
     * @param subject
     * @param templatePath
     * @param params
     */
    sendHtmlEmail: function (to, from, subject, templatePath, params) {
        check([to, from, subject, templatePath], [String]);
        check(params, Object);

        SSR.compileTemplate(
            'htmlEmail',
            Assets.getText('mail-templates/' + templatePath)
        );

        let footer = {
            msg: i18n('footer.msg'),
            link: Meteor.absoluteUrl('/account/edit'),
            msgLink: i18n('footer.linkMsg')
        };

        Email.send({
            to: to,
            from: from,
            subject: subject,
            html: SSR.render("htmlEmail",
                Object.assign(
                    {
                        title: subject,
                        footer: footer
                    },
                    params
                )
            )
        });
    }
};