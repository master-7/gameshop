/**
 * Before insert data action
 */
CategoryKeywords.before.insert(function (userId, doc) {
	doc.creator = Meteor.userId();
});

/**
 * Before update data action
 */
CategoryKeywords.before.update(function (userId, doc, fieldNames, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.editor = Meteor.userId();
});

Meteor.publish('categoryKeywords', function (id) {
	if(id) {
		return CategoryKeywords.find({_id: id})
	}
	return CategoryKeywords.find({});
});

Meteor.publish('categoryKeywordsActive', function (id) {
	if(id) {
		return CategoryKeywords.find({
			_id: id,
			isActive: true
		})
	}
	return CategoryKeywords.find(
		{
			isActive: true
		},
		{
			sort: { name: 1 }
		}
	);
});