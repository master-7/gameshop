/**
 * Template types enum
 */
let templateTypes = {
    html: "html",
    css: "css"
};

/**
 * Get template name with path
 * @param name
 * @param type
 * @returns {string}
 */
let getTemplateName = (name, type = templateTypes.html) => {
    return `seo-templates/${name}.${type}`;
};

/**
 * Assets get text helper
 * @param templateName
 * @param type
 * @returns {any}
 */
let assetsGetText = (templateName, type = templateTypes.html) => {
    return Assets.getText(
        getTemplateName(templateName, type)
    );
};

/**
 * Get image link helper
 * @param image
 * @returns {string}
 */
let getImageLink = (image) => {
    return `ufs/${image.store}/${image._id}/${image.name}`;
};

/**
 * Compile layout template
 */
SSR.compileTemplate('layout',
    assetsGetText('layout')
);

/**
 * Blaze does not allow to render templates with DOCTYPE in it.
 * This is a trick to made it possible
 */
Template.layout.helpers({
    getDocType: function() {
        return "<!DOCTYPE html>";
    }
});

/**
 * All SEO templates
 * @type {string[]}
 */
let templates = [
    'public',
    'game-view',
    'rules',
    'faq',
    'partnership',
    'team',
    'login',
    'registration'
];

/**
 * Compile all templates
 */
for (let index in templates) {
    let templateName = templates[index];
    if(typeof templateName == 'string') {
        SSR.compileTemplate(templateName,
            assetsGetText(templateName)
        );
    }
}

/**
 * Public template helpers
 */
Template.public.helpers({
    additionOfNumber: function (number1, number2) {
        return number1 + number2;
    },
    getMainImageURL: function (images) {
        return Meteor.absoluteUrl(getImageLink(images[0]));
    }
});

/**
 * User cache variables
 * @type {Array}
 */
let authors = [];

/**
 * Game-view template helpers
 */
Template["game-view"].helpers({
    additionOfNumber: function (number1, number2) {
        return number1 + number2;
    },
    chatExist: function (chatObj) {
        if (chatObj)
            return chatObj.length > 0;
        return false;
    },
    getUserFullName: function (userId) {
        if(authors[userId]) {
            return authors[userId].profile.name + " " + authors[userId].profile.surname;
        }
        else {
            authors[userId] = Meteor.users.findOne({
                _id: userId
            });
            return authors[userId].profile.name + " " + authors[userId].profile.surname;
        }
    },
    getUserAvatar: function (userId) {
        if(authors[userId]) {
            return authors[userId].profile.avatar || null;
        }
        else {
            authors[userId] = Meteor.users.findOne({
                _id: userId
            });
            return authors[userId].profile.avatar || null;
        }
    },
    getImageURL: function (image) {
        return Meteor.absoluteUrl(getImageLink(image));
    }
});

/**
 * Get CSS styles
 * @type {any}
 */
let css = assetsGetText('style', templateTypes.css);

/**
 * @param res
 * @param css
 * @param meta
 * @param template
 * @param data
 */
let  renderHTML = (res, css, meta, template, data = {}) => {
    let html = SSR.render('layout', {
        css: css,
        meta: meta,
        template: template,
        data: data
    });
    res.end(html);
};

/**
 * @param req
 * @param name
 * @returns {{keys: *, meta: string}}
 */
let defaultKeysAndMeta = (req, name) => {
    req.url = req.url.replace(/\?_escaped_fragment_=\//, "");

    let seoData = {};

    seoData.keys = `
        <h1>Здесь вы можете купить игровые аккаунты steam, origin, world of tanks и многое другое из большого количества игровых категорий</h1>
        <h2>Покупка и продажа игровых аккаунтов</h2>
    `;
    if(name) {
        seoData.metaTags += `
            <title>Купить ${name} аккаунт, продать игровой аккаунт</title>
            <meta name="description" content="Большой выбор аккаунтов ${name}, только на бирже игровых аккаунтов Salegame">
            <meta name="keywords" content="Аккаунты ${name}, купить ${name}, продать ${name}, купить аккаунт ${name}, продать аккаунт ${name}">
            <link rel="canonical" href="${req.url}">
            <meta property="url" content="${req.url}">
            <meta property="og:url" content="${req.url}">
        `;
    }
    else {
        seoData.metaTags += `
            <title>Купить игровой аккаунт, продать игровой аккаунт</title>
            <meta name="description" content="Большой выбор игровых аккаунтов, только на бирже игровых аккаунтов Salegame">
            <meta name="keywords" content="Игровые аккаунты, купить аккаунт, продать аккаунт, продать игровой аккаунт"> 
            <link rel="canonical" href="${req.url}">
            <meta property="url" content="${req.url}">
            <meta property="og:url" content="${req.url}">
        `;
    }

    return {meta: seoData.metaTags, keys: seoData.keys};
};

/**
 * Route for public
 */
let routePublic = (params, req, res) => {
    let games = GameAccount.find({
        status: Common.constants.game.statuses.isPublic
    });

    let {keys, meta} = defaultKeysAndMeta(req);

    renderHTML(res, css, meta, "public", { games: games, keys: keys });
};

/**
 * @param res
 * @param req
 * @param games
 * @param name
 * @param keywords
 * @param metaTags
 * @param image
 */
let renderPublicWithKeysAndMeta = (res, req, games, name, keywords, metaTags, image) => {
    let {keys, meta} = defaultKeysAndMeta(req, name);

    if (keywords) {
        keys = keywords;
    }

    meta += `
        <meta property="og:image" content="${Meteor.absoluteUrl(getImageLink(image))}">
    `;

    if(metaTags) {
        meta += metaTags;
    }

    renderHTML(res, css, meta, "public", { games: games, keys: keys });
};

/**
 * @param category
 * @param params
 * @param req
 * @param res
 */
let routePublicWithCategory = (category, params, req, res) => {
    let games = GameAccount.find({
        "category._id": category,
        status: Common.constants.game.statuses.isPublic
    });

    let seoData = CategoryKeywords.findOne({
        _id: category
    });

    renderPublicWithKeysAndMeta(res, req, games, seoData.name, seoData.keywords, seoData.metaTags, seoData.image);
};

/**
 * @param type
 * @param params
 * @param req
 * @param res
 */
let routePublicWithType = (type, params, req, res) => {
    let games = GameAccount.find({
        type: type,
        status: Common.constants.game.statuses.isPublic
    });

    let seoData = GameAccountType.findOne({
        _id: type
    });

    renderPublicWithKeysAndMeta(res, req, games, seoData.name, seoData.keywords, seoData.metaTags, seoData.image);
};

/**
 * filtering only HTTP request sent by seo via AJAX-Crawling
 * this is meteorhacks:pickers coolest feature
 */
var seoPicker = Picker.filter(function(req, res) {
    let isSearchBot = /facebookexternalhit|facebot|facebook|Twitterbot|tinterest|google|google.*snippet|Googlebot|vk.com|vkShare|yandex|yandexbot|skype/
        .test(req.headers['user-agent']) ||
        /_escaped_fragment_/.test(req.url);

    return isSearchBot;
});

/**
 * Root route
 */
seoPicker.route('/', function(params, req, res) {
    routePublic(params, req, res)
});

/**
 * Public page route
 */
seoPicker.route('/public', function(params, req, res) {
    for (let index in params.query) {
        if(typeof params.query[index] !== 'object')
            params.query[index] = params.query[index].replace(/\?_escaped_fragment_=\//, "");
    }
    if(params.query && params.query.category) {
        routePublicWithCategory(params.query.category, params, req, res);
    }
    if(params.query && params.query.type) {
        routePublicWithType(params.query.type, params, req, res);
    }
    routePublic(params, req, res)
});

/**
 * Route for view game
 */
seoPicker.route('/game/view/:id', function(params, req, res) {
    let game = GameAccount.findOne({
        _id: params.id,
        status: Common.constants.game.statuses.isPublic
    });

    let creator = Meteor.users.findOne({_id: game.creator});

    let meta = `
        <title>${game.name}</title>
        
        <meta name="description" content="Просмотр игрового аккаунта ${game.name} из категории ${game.category.name}">
        <meta name="keywords" content="покупка игровых аккаунтов, продажа игровых аккаунтов">
        <meta name="author" content="${creator.profile.name || ''} ${creator.profile.surname || ''}">
        <link rel="canonical" href="${Meteor.absoluteUrl("game/view/" + game._id)}">
        <meta property="og:title" content="Игровой аккаунт ${game.name}">
        <meta property="og:description" content="${game.description}">
        <meta property="og:site_name" content="SaleGame">
        <meta property="og:type" content="article">
        <meta property="og:locale" content="ru_ru">
        <meta property="og:url" content="${Meteor.absoluteUrl("game/view/" + game._id)}">`;

    for (let index in game.images) {
        let image = game.images[index];
        if(typeof image == "object") {
            meta += `
                <meta property="image" content="${Meteor.absoluteUrl(getImageLink(image))}">
                <meta property="og:image" content="${Meteor.absoluteUrl(getImageLink(image))}">
            `;
        }
    }

    renderHTML(res, css, meta, "game-view", { game: game });
});

/**
 * Route for rules
 */
seoPicker.route('/rules', function(params, req, res) {
    let meta = `
        <title>Правила сайта</title>
        
        <meta name="description" content="Правила пользования сайтом salegame.net">
        <meta name="keywords" content="правила пользования,salegame.net">
        <link rel="canonical" href="${Meteor.absoluteUrl("rules")}">
        <meta property="og:title" content="Правила пользования сайтом salegame.net">
        <meta property="og:description" content="Как пользоваться salegame.net, информация для пользователей">
        <meta property="og:site_name" content="SaleGame">
        <meta property="og:type" content="article">
        <meta property="og:locale" content="ru_ru">
        <meta property="og:url" content="${Meteor.absoluteUrl("rules")}">
        <meta property="og:image" content="${Meteor.absoluteUrl("rulse-logo.png")}">`;

    renderHTML(res, css, meta, "rules");
});

/**
 * Route for faq
 */
seoPicker.route('/faq', function(params, req, res) {
    let meta = `
        <title>Часто задаваемые вопросы</title>
        
        <meta name="description" content="Часто задаваемые вопросы">
        <meta name="keywords" content="часто задаваемые вопросы,salegame.net">
        <link rel="canonical" href="${Meteor.absoluteUrl("faq")}">
        <meta property="og:title" content="Ответы на часто задаваемые вопросы">
        <meta property="og:description" content="Как пользоваться salegame.net, инструкции, видеогайды">
        <meta property="og:site_name" content="SaleGame">
        <meta property="og:type" content="article">
        <meta property="og:locale" content="ru_ru">
        <meta property="og:url" content="${Meteor.absoluteUrl("faq")}">
        <meta property="og:image" content="${Meteor.absoluteUrl("faq.png")}">`;

    renderHTML(res, css, meta, "faq");
});

/**
 * Route for partnership
 */
seoPicker.route('/partnership', function(params, req, res) {
    let ticketLink = Meteor.absoluteUrl('ticket');

    let meta = `
        <title>Партнерская программа</title>
        
        <meta name="description" content="Как стать партнером salegame.net">
        <meta name="keywords" content="партнеская программа,salegame.net">
        <link rel="canonical" href="${Meteor.absoluteUrl("partnership")}">
        <meta property="og:title" content="Партнерская программа">
        <meta property="og:description" content="Как стать партнером salegame.net">
        <meta property="og:site_name" content="SaleGame">
        <meta property="og:type" content="article">
        <meta property="og:locale" content="ru_ru">
        <meta property="og:url" content="${Meteor.absoluteUrl("partnership")}">
        <meta property="og:image" content="${Meteor.absoluteUrl("partnership.png")}">`;

    renderHTML(res, css, "partnership", {
        ticketLink: ticketLink
    });
});

/**
 * Route for team
 */
seoPicker.route('/team', function(params, req, res) {
    let html = SSR.render('layout', {
        css: css,
        template: "team",
        data: {}
    });

    res.end(html);
});

/**
 * Route for login
 */
seoPicker.route('/login', function(params, req, res) {
    let html = SSR.render('layout', {
        css: css,
        template: "login",
        data: {}
    });

    res.end(html);
});

/**
 * Route for registration
 */
seoPicker.route('/registration', function(params, req, res) {
    let html = SSR.render('layout', {
        css: css,
        template: "registration",
        data: {}
    });

    res.end(html);
});