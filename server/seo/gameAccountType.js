/**
 * Before insert data action
 */
GameAccountType.before.insert(function (userId, doc) {
	doc.creator = Meteor.userId();
});

/**
 * Before update data action
 */
GameAccountType.before.update(function (userId, doc, fieldNames, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.editor = Meteor.userId();
});

Meteor.publish('gameAccountType', function (id, name) {
	let selector = {};

	if(id) {
		selector._id = id;
	}
	if(name) {
		selector.name = new RegExp(name, 'i');
	}

	return GameAccountType.find(
		selector,
		{
			sort: { name: 1 }
		}
	);
});