sitemaps.add('/sitemap.xml', function() {

    let content = [
        { page: '/public', lastmod: new Date() }
    ];

    /**
     * All game categories
     */
    let gameCategories = CategoryKeywords.find({isActive: true}).fetch();
    for (let index in gameCategories) {
        let category = gameCategories[index];

        if(category._id)
            content.push(
                { page: '/public?category=' + category._id, lastmod: new Date() }
            );
    }

    /**
     * All game types
     */
    let gameTypes = GameAccountType.find({isActive: true}).fetch();
    for (let index in gameTypes) {
        let type = gameTypes[index];

        if(type._id)
            content.push(
                { page: '/public?type=' + type._id, lastmod: new Date() }
            );
    }

    /**
     * Public gameAccount view pages generate
     * @type {any}
     */
    let games = GameAccount.find({
        status: Common.constants.game.statuses.isPublic
    }).fetch();

    for (let index in games) {
        let game = games[index];
        if(game._id && typeof game._id == "string")
            content.push(
                { page: '/game/view/' + game._id, lastmod: game.modifiedAt }
            );
    }

    content.push(
        { page: '/rules', lastmod: new Date() },
        { page: '/faq', lastmod: new Date() },
        { page: '/partnership', lastmod: new Date() },
        { page: '/team', lastmod: new Date() },
        { page: '/login', lastmod: new Date() },
        { page: '/registration', lastmod: new Date() }
    );

    return content;
});