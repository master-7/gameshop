/**
 * Game public account collections.
 * Returns only common account data
 */
Meteor.publish('gameAccountPublic', function (selector, limit, sort) {
    selector = selector || {};
    
    let condition = {};
    
    if(selector._id) {
        condition._id = selector._id;
    }
    
    if(selector.category) {
        condition["category._id"] = selector.category;
    }

    if(selector.type) {
        condition.type = selector.type;
    }

    for (let index in sort) {
        if (sort[index] == 0)
            delete sort[index];
    }

    /**
     * Ads type order by priority
     */
    sort = Object.assign({adsType: -1, sellingMode: -1, createdAt: -1});
    
    let calculatedLimit = Common.constants.loadParties.countItems + limit || Common.constants.loadParties.countItems;
    
    return GameAccount.find(
        Object.assign(
            condition,
            {
                status: Common.constants.game.statuses.isPublic
            }
        ),
        Object.assign({},
            { sort: sort },
            {
                fields: {
                    gameAccount: 0,
                    dependentAccount: 0,
                    chatSellingSellerManager: 0,
                    chatSellingBuyerManager: 0
                },
                limit: calculatedLimit
            }
        )
    );
});

/**
 * Game public account collections.
 * Returns only common account data
 */
Meteor.publish('gameAccountsByLikeObjArray', function (likeObj, limit) {
    let gameAccountIdArray = [];
    for (let index in likeObj) {
        let element = likeObj[index];

        if (element.objectId)
            gameAccountIdArray.push(element.objectId)
    }
    
    let calculatedLimit = Common.constants.loadParties.countItems + limit || Common.constants.loadParties.countItems;
    
    if (gameAccountIdArray)
        return GameAccount.find(
            {
                _id: {
                    $in: gameAccountIdArray
                },
                status: Common.constants.game.statuses.isPublic
            },
            {
                limit: calculatedLimit
            }
        );
    return this.ready();
});

/**
 * Game accounts that current user sell
 * Return all field collection
 */
Meteor.publish('userSales', function (selector, limit) {
    selector = selector || {};

    let condition = {};

    if(selector._id) {
        condition._id = selector._id;
    }

    let calculatedLimit = Common.constants.loadParties.countItems + limit || Common.constants.loadParties.countItems;

    return GameAccount.find(
        Object.assign(
            condition,
            {
                status: {
                    $in: [
                        Common.constants.game.statuses.isPublic,
                        Common.constants.game.statuses.selling
                    ]
                },
                creator: this.userId
            }
        ),
        {
            limit: calculatedLimit,
            fields: {gameAccount: 0, dependentAccount: 0, chatSellingBuyerManager: 0}
        }
    );
});

/**
 * Game accounts sold current user
 */
Meteor.publish('userSold', function () {
    return GameAccount.find(
        Object.assign(
            {
                status: {
                    $in: [
                        Common.constants.game.statuses.sold
                    ]
                },
                creator: this.userId
            }
        ),
        {fields: {gameAccount: 0, dependentAccount: 0, chatSellingBuyerManager: 0}}
    );
});

/**
 * Return the accounts where current user buyer
 */
Meteor.publish('userPurchases', function (selector, limit) {
    let condition = {
        buyer: this.userId
    };

    if(selector && selector._id) {
        condition._id = selector._id;
    }

    let calculatedLimit = Common.constants.loadParties.countItems + limit || Common.constants.loadParties.countItems;

    return GameAccount.find(
        condition,
        {
            sort: {createdAt: -1},
            limit: calculatedLimit,
            fields: {chatSellingSellerManager: 0}
        }
    );
});

/**
 * Return game account in status onVerification for user in permissions "moderator"
 */
Meteor.publish('gameAccountOnVerification', function () {
    if (Common.system.permissions.isModerator(this.userId))
        return GameAccount.find({
            status: Common.constants.game.statuses.onVerification
        });
    return this.ready();
});

/**
 * Publish gameAccount for user moneyAdmin
 */
Meteor.publish('gameAccountForMoneyAdmin', function (id) {
    if (Common.system.permissions.isMoneyAdmin(this.userId)) {
        if(id) {
            return GameAccount.find({_id: id})
        }
        return GameAccount.find({});
    }
    return this.ready();
});

/**
 * Publish gameAccount for user moneyAdmin
 */
Meteor.publish('gameAccountForModerator', function (id, limit) {
    if (Common.system.permissions.isModerator(this.userId)) {
        let calculatedLimit = Common.constants.loadParties.countItems + limit;
        if(id) {
            return GameAccount.find({_id: id})
        }
        return GameAccount.find({}, {limit: calculatedLimit});
    }
    return this.ready();
});

/**
 * Publish gameAccount on status "selling" for moneyAdmin
 */
Meteor.publish('gameAccountDealForModerator', function (id, onlyMy, onlyFree) {
    if (Common.system.permissions.isModerator(this.userId)) {
        let condition = {};
        if(id) {
            condition._id = id;
        }
        if(onlyFree) {
            condition.moderator = null;
        }
        else
            if(onlyMy) {
                condition.moderator = this.userId;
            }
        condition.status = Common.constants.game.statuses.selling;
        
        return GameAccount.find(condition);
    }
    return this.ready();
});

/**
 * Return game account in status onVerification for user in permissions "moderator"
 */
Meteor.publish('gameAccountOnVerificationForCreator', function () {
    return GameAccount.find(
        {
            status: {
                $in: [
                    Common.constants.game.statuses.onVerification,
                    Common.constants.game.statuses.moderatorIsWorking,
                    Common.constants.game.statuses.verificationFail
                ]
            },
            creator: this.userId
        },
        {fields: {gameAccount: 0, dependentAccount: 0, chatSellingSellerManager: 0, chatSellingBuyerManager: 0}}
    );
});
/**
 * Return game account in status moderatorIsWorking for current user in permissions "moderator"
 */
Meteor.publish('gameAccountModeratorIsWorking', function () {
    if (Common.system.permissions.isModerator(this.userId))
        return GameAccount.find({
            status: {
                $in: [
                    Common.constants.game.statuses.onVerification,
                    Common.constants.game.statuses.moderatorIsWorking
                ]
            },
            moderator: this.userId
        });
    return this.ready();
});
/**
 * Game account for edit after return moderator
 */
Meteor.publish("gameAccountEditAfterModeratorReturn", function (gameId) {
    return GameAccount.find({
        _id: gameId,
        status: Common.constants.game.statuses.verificationFail,
        creator: this.userId
    });
});

/**
 * User game accounts
 */
Meteor.publish('userGames', function (userId) {
    return GameAccount.find(
        {creator: userId},
        {
            fields: {
                gameAccount: 0,
                dependentAccount: 0,
                chatSellingSellerManager: 0,
                chatSellingBuyerManager: 0
            }
        }
    );
});

/**
 * User game accounts
 */
Meteor.publish('returnedGames', function (gameId) {
    return GameAccount.find(Object.assign({},
            gameId ? {
                _id: gameId
            } : {},
            {
                status: Common.constants.game.statuses.gameReturned,
                creator: this.userId
            }
        )
    );
});
