/**
 * Before insert data action
 */
GameAccount.before.insert((userId, account) => {
    if(!account.adsType)
        account.adsType = Common.constants.game.adsType.free;
    
    if(account.adsType != Common.constants.game.adsType.free) {
        let moneyAccount = MoneyAccount.findOne({owner: Meteor.userId()});
        let sum = 0;
        switch (account.adsType) {
            case Common.constants.game.adsType.color:
                if(moneyAccount.sum < Common.constants.payment.payAd.color)
                    throw new Meteor.Error(Common.constants.errorCode.http.paymentRequired, "Not enough money!");
                sum = Common.constants.payment.payAd.color;
                break;
            case Common.constants.game.adsType.top:
                if(moneyAccount.sum < Common.constants.payment.payAd.top)
                    throw new Meteor.Error(Common.constants.errorCode.http.paymentRequired, "Not enough money!");
                sum = Common.constants.payment.payAd.top;
                break;
        }
        MoneyTransactions.insert({
            type: MoneyTransactions.types.debit.gamePayAds,
            status: MoneyTransactions.statuses.success,
            data: {
                moneyAccountId: moneyAccount._id,
                accountId: account._id
            },
            sum: sum
        });
    }

    account.name = TagStripper.strip(account.name);
    account.description = TagStripper.strip(account.description);
    account.commission = Common.helpers.calcCommission(account.price);

    if(account.sellingMode == Common.constants.game.sellingMode.preApproval) {
        account.status = Common.constants.game.statuses.onVerification;

        /**
         * Create new event for current user
         */
        UserTracker.insert({
            type: Common.constants.userTracker.type.moderation.createdGame,
            image: account.images[0],
            data: {
                _id: account._id
            }
        });
    }
    else
        account.status = Common.constants.game.statuses.isPublic;

    if (!account.creator)
        account.creator = Meteor.userId();
});

/**
 * Before update data action
 */
GameAccount.before.update(function (userId, account, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};

    if(modifier.$set.price)
        modifier.$set.commission = Common.helpers.calcCommission(modifier.$set.price);

    if(modifier.$set) {
        if(modifier.$set.name)
            modifier.$set.name = TagStripper.strip(modifier.$set.name);
        if(modifier.$set.description)
            modifier.$set.description = TagStripper.strip(modifier.$set.description);
    }

    if(modifier.$set.status)
        switch (parseInt(modifier.$set.status)) {
            case Common.constants.game.statuses.isPublic:
                UserTracker.insert({
                    type: Common.constants.userTracker.type.moderation.success,
                    image: account.images[0],
                    data: {
                        _id: account._id
                    },
                    creator: account.creator
                });
                break;
            case Common.constants.game.statuses.verificationFail:
                UserTracker.insert({
                    type: Common.constants.userTracker.type.moderation.returnGame,
                    image: account.images[0],
                    data: {
                        _id: account._id
                    },
                    creator: account.creator
                });
                break;
            case Common.constants.game.statuses.isBlocked:
                UserTracker.insert({
                    type: Common.constants.userTracker.type.moderation.fail,
                    image: account.images[0],
                    data: {
                        _id: account._id
                    },
                    creator: account.creator
                });
                break;
        }
});

/**
 * After update data action
 */
GameAccount.after.update(function (userId, account, fieldNames, modifier, options) {
    
    let isNewMessage = modifier.$push && modifier.$push.moderatorComments
        && modifier.$push.moderatorComments.author != account.creator;
    if(isNewMessage) {
        UserTracker.insert({
            type: Common.constants.userTracker.type.moderation.message,
            image: account.images[0],
            creator: account.creator,
            data: {
                _id: account._id,
                commentId: modifier.$push.moderatorComments._id
            }
        });
    }
    
    /**
     * Send notification deals participant
     */
    if(modifier.$push && modifier.$push.chatSelling) {
        if(account.creator != modifier.$push.chatSelling.author)
            UserTracker.insert({
                type: Common.constants.userTracker.type.deal.message,
                image: account.images[0],
                data: {
                    _id: account._id,
                    creator: account.creator,
                    buyer: account.buyer,
                    moderator: account.moderator,
                    commentId: modifier.$push.chatSelling._id
                },
                creator: account.creator
            });
        if(account.buyer != modifier.$push.chatSelling.author)
            UserTracker.insert({
                type: Common.constants.userTracker.type.deal.message,
                image: account.images[0],
                data: {
                    _id: account._id,
                    creator: account.creator,
                    buyer: account.buyer,
                    moderator: account.moderator,
                    commentId: modifier.$push.chatSelling._id
                },
                creator: account.buyer
            });
        if(account.moderator && account.moderator != modifier.$push.chatSelling.author)
            UserTracker.insert({
                type: Common.constants.userTracker.type.deal.message,
                image: account.images[0],
                data: {
                    _id: account._id,
                    creator: account.creator,
                    buyer: account.buyer,
                    moderator: account.moderator,
                    commentId: modifier.$push.chatSelling._id
                },
                creator: account.moderator
            });
    }
});