/**
 * Game icon collections
 */
Meteor.publish('gameIconPublic', function () {
    return GameIcon.find({});
});