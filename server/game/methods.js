/**
 * Change gameAccount status for moderator
 * @param id
 * @param status
 */
let changeGameAccountStatusForModerator = (id, status) => {
    check(id, String);

    if(!Meteor.userId())
        throw new Meteor.Error(
            Common.constants.errorCode.http.unauthorized,
            "Not allowed unauthorized user!"
        );

    if(!Common.system.permissions.isModerator(Meteor.userId()))
        throw new Meteor.Error(
            Common.constants.errorCode.http.forbidden,
            "Not allowed user without permissions moderator!"
        );

    GameAccount.update(
        {_id: id},
        {
            $set: {
                status: status,
                moderator: Meteor.userId()
            }
        },
        {
            tx: true
        }
    );
};

/**
 * Chat sales common function
 * @param id
 * @param msg
 * @param key
 */
let chatSales = (id, msg, key) => {
    check(id, String);
    check(msg, String);

    if(!Meteor.userId())
        throw new Meteor.Error(Common.constants.errorCode.http.unauthorized, "Not allowed unauthorized user!");

    /**
     * If moderator create the message - set the moderator on the GameAccount
     */
    if(Common.system.permissions.isModerator(Meteor.userId())) {
        GameAccount.update(
            {_id: id},
            {
                $set: {
                    moderator: Meteor.userId()
                }
            }
        );
    }

    let data = {
        $push: {}
    };

    data.$push[key] = {
        _id: new Meteor.Collection.ObjectID(),
        msg: TagStripper.strip(msg),
        author: Meteor.userId(),
        createdAt: Date.now()
    };

    GameAccount.update(
        {_id: id},
        data
    );
};

/**
 * Public methods GameAccount model
 */
Meteor.methods({
    /**
     * Change account status for moderator
     * @param id
     * @param status
     */
    "changeGameAccountStatusForModerator": (id, status) => {
        check(id, String);

        if(!Meteor.userId())
            throw new Meteor.Error(
                Common.constants.errorCode.http.unauthorized,
                "Not allowed unauthorized user!"
            );

        if(!Common.system.permissions.isModerator(Meteor.userId()))
            throw new Meteor.Error(
                Common.constants.errorCode.http.forbidden,
                "Not allowed user without permissions moderator!"
            );

        GameAccount.update(
            {_id: id},
            {
                $set: {
                    status: status,
                    moderator: Meteor.userId()
                }
            },
            {
                tx: true
            }
        );
    },
    /**
     * Block gameAccount and avatar
     * @param id
     */
    "moderationBlockGameAccountAndUser": (id) => {
        changeGameAccountStatusForModerator(id, Common.constants.game.statuses.isBlocked);
        let gameAccount = GameAccount.findOne({_id: id});
        Meteor.users.update({_id: gameAccount.creator}, {
                $set: {
                    "profile.blocked": true
                }
            },
            (error, result) => {
                if(!error) {
                    //Logout user
                    Meteor.users.update({_id: gameAccount.creator}, {$set: { "services.resume.loginTokens" : [] }});
                }
            }
        );
    },
    /**
     * Edit game account for user creator
     * @param gameAccount
     */
    "editGameAccountAfterModerate": (gameAccount) => {
        if(!Meteor.userId())
            throw new Meteor.Error(Common.constants.errorCode.http.unauthorized, "Not allowed unauthorized user!");

        if(Meteor.userId() != gameAccount.creator)
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allowed change game account another user!");

        if(gameAccount.status != Common.constants.game.statuses.verificationFail)
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden,
                "Not allowed change game account if moderator is working!");

        check(gameAccount, GameAccount.simpleSchema());

        GameAccount.update(
            {_id: gameAccount._id},
            {
                $set: {
                    name: gameAccount.name,
                    description: gameAccount.description,
                    category: gameAccount.category,
                    type: gameAccount.type,
                    price: gameAccount.price,
                    videos: gameAccount.videos,
                    images: gameAccount.images,
                    "gameAccount.login": gameAccount.login,
                    "gameAccount.pass": gameAccount.pass,
                    "dependentAccount.login": gameAccount.login,
                    "dependentAccount.pass": gameAccount.pass,
                    status: Common.constants.game.statuses.onVerification
                }
            }
        );
    },
    /**
     * Create comment for GameAccount
     * @param id
     * @param msg
     */
    "gameAccountCreateComment": (id, msg) => {
        check(id, String);
        check(msg, String);

        if(!Meteor.userId())
            throw new Meteor.Error(Common.constants.errorCode.http.unauthorized, "Not allowed unauthorized user!");

        GameAccount.update(
            {_id: id},
            {
                $push: {
                    comments: {
                        _id: new Meteor.Collection.ObjectID(),
                        msg: TagStripper.strip(msg),
                        author: Meteor.userId(),
                        createdAt: Date.now()
                    }
                }
            }
        );
    },
    /**
     * Create comment for GameAccount on status selling
     * @param id
     * @param msg
     */
    "gameAccountSalesCreateComment": (id, msg) => {
        chatSales(id, msg, "chatSelling");
    },
    /**
     * Create comment for GameAccount on status selling
     * @param id
     * @param msg
     */
    "gameAccountSalesBuyerManager": (id, msg) => {
        chatSales(id, msg, "chatSellingBuyerManager");
    },
    /**
     * Create comment for GameAccount on status selling
     * @param id
     * @param msg
     */
    "gameAccountSalesSellerManager": (id, msg) => {
        chatSales(id, msg, "chatSellingSellerManager");
    },
    /**
     * Create comment for GameAccount
     * @param id
     * @param msg
     */
    "gameAccountCreateModeratorComment": (id, msg) => {
        check(id, String);
        check(msg, String);

        if(!Meteor.userId())
            throw new Meteor.Error(Common.constants.errorCode.http.unauthorized, "Not allowed unauthorized user!");

        let checkGameAccount = GameAccount.findOne({
            _id: id,
            $or: [
                {
                    status: Common.constants.game.statuses.moderatorIsWorking,
                    moderator: Meteor.userId()
                },
                {
                    creator: Meteor.userId()
                }
            ]
        });

        if(!checkGameAccount)
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allowed!");

        GameAccount.update(
            { _id: id },
            {
                $push: {
                    moderatorComments: {
                        _id: new Meteor.Collection.ObjectID(),
                        msg: TagStripper.strip(msg),
                        author: Meteor.userId(),
                        createdAt: Date.now()
                    }
                }
            }
        );
    },
    /**
     * Change game price for creator
     * @param gameId
     * @param newPrice
     */
    "changeGamePriceForCreator": (gameId, newPrice) => {
        check(gameId, String);
        check(newPrice, Number);

        let gameAccount = GameAccount.findOne({_id: gameId});

        let isCreator = gameAccount.creator == Meteor.userId();

        if(!isCreator)
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allowed to return a game where you not owner!");

        GameAccount.update(
            {
                _id: gameId
            },
            {
                $set: {
                    price: newPrice
                }
            }
        );
    },
	/**
     * Delete game account for creator
     * @param gameId
     */
    "deleteGameAccountForCreator": (gameId) => {
        check(gameId, String);

        let gameAccount = GameAccount.findOne({_id: gameId});

        let isCreatorAndSellingModeToMediate = gameAccount.creator == Meteor.userId() && 
            gameAccount.sellingMode == Common.constants.game.sellingMode.toMediate;

        if(!isCreatorAndSellingModeToMediate)
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allowed to return a game where you not owner or selling mode with not mediate!");

        GameAccount.remove({
            _id: gameId
        });
    },
    /**
     * Return game for owner
     */
    "returnGameAccountForCreator": (gameId) => {
        check(gameId, String);

        let gameAccount = GameAccount.findOne({_id: gameId});
        
        let isCreator = gameAccount.creator == Meteor.userId();
        
        if(!isCreator)
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allowed to return a game where you not owner!");
        
        let weekPassed = ((Date.now() - gameAccount.createdAt) / (1000 * 60 * 60 * 24)) > 7;

        if(weekPassed) {
            GameAccount.update(
                {
                    _id: gameId
                }, 
                {
                    $set: {
                        status: Common.constants.game.statuses.gameReturned
                    }
                }
            )
        }
        else {
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allowed to return a game if not a week passed!");
        }
    }
});