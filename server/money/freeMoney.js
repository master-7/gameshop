FreeMoney.before.insert(function (userId, doc) {
    doc.status = FreeMoney.status.active;
});

/**
 * Before update data action
 */
FreeMoney.before.update(function (userId, account, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
});

/**
 * Publish only the user is admin
 */
Meteor.publish('freeMoney', function(limit, condition) {
    if (Common.system.permissions.isAdmin(this.userId)) {
        let calculatedLimit = Common.constants.loadParties.countItems + limit;

        let selector = {};

        if(Number.isInteger(condition.status)) {
            selector.status = condition.status;
        }

        if(condition.transactionId) {
            selector.transactionId = condition.transactionId;
        }

        return FreeMoney.find(selector, {limit: calculatedLimit});
    }
    return this.ready();
});