/**
 * YandexMoney payment config
 * @returns {{account: string, clientId: string, clientSecret: string, redirectURI: string}}
 */
var yandexMoneyConfig = Common.configs.payIn.yandex;

/**
 * Publish payment system methods
 */
Meteor.methods({
    /**
     * YandexMoney get access token
     * @returns {*|Object}
     */
    "getYandexMoneyClientCode": function getYandexMoneyClientCode() {
        let yandexMoneySdk = Meteor.npmRequire("yandex-money-sdk");

        let data = Async.runSync(function (done) {
            let url = yandexMoneySdk.Wallet.buildObtainTokenUrl(
                yandexMoneyConfig.clientId,
                yandexMoneyConfig.redirectURI,
                yandexMoneyConfig.scope
            );
            done(null, url);
        });
        return data.result;
    },
    /**
     * Get access token for pay action
     * @param code
     * @returns {*|Object}
     */
    "getYandexMoneyAccessToken": function getYandexMoneyAccessToken(code) {
        //Bug SDK to get access_token - create API request
        //Run with sync mode
        var syncPost = Meteor.wrapAsync(HTTP.post);

        let result = syncPost("https://sp-money.yandex.ru/oauth/token", {
            data: {
                code: code,
                client_id: yandexMoneyConfig.clientId,
                grant_type: "authorization_code",
                redirect_uri: yandexMoneyConfig.redirectURI,
                client_secret: yandexMoneyConfig.clientSecret
            }
        });

        if (result.data.access_token) {
            Meteor.users.update(
                {_id: Meteor.userId()},
                {
                    $set: {
                        "services.payment": {
                            yandex: {
                                access_token: result.data.access_token
                            }
                        }
                    }
                }
            );
            return true;
        }
        else
            return false;
    },
    /**
     * Yandex money payin method
     * @param options
     */
    "yandexMoneyPayin": (options) => {
        options.pattern_id = "p2p";
        options.to = yandexMoneyConfig.account;
        options.comment = "SaleGame.net";

        /**
         * Get transactionsId and set as a label
         */
        options.label = MoneyTransactions.insert({
            sum: options.amount_due,
            type: MoneyTransactions.types.payIn.yandex,
            status: MoneyTransactions.statuses.createdRequest,
            data: options
        });

        let yandexMoneySdk = Meteor.npmRequire("yandex-money-sdk");

        let data = Async.runSync(function (done) {
            /**
             * Set transactions status and data
             * Throw process errors
             * @param status
             * @param data
             * @param msg
             */
            let setOperationStatus = (status, data, msg) => {
                done(null, {
                    _id: options.label,
                    status: status,
                    data: data,
                    msg: msg
                });
            };

            let accessToken = Meteor.user().services.payment.yandex.access_token;

            let api = new yandexMoneySdk.Wallet(accessToken);
            /**
             * Create api wallet
             */
            api.requestPayment(options, (err, data) => {
                if (err) {
                    setOperationStatus(MoneyTransactions.statuses.fail, err,
                        "Error on yandexMoneySdk.requestPayment!");
                    return;
                }
                if (data && data.status !== "success") {
                    setOperationStatus(MoneyTransactions.statuses.fail, data,
                        "Error on yandexMoneySdk.requestPayment!");
                    return;
                }
                let request_id = data.request_id;
                api.processPayment({
                    "request_id": request_id
                }, function processComplete(err, data) {
                    if (err) {
                        setOperationStatus(MoneyTransactions.statuses.fail, err,
                            "Error on yandexMoneySdk.processPayment!");
                        return;
                    }
                    setOperationStatus(MoneyTransactions.statuses.success, data);
                });
            });
        });

        /**
         * Update transaction
         */
        MoneyTransactions.update(
            {_id: options.label},
            {
                $set: {
                    sum: options.amount_due,
                    type: MoneyTransactions.types.payIn.yandex,
                    status: data.result.status,
                    result: data.result,
                    data: options
                }
            },
            {upsert: true}
        );

        if (data.result.msg)
            throw new Meteor.Error(500, data.result.msg, data.result.data);

        return data.result;
    }
});