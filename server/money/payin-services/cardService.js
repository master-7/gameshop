/**
 * YandexMoney payment config
 * @returns {{account: string, clientId: string, clientSecret: string, redirectURI: string}}
 */
var yandexMoneyConfig = Common.configs.payIn.yandex;

/**
 * Change pay in transaction status
 * @param transactionId
 * @param status
 */
var changePayStatus = (transactionId, status) => {
    let transaction = MoneyTransactions.findOne({_id: transactionId});

    let checkStatus = transaction.status == MoneyTransactions.statuses.inProcess;

    if(checkStatus)
        MoneyTransactions.update(
            {_id: transactionId},
            {
                $set: {
                    status: status
                }
            }
        );
    else
        throw new Meteor.Error(403,
            "Not allow change transaction for status not equals 'inProcess'!",
            transactionId
        );
};
/**
 * Publish payment system methods
 */
Meteor.methods({
    /**
     * Card payIn method
     * @param sum
     */
    "cardPay": (sum) => {
        let options = {};

        options.amount = sum;
        options.pattern_id = "p2p";
        options.to = yandexMoneyConfig.account;

        let sumWithCommission = sum - sum * yandexMoneyConfig.cardCommission;

        /**
         * Get transactionsId
         */
        let transactionId = MoneyTransactions.insert({
            sum: sumWithCommission,
            type: MoneyTransactions.types.payIn.card,
            status: MoneyTransactions.statuses.createdRequest,
            data: options
        });

        let yandexMoneySdk = Meteor.npmRequire("yandex-money-sdk");

        let data = Async.runSync(function (done) {

            let setOperationStatus = (status, data, msg) => {
                done(null, {
                    _id: transactionId,
                    status: status,
                    data: data,
                    msg: msg
                });
            };

            yandexMoneySdk.ExternalPayment.getInstanceId(
                yandexMoneyConfig.clientId,
                (err, data) => {
                    if (err) {
                        setOperationStatus(MoneyTransactions.statuses.fail, err,
                            "Error on yandexMoneySdk.ExternalPayment.getInstanceId!");
                        return;
                    }
                    var instanceId = data.instance_id;

                    let externalPayment = new yandexMoneySdk.ExternalPayment(instanceId);

                    externalPayment.request(options, (err, data) => {
                        if(err) {
                            setOperationStatus(MoneyTransactions.statuses.fail, err,
                                "Error on externalPayment.request!");
                            return;
                        }
                        if(data.status != "success") {
                            setOperationStatus(MoneyTransactions.statuses.fail, data,
                                "Error on externalPayment.request!");
                            return;
                        }
                        var requestId = data.request_id;

                        externalPayment.process({
                            "request_id": requestId,
                            "ext_auth_success_uri": Meteor.absoluteUrl() + `money/account/put-money/card/success/${transactionId}`,
                            "ext_auth_fail_uri": Meteor.absoluteUrl() + `money/account/put-money/card/fail/${transactionId}`
                        }, (err, data) => {
                            if(err) {
                                setOperationStatus(MoneyTransactions.statuses.fail, err,
                                    "Error on externalPayment.process!");
                                return;
                            }
                            setOperationStatus(MoneyTransactions.statuses.inProcess, data);
                        });
                    });
                }
            );
        });
        /**
         * Update transaction
         */
        MoneyTransactions.update(
            {_id: transactionId},
            {
                $set: {
                    type: MoneyTransactions.types.payIn.card,
                    status: data.result.status,
                    result: data.result,
                    sum: sumWithCommission,
                    data: options
                }
            },
            {upsert: true}
        );
        if (data.result.msg)
            throw new Meteor.Error(500, data.result.msg, data.result.data);

        return data.result;
    },
    /**
     * Change payIn transaction status success
     * @param transactionId
     */
    "cardPayChangeStatusSuccess": (transactionId) => {
        check(transactionId, String);
        changePayStatus(transactionId, MoneyTransactions.statuses.success);
    },
    /**
     * Change payIn transaction status fail
     * @param transactionId
     */
    "cardPayChangeStatusFail": (transactionId) => {
        check(transactionId, String);
        changePayStatus(transactionId, MoneyTransactions.statuses.fail);
    }
});