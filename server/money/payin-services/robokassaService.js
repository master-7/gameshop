/**
 * Config for connect
 * @type {{saleId, pass1, pass2, testPayments, successRedirectURI, failRedirectURI}}
 */
var robokassaConfig = Common.configs.payIn.robokassa;

/**
 * Check robokassa signature
 * @param transaction
 * @param invId
 * @param outSum
 * @param signatureValue
 */
var checkRobokassaSignature = (transaction, invId, outSum, signatureValue) => {
    if(!transaction) {
        throw new Meteor.Error(404, "Not found transaction!");
    }

    if(transaction.status != MoneyTransactions.statuses.createdRequest) {
        throw new Meteor.Error(400, "Not correct transaction!");
    }

    if(transaction.sum != outSum) {
        throw new Meteor.Error(400, "Not correct sum!");
    }
};

/**
 * Publish payment system methods
 */
Meteor.methods({
    /**
     * Generate merchant link
     * @param sum
     * @returns {*|Object}
     */
    "rabokassaGenerateMerchantLink": (sum) => {
        let robokassaSDK = Meteor.npmRequire("robokassa");
        let robokassaAPI = new robokassaSDK({
            login: robokassaConfig.saleId,
            password: robokassaConfig.pass1
        });

        let InvId = Sequence.getNextSequence(Sequence.type.robokassaTransaction);

        /**
         * @todo: index on field data.invId
         */
        MoneyTransactions.insert({
            sum: sum,
            type: MoneyTransactions.types.payIn.robokassa,
            status: MoneyTransactions.statuses.createdRequest,
            invId: InvId,
            data: {sum: sum}
        });

        let data = Async.runSync(function (done) {
            let url = robokassaAPI.merchantUrl({ id: InvId, summ: sum, description: "SaleGame.net"});
            done(null, url);
        });
        return data.result;
    },
	/**
     * @param invId
     * @param outSum
     * @param signatureValue
     */
    "robokassaSuccessPayment": (invId, outSum, signatureValue) => {
        let transaction = MoneyTransactions.findOne({ invId: Number.parseInt(invId) });

        checkRobokassaSignature(
            transaction,
            invId,
            outSum,
            signatureValue
        );
        MoneyTransactions.update({_id: transaction._id}, { $set: { status: MoneyTransactions.statuses.success }});
    },
	/**
     * @param invId
     * @param outSum
     * @param signatureValue
     */
    "robokassaFailPayment": (invId, outSum, signatureValue) => {
        let transaction = MoneyTransactions.findOne({ invId: Number.parseInt(invId) });

        checkRobokassaSignature(
            transaction,
            invId,
            outSum,
            signatureValue
        );
        MoneyTransactions.update({_id: transaction._id}, { $set: { status: MoneyTransactions.statuses.fail }});
    }
});