MoneyTransactions.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        type: {
            type: Number,
            label: "Money transaction type",
            index: true
        },
        status: {
            type: Number,
            label: "Money transaction status",
            min: MoneyTransactions.statuses.fail,
            max: MoneyTransactions.statuses.success,
            index: true
        },
        data: {
            type: Object,
            blackbox: true
        },
        invId: {
            type: Number,
            index: true,
            optional: true
        },
        sum: {
            type: Number,
            label: "Money transaction sum",
            decimal: true
        },
        owner: {
            type: String,
            index: true,
            optional: true
        }
    },
    Common.system.schemas.commonFields
));

MoneyTransactions.attachSchema(MoneyTransactions.schema);

/**
 * Publish only current user
 */
Meteor.publish('moneyTransactions', function (limit, selectors) {
    if (Common.system.permissions.isMoneyAdmin(this.userId)) {
        let calculatedLimit = Common.constants.loadParties.countItems + limit;
        let condition = {};

        if (selectors) {
            if (selectors._id)
                condition._id = selectors._id;

            if (selectors.type)
                switch (selectors.type) {
                    case MoneyTransactions.globalTypes.payIn:
                        condition.type = {
                            $in: [
                                MoneyTransactions.types.payIn.yandex,
                                MoneyTransactions.types.payIn.robokassa,
                                MoneyTransactions.types.payIn.card
                            ]
                        };
                    break;
                    case MoneyTransactions.globalTypes.buy:
                        condition.type = {
                            $in: [
                                MoneyTransactions.types.deal.game,
                                MoneyTransactions.types.deal.thing
                            ]
                        };
                    break;
                }

            if (selectors.owner)
                condition.owner = selectors.owner;

            if (Number.isInteger(selectors.status))
                condition.status = selectors.status;
        }
        
        return MoneyTransactions.find(
            condition,
            {
                sort: {
                    createdAt: -1
                },
                limit: calculatedLimit
            }
        );
    }
    return this.ready();
});

/**
 * If pay in success add moneyAccount sum
 * @param transaction
 */
MoneyTransactions.payInTransactionAction = (transaction) => {
    let successPayIn = MoneyTransactions.types.payIn.test.hasOwnProperty(transaction.type) &&
        transaction.status == MoneyTransactions.statuses.success;

    if (successPayIn) {
        let exist = MoneyAccount.findOne({owner: Meteor.userId()});
        if (exist)
            MoneyAccount.update({_id: exist._id}, {$inc: {sum: transaction.sum}}, {tx: true});
        else
            MoneyAccount.insert({sum: transaction.sum}, {tx: true});
    }
};

/**
 * If success pay game deal do cash settlements
 * @param transaction
 */
MoneyTransactions.payGameTransactionAction = (transaction) => {
    let successPayGame = (MoneyTransactions.types.deal.game == transaction.type ||
        MoneyTransactions.types.deal.gameWithMediator == transaction.type) &&
        (transaction.status == MoneyTransactions.statuses.success ||
            transaction.status == MoneyTransactions.statuses.inProcess);

    if (successPayGame) {
        let targetForSale = GameAccount.findOne({
            _id: transaction.data.targetForSale,
            status: Common.constants.game.statuses.isPublic
        });

        if(!targetForSale) {
            throw new Meteor.Error(Common.constants.errorCode.http.internalServerError,
                "Game does not exist or has been sold!");
        }

        let moneyAccountBuyer = MoneyAccount.findOne({owner: transaction.data.buyer});
        let moneyAccountSeller = MoneyAccount.findOne({owner: transaction.data.seller});

        if (moneyAccountBuyer && moneyAccountBuyer.sum >= transaction.sum) {
            tx.start("Start to perform deal");
            MoneyAccount.update(
                {_id: moneyAccountBuyer._id},
                {$inc: {sum: -transaction.sum - targetForSale.commission}},
                {tx: true}
            );
            /**
             * If pre-moderation mode
             */
            if(targetForSale.sellingMode == Common.constants.game.sellingMode.preApproval) {
                if (moneyAccountSeller) {
                    MoneyAccount.update(
                        {_id: moneyAccountSeller._id},
                        {$inc: {sum: transaction.sum}},
                        {tx: true}
                    );
                }
                else {
                    MoneyAccount.insert(
                        {
                            transactionId: transaction._id,
                            sum: transaction.sum,
                            owner: transaction.data.seller
                        },
                        {tx: true}
                    );
                }
                FreeMoney.insert(
                    {
                        transactionId: transaction._id,
                        sum: targetForSale.commission
                    },
                    {tx: true}
                );
                GameAccount.update(
                    {_id: targetForSale._id},
                    {
                        $set: {
                            status: Common.constants.game.statuses.sold,
                            buyer: transaction.data.buyer,
                            transaction: transaction._id
                        }
                    },
                    {tx: true}
                );
                Deals.update(
                    {_id: transaction.data._id},
                    {
                        $set: {
                            status: Common.constants.deal.status.success,
                            moneyTransactionsId: transaction._id
                        }
                    },
                    {tx: true}
                );
                Meteor.users.update(
                    {
                        _id: transaction.data.seller
                    },
                    {
                        $inc: {
                            'profile.successDeal': 1
                        }
                    },
                    {tx: true}
                );
            }
            else {
                GameAccount.update(
                    {_id: targetForSale._id},
                    {
                        $set: {
                            status: Common.constants.game.statuses.selling,
                            buyer: transaction.data.buyer,
                            transaction: transaction._id
                        }
                    },
                    {tx: true}
                );
                Deals.update(
                    {_id: transaction.data._id},
                    {
                        $set: {
                            status: Common.constants.deal.status.inProcess,
                            moneyTransactionsId: transaction._id
                        }
                    },
                    {tx: true}
                );
            }
            tx.commit();
        }
        else {
            Deals.update({_id: transaction.data._id}, {
                $set: {
                    status: Common.constants.deal.status.fail,
                    moneyTransactionsId: transaction._id
                }
            });
            throw new Meteor.Error(Common.constants.errorCode.http.internalServerError, "Error on create new deal! Not enough money!");
        }
    }
};

/**
 * If success paid ads add - funds the cache
 * @param transaction
 */
MoneyTransactions.paidAdsTransactionAction = (transaction) => {
    let successPaidAds = MoneyTransactions.types.debit.test.hasOwnProperty(transaction.type) &&
        transaction.status == MoneyTransactions.statuses.success;
    if(successPaidAds) {
        tx.start("Create paid ads action");
            MoneyAccount.update(
                {_id: transaction.data.moneyAccountId},
                {$inc: {sum: -transaction.sum}},
                {tx: true}
            );
            FreeMoney.insert(
                {
                    transactionId: transaction._id,
                    sum: transaction.sum
                }
            );
        tx.commit();        
    }
};

/**
 * Deal game with moderator finish
 * @param transaction
 */
MoneyTransactions.dealGameWithModerator = (transaction) => {
    if(Deals.types.gameWithMediator == transaction.type) {
        let targetForSale = GameAccount.findOne({
            _id: transaction.data.targetForSale
        });

        if(!targetForSale)
            throw new Meteor.Error(Common.constants.errorCode.http.badRequest,
                "Target for sale not exist!");

        let moneyAccountBuyer = MoneyAccount.findOne({owner: targetForSale.buyer});
        let moneyAccountSeller = MoneyAccount.findOne({owner: targetForSale.creator});

        if(transaction.status == MoneyTransactions.statuses.success) {
            tx.start("Start success finished deals with mediator");
            if (moneyAccountSeller) {
                MoneyAccount.update(
                    {_id: moneyAccountSeller._id},
                    {$inc: {sum: transaction.sum}},
                    {tx: true}
                );
            }
            else {
                MoneyAccount.insert(
                    {
                        transactionId: transaction._id,
                        sum: transaction.sum,
                        owner: transaction.data.seller
                    },
                    {tx: true}
                );
            }
            FreeMoney.insert(
                {
                    transactionId: transaction._id,
                    sum: targetForSale.commission
                },
                {tx: true}
            );
            GameAccount.update(
                {_id: targetForSale._id},
                {
                    $set: {
                        status: Common.constants.game.statuses.sold,
                        buyer: targetForSale.buyer,
                        transaction: transaction._id
                    }
                },
                {tx: true}
            );
            Meteor.users.update(
                {
                    _id: transaction.data.seller
                },
                {
                    $inc: {
                        'profile.successDeal': 1
                    }
                },
                {tx: true}
            );
            tx.commit();
        }
        if(transaction.status == MoneyTransactions.statuses.fail) {
            tx.start("Start to fail finished deals with mediator");
            MoneyAccount.update(
                {_id: moneyAccountBuyer._id},
                {$inc: {sum: transaction.sum + targetForSale.commission}},
                {tx: true}
            );
            GameAccount.update(
                {_id: targetForSale._id},
                {
                    $unset: {
                        buyer: ""
                    },
                    $set: {
                        status: Common.constants.game.statuses.failed,
                        transaction: transaction._id
                    }
                },
                {tx: true}
            );
            Meteor.users.update(
                {
                    _id: transaction.data.seller
                },
                {
                    $inc: {
                        'profile.failDeal': 1
                    }
                },
                {tx: true}
            );
            tx.commit();
        }
    }
};

/**
 * Before insert data action
 */
MoneyTransactions.before.insert(function (userId, transaction) {
    transaction.owner = Meteor.userId();
});

/**
 * After insert data action
 */
MoneyTransactions.after.insert(function (userId, transaction) {
    MoneyTransactions.payInTransactionAction(
        transaction
    );
    MoneyTransactions.payGameTransactionAction(
        transaction
    );
    MoneyTransactions.paidAdsTransactionAction(
        transaction
    );
    MoneyTransactions.dealGameWithModerator(
        transaction
    );
});

/**
 * Before update data action
 */
MoneyTransactions.before.update(function (userId, transaction, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
});

/**
 * After update data action
 */
MoneyTransactions.after.update(function (userId, transaction, fieldNames, modifier, options) {
    MoneyTransactions.payInTransactionAction(
        transaction
    );
    MoneyTransactions.dealGameWithModerator(
        transaction
    );
});