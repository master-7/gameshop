/**
 * Allow actions rules
 */
PayOutBid.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, bid) {
        return Common.system.permissions.isMoneyAdmin(userId);
    },
    update: function (userId, bid) {
        return Common.system.permissions.isMoneyAdmin(userId);
    }
});

/**
 * Before insert data action
 */
PayOutBid.before.insert(function (userId, bid) {
    //Check money account
    let moneyAccountUser = MoneyAccount.findOne({owner: userId});

    if(!moneyAccountUser || !moneyAccountUser.sum || moneyAccountUser.sum <= 0) {
        throw new Meteor.Error(Common.constants.errorCode.http.badRequest, "You not have the money account!");
    }

    if(moneyAccountUser.sum < bid.sum) {
        throw new Meteor.Error(Common.constants.errorCode.http.badRequest, "Sum more then you have!");
    }

    bid.creator = Meteor.userId();
    bid.status = Common.constants.payOutBid.statuses.hasCreated;
});

/**
 * After insert data action
 */
PayOutBid.after.insert(function (userId, bid) {
    /**
     * Fix money account sum after create bid
     */
    MoneyAccount.update({owner: userId},
        {
            $inc: {
                sum: -bid.sum
            }
        },
    );
});

/**
 * After update data action
 */
PayOutBid.after.update(
    function (userId, doc, fieldNames, modifier, options) {
        if(doc.status == Common.constants.payOutBid.statuses.verificationFail) {
            MoneyAccount.update({owner: doc.creator},
                {
                    $inc: {
                        sum: doc.sum
                    }
                },
            );
        }
    },
    {fetchPrevious: false}
);

/**
 * PayOutBid publish
 */
Meteor.publish("userPayOutBids", function(limit) {
    let calculatedLimit = Common.constants.loadParties.countItems + limit;
    return PayOutBid.find(
        {
            creator: this.userId
        },
        {
            sort: {
                createdAt: -1
            },
            limit: calculatedLimit
        }
    );
});

/**
 * PayOutBid for admin publish
 */
Meteor.publish("userPayOutBidsForAdmin", function(limit, selectors) {
    let calculatedLimit = Common.constants.loadParties.countItems + limit;

    if(Common.system.permissions.isMoneyAdmin(this.userId)) {
        let condition = {};

        if(selectors) {
            if(selectors._id)
                condition._id = selectors._id;

            if(selectors.creator)
                condition.creator = selectors.creator;

            if(selectors.status)
                condition.status = selectors.status;
        }

        return PayOutBid.find(
            condition,
            {
                sort: {
                    createdAt: -1
                },
                limit: calculatedLimit
            }
        );
    }
    return this.ready();
});