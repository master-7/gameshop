/**
 * Before insert data action
 */
Wallets.before.insert(function (userId, wallet) {
    if(!wallet.owner)
        wallet.owner = Meteor.userId();
});

/**
 * user's all wallets
 */
Meteor.publish("userWallets", function () {
    return Wallets.find({owner: this.userId});
});