/**
 * Account status
 * @type {{block: number, active: number}}
 */
MoneyAccount.statuses = {
    block: 0,
    active: 1
};

/**
 * Before insert data action
 */
MoneyAccount.before.insert(function (userId, account) {
    account.status = MoneyAccount.statuses.active;
    if(!account.owner)
        account.owner = Meteor.userId();
    account.createdAt = Date.now();
    account.modifiedAt = Date.now();
});

/**
 * Before update data action
 */
MoneyAccount.before.update(function (userId, account, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = Date.now();
});

/**
 * Publish only current user
 */
Meteor.publish('moneyAccount', function() {
    return MoneyAccount.find({
        owner: this.userId
    });
});