Meteor.startup(function () {
    let systemBots = [
        {
            name: "Sale",
            surname: "Game",
            username: "gamebot@salegame.net",
            pass: "etT53Fbh87Mn#@",
            roles: ['user']
        }
    ];
    for (let index in systemBots) {
        let user = systemBots[index];
        if(user && user.username) {
            let existsUser = Meteor.users.findOne({username: user.username});
            if (!existsUser) {
                let id = Accounts.createUser({
                    username: user.username,
                    password: user.pass,
                    profile: {name: user.name, surname: user.surname}
                });
                Roles.addUsersToRoles(id, user.roles, Roles.GLOBAL_GROUP);
            }
        }
    }
});
