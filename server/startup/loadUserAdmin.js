Meteor.startup(function () {
    let userAdmin = {
        name: "Admin",
        surname: "Default",
        username: "admin@salegame.net",
        pass: "Ytrewq654321",
        roles: ['admin']
    };

    let existsUserAdmin = Meteor.users.findOne({username: userAdmin.username});

    if (!existsUserAdmin) {
        let id = Accounts.createUser({
            username: userAdmin.username,
            password: userAdmin.pass,
            profile: {name: userAdmin.name, surname: userAdmin.surname}
        });

        Roles.addUsersToRoles(id, userAdmin.roles, Roles.GLOBAL_GROUP);
    }
});
