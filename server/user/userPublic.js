/**
 * On create user add user role
 */
Meteor.users.after.insert(function (userId, account) {
    Roles.addUsersToRoles(account._id, Common.constants.roles.types.user, Roles.GLOBAL_GROUP);
});

/**
 * If user blocked - find all public games and change creator to system bot
 */
Meteor.users.before.update(function (userId, account, fieldNames, modifier, options) {
    if(modifier.$set && modifier.$set['profile.blocked']) {
        let systemBot = Meteor.users.findOne({username: "gamebot@salegame.net"});
        if(systemBot && systemBot._id) {
            let activeGameAccount = GameAccount.find({
                creator: account._id,
                status: Common.constants.game.statuses.isPublic
            }).fetch();
            for(let index in activeGameAccount) {
                let gameAccount = activeGameAccount[index];
                if(gameAccount) {
                    GameAccount.update(
                        {
                            _id: gameAccount._id
                        },
                        {
                            $set: {
                                creator: systemBot._id
                            }
                        },
                        {
                            tx: true
                        }
                    );
                }
            }
        }
    }
});

/**
 * Get all user data
 */
Meteor.publish("userData", function () {
    if (this.userId) {
        return Meteor.users.find(
            {
                _id: this.userId
            },
            {
                fields: { 'services': 1, 'others': 1 }
            }
        );
    }
    else {
        return this.ready();
    }
});

/**
 * Users subscribe
 */
Meteor.publish("usersByIdInArray", function ($id) {
    return Meteor.users.find(
        {
            _id: {
                $in: $id
            }
        },
        {
            fields: { _id: 1, username: 1, profile: 1 }
        }
    );
});

/**
 * Get user data by id
 */
Meteor.publish("getUserDataById", function (id) {
    if(id) {
        return Meteor.users.find(
            {_id: id},
            {
                fields: {_id: 1, username: 1, profile: 1}
            }
        );
    }
    return this.ready();
});

/**
 * Get all users for management
 */
Meteor.publish("userForManagement", function (limit, selectors) {
    if (Common.system.permissions.isMoneyAdmin(this.userId)) {
        let condition = {};
        if(selectors) {
            if(selectors._id)
                condition._id = selectors._id;
            if(selectors.username) {
                if(Array.isArray(selectors.username)) {
                    for (let index in selectors.username) {
                        if(typeof selectors.username[index] == "string" && selectors.username[index]) {
                            if(!condition.$or) {
                                condition.$or = [];
                            }
                            condition.$or.push({
                                username: new RegExp(selectors.username[index], "g")
                            });
                        }
                    }
                }
                else {
                    condition.username = new RegExp(selectors.username, "g");
                }
            }
        }
        
        let calculatedLimit = Common.constants.loadParties.countItems + limit;

        return Meteor.users.find(
            condition,
            {
                fields: {_id: 1, username: 1, profile: 1, roles: 1},
                limit: calculatedLimit
            }
        );
    }
    return this.ready();
});

Meteor.methods({
    /**
     * Edit user profile method
     * @param data
     */
    "changeUserProfile": function (data) {
        if (data._id != Meteor.userId()) {
            /**
             * Fix tracker events - disallow change someone else profile
             */
            SystemTracker.insert({
                type: SystemTracker.type.edit.profile,
                status: SystemTracker.operationStatus.disallow,
                data: data
            });
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Not allow change someone else profile!");
        }

        check(data.profile.name, String);
        check(data.profile.surname, String);

        if(typeof data.profile.sendEmailNotification == "boolean")
            check(data.profile.sendEmailNotification, Boolean);

        check(data.username, String);

        /**
         * Tracker event "success profile changes"
         */
        SystemTracker.insert({
            type: SystemTracker.type.edit.profile,
            status: SystemTracker.operationStatus.success,
            data: data
        });

        if (Meteor.user().profile.name != data.profile.name)
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    "profile.name": data.profile.name
                }
            });

        if (Meteor.user().profile.surname != data.profile.surname)
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    "profile.surname": data.profile.surname
                }
            });

        if (Meteor.user().profile.sendEmailNotification != data.profile.sendEmailNotification)
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    "profile.sendEmailNotification": data.profile.sendEmailNotification
                }
            });

        if (Meteor.user().profile.avatar != data.profile.avatar)
            Meteor.users.update({_id: Meteor.userId()}, {
                $addToSet: {
                    "profile.avatar": data.profile.avatar
                }
            });

        if (Meteor.user().username != data.username) {
            let userExist = Meteor.users.findOne({username: data.username});
            if(userExist) {
                throw new Meteor.Error(
                    Common.constants.errorCode.http.badRequest,
                    "User exist!"
                );
            }
            else {
                Meteor.users.update({_id: Meteor.userId()}, {
                    $set: {
                        "username": data.username
                    },
                    $unset: {
                        "profile.verifyEmail": 0,
                        "profile.verifyEmailSend": 0,
                        "profile.sendEmailNotification": 0
                    }
                });
            }
        }

        if (data.password)
            Accounts.setPassword(Meteor.userId(), data.password);
    },
    /**
     * Change user avatar (imageId)
     * @param imageId
     */
    "changeUserAvatar": (imageId) => {
        check(imageId, String);
        if (Meteor.user().profile.avatar != imageId) {
            Images.remove({_id: Meteor.user().profile.avatar});
            Meteor.users.update({_id: Meteor.userId()}, {
                $set: {
                    "profile.avatar": imageId
                }
            });
        }
    },
    /**
     * Add steam account
     * @param userId
     */
    "addSteamAccountToUser": (userId) => {
        let user = Meteor.user();

        let steamData = JSON.parse(JSON.stringify(user.services.steam));

        //Remove user
        Meteor.users.remove({_id: Meteor.userId()});

        if(steamData)
            Meteor.users.update({_id: userId},
                {
                    $set: {
                        "services.steam.id": steamData.id
                    }
                }
            );
    },
    /**
     * Create verify email query
     */
    "createVerifyEmailQuery": () => {
        let user = Meteor.user();
        if (!user.profile.verifyEmail && !user.profile.verifyEmailSend) {
            if(user.username) {
                let verifyEmailHash = SHA256(
                    Common.helpers.generateRandomString() + Date.now()
                );

                Meteor.users.update({_id: user._id},
                    {
                        $set: {
                            "profile.verifyEmail": false,
                            "profile.verifyEmailSend": true,
                            "profile.verifyEmailHash": verifyEmailHash
                        }
                    }
                );

                let eventLink = Meteor.absoluteUrl('user/verify-email/' + user.username + '/' + verifyEmailHash);

                Common.methods.sendHtmlEmail(
                    user.username,
                    Common.configs.email.support,
                    i18n("user.verifyEmail"),
                    "notification.html",
                    {
                        eventHeader: i18n("user.verifyNeed"),
                        eventData: i18n("user.pressLink"),
                        eventLink: eventLink,
                        eventAction: i18n("user.verifyAction"),
                        eventActionWithoutHTML: i18n("user.emailClientNotSupport") + eventLink
                    }
                );
            }
        }
        else
            throw new Meteor.Error(
                Common.constants.errorCode.http.badRequest,
                "You verify the email address or query was be send!"
            );
    },
    /**
     * Verify email for user
     * @param username
     * @param verifyEmailHash
     */
    "verifyEmailForUser": (username, verifyEmailHash) => {
        let tokenValid = Meteor.users.findOne({
            username: username,
            "profile.verifyEmailHash": verifyEmailHash
        });

        if(tokenValid) {
            Meteor.users.update(
                {
                    username: username,
                    "profile.verifyEmailHash": verifyEmailHash
                },
                {
                    $set: {
                        "profile.verifyEmail": true
                    },
                    $unset: {
                        "profile.verifyEmailSend": "",
                        "profile.verifyEmailHash": ""
                    }
                }
            )
        }
        else
            throw new Meteor.Error(
                Common.constants.errorCode.http.badRequest,
                "Not correct username or token!"
            );
    },
    /**
     * Create user for administrator
     * @param credentials
     */
    "createUserForAdmin": (credentials) => {
        if (Common.system.permissions.isAdmin(Meteor.userId())) {
            check(credentials.username, String);
            check(credentials.password, String);
            check(credentials.profile.name, String);
            check(credentials.profile.surname, String);
            check(credentials.role, String);

            let id = Accounts.createUser({
                username: credentials.username,
                password: credentials.password,
                profile: {
                    name: credentials.profile.name,
                    surname: credentials.profile.surname
                }
            });

            Roles.addUsersToRoles(
                id,
                [ credentials.role ] || [ Common.constants.roles.types.user ],
                Roles.GLOBAL_GROUP
            );
        }
        else {
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Only for administrator!");
        }
    },
    /**
     * Edit user profile method for admin
     * @param data
     */
    "adminChangeUserProfile": function (data) {
        if (Common.system.permissions.isAdmin(Meteor.userId())) {
            check(data.profile.name, String);
            check(data.profile.surname, String);
            check(data.username, String);

            let user = Meteor.users.findOne({_id: data._id});

            if (user.profile.name != data.profile.name)
                Meteor.users.update({_id: data._id}, {
                    $set: {
                        "profile.name": data.profile.name
                    }
                });

            if (user.profile.surname != data.profile.surname)
                Meteor.users.update({_id: data._id}, {
                    $set: {
                        "profile.surname": data.profile.surname
                    }
                });

            if (user.username != data.username)
                Accounts.setUsername(data._id, data.username);

            if (data.password)
                Accounts.setPassword(data._id, data.password);

            let changeBlockedStatus = data.profile.blocked ?
            {
                $set: {
                    "profile.blocked": true
                }
            }
                :
            {
                $unset: {
                    "profile.blocked": ""
                }
            };

            Meteor.users.update({_id: data._id}, changeBlockedStatus);

            if (data.roles["__global_roles__"])
                Roles.setUserRoles(
                    data._id,
                    data.roles["__global_roles__"],
                    Roles.GLOBAL_GROUP
                );
        }
        else {
            throw new Meteor.Error(Common.constants.errorCode.http.forbidden, "Only for administrator!");
        }
    },
    /**
     * Create restore pass query
     */
    "createRestorePassQuery": (username) => {
        let user = Meteor.users.findOne({username: username});

        if(user.username) {
            let restorePassToken = SHA256(
                Common.helpers.generateRandomString() + Date.now()
            );

            Meteor.users.update({_id: user._id},
                {
                    $set: {
                        "profile.restorePassToken": restorePassToken
                    }
                }
            );

            let eventLink = Meteor.absoluteUrl(`restore-pass?username=${user.username}&token=${restorePassToken}`);

            Common.methods.sendHtmlEmail(
                user.username,
                Common.configs.email.support,
                i18n("restorePass.theme"),
                "notification.html",
                {
                    eventHeader: i18n("restorePass.restore"),
                    eventData: i18n("restorePass.pressLink"),
                    eventLink: eventLink,
                    eventAction: i18n("restorePass.verifyAction"),
                    eventActionWithoutHTML: i18n("restorePass.emailClientNotSupport") + eventLink
                }
            );
        }
    },
    /**
     * Create restore pass query
     */
    "changePassByToken": (username, token, newPass) => {
        let user = Meteor.users.findOne({
	        username: username,
	        "profile.restorePassToken": token
        });
        if(user.username) {
	        Accounts.setPassword(user._id, newPass);
	        Meteor.users.update(
		        {
			        username: username
		        },
		        {
			        $unset: {
				        "profile.restorePassToken": ""
			        }
		        }
	        );
        }
	    else
	        throw new Meteor.Error(
		        Common.constants.errorCode.http.badRequest,
		        "Not correct username or token!"
	        );
    }
});