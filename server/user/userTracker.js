/**
 * Publish user tracker events
 */
Meteor.publish("userTrackerPublic", function (id) {
    let condition = {};

    if(id) {
        condition._id = id;
    }

    return UserTracker.find(
        Object.assign({},
            condition,
            {
                creator: this.userId,
                status: Common.constants.userTracker.status.created
            }
        )
    );
});

/**
 * Publish user tracker events without status
 */
Meteor.publish("userTrackerForCreator", function (id) {
    let condition = {};

    if(id) {
        condition._id = id;
    }

    return UserTracker.find(
        Object.assign({},
            condition,
            {
                creator: this.userId
            }
        )
    );
});

/**
 * Before insert data action
 */
UserTracker.before.insert(function (userId, doc) {
    if (!doc.creator)
        doc.creator = Meteor.userId();
    doc.status = Common.constants.userTracker.status.created;
    doc.createdAt = new Date();
});

/**
 * After insert data action
 */
UserTracker.after.insert(function (userId, doc) {
    let creator = Meteor.users.findOne({_id: doc.creator});

    let creatorReadyGetEmailNotification = creator.profile.verifyEmail
        && creator.profile.sendEmailNotification && !creator.status.online;

    if(creatorReadyGetEmailNotification) {
        let data = {};

        if(doc.image) {
            let image = Thumbs.findOne({_id: doc.image._id});
            data.icon = Meteor.absoluteUrl(
                Common.helpers.getImageLink(image)
            );
        }
        else {
            data.icon = null;
        }

        switch (doc.type) {
            case Common.constants.userTracker.type.moderation.createdGame:
                data.title = i18n('events.createdGame.title');
                data.body = i18n('events.createdGame.body');
                break;
            case Common.constants.userTracker.type.moderation.success:
                data.title = i18n('events.success.title');
                data.body = i18n('events.success.body');
                break;
            case Common.constants.userTracker.type.moderation.returnGame:
                data.title = i18n('events.returnGame.title');
                data.body = i18n('events.returnGame.body');
                break;
            case Common.constants.userTracker.type.moderation.fail:
                data.title = i18n('events.fail.title');
                data.body = i18n('events.fail.body');
                break;
            case Common.constants.userTracker.type.moderation.message:
                data.title = i18n('events.message.title');
                data.body = i18n('events.message.body');
                break;
            case Common.constants.userTracker.type.ticket.message:
                data.title = i18n('events.ticketMessage.title');
                data.body = i18n('events.ticketMessage.body');
                break;
            case Common.constants.userTracker.type.deal.created:
                data.title = i18n('events.dealCreated.title');
                data.body = i18n('events.dealCreated.body');
                break;
            case Common.constants.userTracker.type.deal.message:
                data.title = i18n('events.dealMessage.title');
                data.body = i18n('events.dealMessage.body');
                break;
        }

        let eventLink = Meteor.absoluteUrl(`user-tracker-jump/${doc._id}`);

        Common.methods.sendHtmlEmail(
            creator.username,
            Common.configs.email.support,
            i18n("events.subject"),
            "events.html",
            {
                eventImage: data.icon,
                eventHeader: data.title,
                eventData: data.body,
                eventLink: eventLink,
                eventAction: i18n("events.action"),
                eventActionWithoutHTML: i18n("user.emailClientNotSupport") + eventLink
            }
        );
    }
});