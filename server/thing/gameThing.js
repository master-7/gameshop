/**
 * Before insert data action
 */
GameThing.before.insert(function (userId, thing) {
    thing.name = TagStripper.strip(thing.name);
    thing.description = TagStripper.strip(thing.description);
    thing.commission = Common.helpers.calcCommission(thing.price);
    thing.status = Common.constants.thing.statuses.isPublic;
    if(!thing.creator)
        thing.creator = Meteor.userId();
});

/**
 * Public thing
 * Returns only common account data
 */
Meteor.publish('publicThing', function (limit) {
    let calculatedLimit = Common.constants.loadParties.countItems + limit;
    return GameThing.find(
        {status: Common.constants.thing.statuses.isPublic},
        {limit: calculatedLimit}
    );
});

/**
 * Public thing
 * Returns only common account data
 */
Meteor.publish('publicThingById', function (id) {
    return GameThing.find({
        _id: id, 
        status: Common.constants.thing.statuses.isPublic
    });
});

/**
 * Mark user things
 */
Meteor.publish('thingByLikeObjArray', function (likeObj) {
    let thingIdArray = [];
    for(let index in likeObj) {
        let element = likeObj[index];

        if(element.objectId)
            thingIdArray.push(element.objectId)
    }
    if(thingIdArray)
        return GameThing.find(
            {
                _id: {
                    $in: thingIdArray
                },
                status: Common.constants.game.statuses.isPublic
            }
        );
    return this.ready();
});

/**
 * Thing that current user creator
 * Return all field collection
 */
Meteor.publish('userThing', function () {
    return GameThing.find({
        creator: this.userId
    });
});

/**
 * User game thing
 */
Meteor.publish('userThings', function (userId) {
    return GameThing.find({
        creator: userId
    });
});

Meteor.methods({
    "gameThingCreateComment":(id,msg)=>{
        check(id, String);
        check(msg, String);

        if(!Meteor.userId())
            throw new Meteor.Error(Common.constants.errorCode.http.unauthorized, "Not allowed unauthorized user!");

        GameThing.update(
            {_id: id},
            {
                $push: {
                    comments: {
                        _id: new Meteor.Collection.ObjectID(),
                        msg: TagStripper.strip(msg),
                        author: Meteor.userId(),
                        createdAt: Date.now()
                    }
                }
            }
        );
    }
});