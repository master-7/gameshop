angular.module('gameShopApp').directive('thingPublic', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/thing/publicThing.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = GameThing;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Complaint type
             * @type {number}
             */
            this.complaintType = Common.constants.complaint.objectType.thing;

            /**
             * Subscribe public things collection
             */
            this.subscriptionHandles.push(
                this.subscribe("publicThing",
                    () => {
                        return [
                            this.getReactively('limit.count')
                        ]
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                            GameThing.find({});
                        }
                    }
                )
            );

            /**
             * Select user things collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _thingPublicCollections = () => {
                return GameThing.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                thingPublic: _thingPublicCollections
            });
        }
    }
});

