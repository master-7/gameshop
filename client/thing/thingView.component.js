angular.module('gameShopApp').directive('thingView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/thing/thingView.html',
        controllerAs: 'thingView',
        controller: function ($scope, $controller, $stateParams, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameThing collection
             */
            this.subscriptionHandles.push(
                this.subscribe("publicThingById",
                    () => {
                        return [
                            $stateParams.id
                        ]
                    },
                    {
                        onReady: () => {
                            let gameThing = GameThing.findOne({_id: $stateParams.id});
                            if(gameThing)
                                return gameThing;
                            else
                                library.system.error.handlers(Common.constants.errorCode.http.notFound);
                        }
                    }
                )
            );
            
            /**
             * Select thingcollections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _thingCollections = () => {
                return GameThing.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                thing: _thingCollections
            });

            /**
             * Chat init data
             */
            this.chat = {
                createCommentMethodName: "gameThingCreateComment",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "publicThing",
                subscribeMethod: _thingCollections,
                commentKey: "comments",
                commentType: Common.constants.complaint.objectType.commentType.thing
            };

            /**
             * Init like directive data
             * @type {{objectType: number, key: string}}
             */
            this.like = {
                objectType: Common.constants.like.type.thing,
                objectId: $stateParams.id
            };
        }
    }
});
