angular.module('gameShopApp').directive('thingSales', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/thing/thingSales.html',
        controllerAs: 'thingSalesList',
        controller: function ($scope, $reactive, library) {
            $reactive(this).attach($scope);

             /**
             * Subscribe to user things collection
             */
            this.subscribe("userThing");
            
            /**
             * Select user things collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _thingSalesCollections = () => {
                return GameThing.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                thingSales: _thingSalesCollections
            });

            /**
             * Get main image helper
             * @param imageId
             * @returns {*}
             */
            this.getImageById = (imageId) => {
                if(imageId)
                    return library.image.getImageLink(imageId);
                return undefined;
            };

            /**
             * Check collection on empty
             * @param collection
             * @returns {boolean}
             */
            this.notEmptySection = (collection) => {
                return library.system.checkCollection(collection);
            };
        }
    }
});
