angular.module('gameShopApp').directive('userThings', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/thing/userThings.html',
        controllerAs: 'userThings',
        controller: function ($scope, $controller, $stateParams, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to userThing collection
             */
            this.subscribe("userThings", () => {
                return [
                    $stateParams.id || Meteor.userId()
                ];
            });

            /**
             * Select gameThing collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameThingCollections = () => {
                return GameThing.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                thingPublic: _gameThingCollections
            });
        }
    }
});
