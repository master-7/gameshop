angular.module('gameShopApp').directive('newThing', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/thing/newThing.html',
        controllerAs: 'vm',
        controller: function ($scope, $state, $reactive, library) {
            $reactive(this).attach($scope);

            /**
             * Flag query process
             * @type {boolean}
             */
            this.queryProcess = false;

            /**
             * Steam auth flag
             * @type {boolean}
             */
            this.steamAuth = false;

            /**
             * Thing data
             * @type {{description: null, category: null, images: Array}}
             */
            this.thing = {
                name: null,
                description: null,
                category: null,
                price: null,
                images: []
            };

            /**
             * Data for validate
             */
            this.validateData = Common.constants.schema.validate.thing;

            /**
             * System commission
             * @type {number}
             */
            this.commission = library.payment.getRateInPercent();

            /**
             * Inject to view calc the commission
             * @type {library.payment.calcCommission}
             */
            this.calcCommission = library.payment.calcCommission;

            /**
             * Condition for the subscriptions
             * @returns {{_id: {$in: *}}}
             * @private
             */
            _imagesSubscription = () => {
                return {
                    _id: {
                        $in: this.getCollectionReactively("thing.images")
                    }
                }
            };

            /**
             * Getter images collections
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _imagesCollections = () => {
                return Images.find(_imagesSubscription());
            };

            /**
             * Get meteor user data
             * @returns {*}
             * @private
             */
            _userData = () => {
                return Meteor.users.findOne({_id: Meteor.userId()});
            };

            /**
             * Subscribe to images collection
             */
            this.subscribe("images", () => {
                return [_imagesSubscription()]
            });

            /**
             * Subscribe on all user data
             */
            this.subscribe("userData", () => {},
                {
                    onReady: () => {
                        let userData = Meteor.users.findOne({_id: Meteor.userId()});
                        if(userData.services.steam)
                            this.steamAuth = true;
                    }
                }
            );

            /**
             * Inject image collection to view
             */
            this.helpers({
                images: _imagesCollections,
                user: _userData
            });

            /**
             * Remove image event handler
             * @param id
             */
            this.removeImage = (id) => {
                this.thing.images.splice(
                    this.thing.images.indexOf(id), 1
                );
                Images.remove({_id: id});
            };

            /**
             * Login on steam current user
             */
            this.loginWithSteam = () => {
                this.queryProcess = true;
                let currentUserId = Meteor.userId();
                Meteor.loginWithSteam({
                    redirectUri: "http://salegame.net"
                }, (err, data) => {
                    this.queryProcess = false;
                    Meteor.call("addSteamAccountToUser",
                        currentUserId,
                        function (error, data) {
                            $state.go('login');
                            Accounts.logout();
                        }
                    );
                })
            };

            /**
             * Save thing handler
             */
            this.saveThing = () => {
                this.queryProcess = true;
                GameThing.insert(this.thing, (error, result) => {
                    this.queryProcess = false;
                    if(error) {
                        library.system.error.formValidate(
                            $scope.addThingForm,
                            GameThing.simpleSchema().namedContext().invalidKeys()
                        );
                    }
                    else {
                        library.system.toast('/packages/gameshop-browser/client/main/toast/thingAddSuccess.toast.html');
                        $state.go('thing/sales');
                    }
                });
            };
        }
    }
});
