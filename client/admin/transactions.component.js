angular.module('gameShopApp').directive('transactions', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/transactions.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Search conditions
             */
            this.search = {
                _id: null,
                owner: null,
                type: null,
                status: null
            };

            /**
             * Autocomplete search owner
             * @param owner
             */
            this.selectedOwnerChange = (owner) => {
                if (owner && owner._id) {
                    this.search.owner = owner._id;
                }
                else {
                    this.search.owner = null;
                }
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = MoneyTransactions;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * User's payOutBids subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("moneyTransactions",
                    () => {
                        return [
                            this.getReactively('limit.count'),
                            {
                                _id: this.getReactively('search._id'),
                                owner: this.getReactively('search.owner'),
                                type: parseInt(this.getReactively('search.type')),
                                status: parseInt(this.getReactively('search.status'))
                            }
                        ];
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                        },
                        onStop: () => {
                            $scope.mongoQueryReady = true;
                        }
                    }
                )
            );

            /**
             * User's deals subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                username: [
                                    this.getReactively('userLoginOwner')
                                ]
                            }
                        ];
                    }
                )
            );

            /**
             * MoneyTransactions collection subscribe
             * @private
             */
            _moneyTransactionsCollection = () => {
                return MoneyTransactions.find({});
            };

            /**
             * Users collection subscribe
             * @private
             */
            _usersCollection = () => {
                return Meteor.users.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                moneyTransactions: _moneyTransactionsCollection,
                users: _usersCollection
            });
        }
    }
});