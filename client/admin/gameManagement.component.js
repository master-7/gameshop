angular.module('gameShopApp').directive('gameManagement', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/admin/gameManagement.html',
		controllerAs: 'vm',
		controller: function ($scope, $stateParams, $controller, $reactive) {
			$reactive(this).attach($scope);

			this.gameStatus = Common.constants.game.statuses;

			/**
			 * Limit new items
			 * @type {number}
			 */
			this.limit = {
				count: 0
			};

			/**
			 * Search conditions
			 */
			this.search = {
				gameId: null
			};

			/**
			 * Subscription handles
			 * @type {{}}
			 */
			this.subscriptionHandles = [];

			/**
			 * Inject subscription handles to baseCtrl
			 * @type {{}}
			 */
			$scope.subscriptionHandles = this.subscriptionHandles;

			/**
			 * Inject collection name to baseCtrl
			 * @type {string}
			 */
			$scope.collectionObj = GameAccount;

			/**
			 * Angular extend base controller
			 */
			angular.extend(this, $controller('baseCtrl', {
				$scope: $scope
			}));

			/**
			 * FreeMoney subscription
			 */
			this.subscriptionHandles.push(
				this.subscribe("gameAccountForModerator",
					() => {
						return [
							this.getReactively('search.gameId'),
							this.getReactively('limit.count')
						]
					},
					{
						onReady: () => {
							$scope.mongoQueryReady = true;
						},
						onStop: () => {
							$scope.mongoQueryReady = true;
						}
					}
				)
			);

			/**
			 * FreeMoney collection subscribe
			 * @private
			 */
			_gameAccountCollection = () => {
				return GameAccount.find({});
			};

			/**
			 * Inject collection to view
			 */
			this.helpers({
				gameAccount: _gameAccountCollection
			});

			/**
			 * @param game
			 */
			this.blockGame = (game) => {
				GameAccount.update({_id: game._id}, {
					$set: {
						status: Common.constants.game.statuses.isBlocked
					}
				});
			};

			/**
			 * @param game
			 */
			this.publishGame = (game) => {
				GameAccount.update({_id: game._id}, {
					$set: {
						status: Common.constants.game.statuses.isPublic
					}
				});
			};
		}
	}
});