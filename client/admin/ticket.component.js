angular.module('gameShopApp').directive('adminTicket', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/ticket.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Search conditions
             */
            this.search = {
                creator: null,
                status: null,
                category: null
            };

            /**
             * Autocomplete search creator
             * @param creator
             */
            this.selectedOwnerChange = (creator) => {
                if (creator && creator._id) {
                    this.search.creator = creator._id;
                }
                else {
                    this.search.creator = null;
                }
            };

            /**
             * Inject on baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = Ticket;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(
                this.subscribe("ticketForModerator",
                    () => {
                        return [
                            this.getReactively('limit.count'),
                            {
                                creator: this.getReactively('search.creator'),
                                status: parseInt(this.getReactively('search.status')),
                                category: parseInt(this.getReactively('search.category'))
                            }
                        ];
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                        },
                        onStop: () => {
                            $scope.mongoQueryReady = true;
                        }
                    }
                )
            );
            
            /**
             * User for management subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                username: [
                                    this.getReactively('userLoginCreator')
                                ]
                            }
                        ];
                    }
                )
            );

            /**
             * Select ticket collection
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _ticketCollection = () => {
                return Ticket.find({});
            };

            /**
             * Users collection subscribe
             * @private
             */
            _usersCollection = () => {
                return Meteor.users.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                tickets: _ticketCollection,
                users: _usersCollection
            });
        }
    }
});