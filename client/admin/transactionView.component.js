angular.module('gameShopApp').directive('transactionView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/transactionView.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $reactive, $controller) {
            $reactive(this).attach($scope);

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * user's payOutBids subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("moneyTransactions",
                    () => {
                        return [
                            null,
                            {
                                _id: $stateParams.id
                            }
                        ];
                    }
                )
            );

            /**
             * User's deals subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                username: [
                                    this.getReactively('transaction.owner')
                                ]
                            }
                        ];
                    }
                )
            );

            /**
             * Transaction collection subscribe
             * @private
             */
            _transactionCollection = () => {
                return MoneyTransactions.findOne({});
            };

            /**
             * Users collection subscribe
             * @private
             */
            _usersCollection = () => {
                return Meteor.users.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                transaction: _transactionCollection,
                users: _usersCollection
            });
        }
    }
});