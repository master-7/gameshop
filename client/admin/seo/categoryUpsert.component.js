angular.module('gameShopApp').directive('adminSeoCategoryUpsert', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/admin/seo/categoryUpsert.html',
		controllerAs: 'vm',
		controller: function ($scope, $stateParams, $reactive, $location, $timeout, library) {
			$reactive(this).attach($scope);

			$timeout(() => {
				if(!this.categoryKeywords)
					this.categoryKeywords = {
						image: "",
						isActive: true
					};
			}, 300);
			
			this.subscribe("iconsThumb",
				() => {
					return [
						this.getReactively('categoryKeywords.image')
					];
				}
			);
			
			this.subscribe("categoryKeywords", () => {
				return [$stateParams.id || null];
			});

			_categoryKeywordsCollections = () => {
				return CategoryKeywords.findOne({
					_id: $stateParams.id
				});
			};

			_iconCollections = () => {
				return IconsThumb.findOne({
					originalStore: Common.constants.image.type.icons,
					originalId: this.getReactively('categoryKeywords.image')
				});
			};

			/**
			 * Inject collection to view
			 */
			this.helpers({
				categoryKeywords: _categoryKeywordsCollections,
				icon: _iconCollections
			});

			/**
			 * If image add start to rebuild directive
			 * @type {boolean}
			 */
			$scope.imageAdd = false;

			$scope.$watchCollection(
				() => {
					if(this.categoryKeywords)
						return this.categoryKeywords.image;
				},
				(newVal, oldVal) => {
					//Rebuild
					if(newVal) {
						$scope.imageAdd = true;
						$timeout(() => {
							$scope.imageAdd = false;
						}, 50);
					}
				}
			);

			/**
			 * Create category action
			 */
			this.create = () => {
				if(this.icon) {
					this.categoryKeywords.image = {
						_id: this.icon._id,
						name: this.icon.name,
						store: this.icon.store,
						originalStore: this.icon.originalStore,
						originalId: this.icon.originalId
					};
				}

				if(!this.categoryKeywords._id) {
					CategoryKeywords.insert(this.categoryKeywords,
						(err, data) => {
							if (err) {
								library.system.error.formValidate(
									$scope.createCategoryKeywordsForm,
									CategoryKeywords.simpleSchema().namedContext().invalidKeys()
								);
							}
							else {
								$location.path("/admin/category");
							}
						}
					);
				}
				else {
					CategoryKeywords.update(
						{
							_id: this.categoryKeywords._id
						},
						{
							$set: {
								image: this.categoryKeywords.image,
								name: this.categoryKeywords.name,
								keywords: this.categoryKeywords.keywords,
								metaTags: this.categoryKeywords.metaTags,
								isActive: this.categoryKeywords.isActive
							}
						},
						(err, data) => {
							if (err) {
								library.system.error.formValidate(
									$scope.createCategoryKeywordsForm,
									CategoryKeywords.simpleSchema().namedContext().invalidKeys()
								);
							}
							else {
								$location.path("/admin/category");
							}
						}
					);
				}
			};
		}
	}
});