angular.module('gameShopApp').directive('adminSeoGameAccountTypeUpsert', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/admin/seo/gameAccountTypeUpsert.html',
		controllerAs: 'vm',
		controller: function ($scope, $stateParams, $reactive, $location, $timeout, library) {
			$reactive(this).attach($scope);

			$timeout(() => {
				if(!this.gameAccountType)
					this.gameAccountType = {
						image: "",
						isActive: true
					};
			}, 300);

			this.subscribe("iconsThumb",
				() => {
					return [
						this.getReactively('gameAccountType.image')
					];
				}
			);

			this.subscribe("gameAccountType", () => {
				return [$stateParams.id || null];
			});

			_gameAccountTypeCollection = () => {
				return GameAccountType.findOne({
					_id: $stateParams.id
				});
			};

			_iconCollection = () => {
				return IconsThumb.findOne({
					originalStore: Common.constants.image.type.icons,
					originalId: this.getReactively('gameAccountType.image')
				});
			};

			/**
			 * Inject collection to view
			 */
			this.helpers({
				gameAccountType: _gameAccountTypeCollection,
				icon: _iconCollection
			});

			/**
			 * If image add start to rebuild directive
			 * @type {boolean}
			 */
			$scope.imageAdd = false;

			$scope.$watchCollection(
				() => {
					if(this.gameAccountType)
						return this.gameAccountType.image;
				},
				(newVal, oldVal) => {
					//Rebuild
					if(newVal) {
						$scope.imageAdd = true;
						$timeout(() => {
							$scope.imageAdd = false;
						}, 50);
					}
				}
			);

			/**
			 * Create category action
			 */
			this.create = () => {
				if(this.icon) {
					this.gameAccountType.image = {
						_id: this.icon._id,
						name: this.icon.name,
						store: this.icon.store,
						originalStore: this.icon.originalStore,
						originalId: this.icon.originalId
					};
				}
				if(!this.gameAccountType._id) {
					GameAccountType.insert(this.gameAccountType,
						(err, data) => {
							if (err) {
								library.system.error.formValidate(
									$scope.createCategoryKeywordsForm,
									GameAccountType.simpleSchema().namedContext().invalidKeys()
								);
							}
							else {
								$location.path("/admin/tags");
							}
						}
					);
				}
				else {
					GameAccountType.update(
						{
							_id: this.gameAccountType._id
						},
						{
							$set: {
								image: this.gameAccountType.image,
								name: this.gameAccountType.name,
								keywords: this.gameAccountType.keywords,
								metaTags: this.gameAccountType.metaTags,
								isActive: this.gameAccountType.isActive
							}
						},
						(err, data) => {
							if (err) {
								library.system.error.formValidate(
									$scope.createCategoryKeywordsForm,
									GameAccountType.simpleSchema().namedContext().invalidKeys()
								);
							}
							else {
								$location.path("/admin/tags");
							}
						}
					);
				}
			};
		}
	}
});