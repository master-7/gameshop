angular.module('gameShopApp').directive('adminSeoCategory', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/admin/seo/categoryKeywords.html',
		controllerAs: 'vm',
		controller: function ($scope, $stateParams, $reactive, $controller) {
			$reactive(this).attach($scope);

			/**
			 * Angular extend base controller
			 */
			angular.extend(this, $controller('baseCtrl', {
				$scope: $scope
			}));

			this.subscribe("categoryKeywords");

			_categoryCollections = () => {
				return CategoryKeywords.find({});
			};

			/**
			 * Inject collection to view
			 */
			this.helpers({
				categoryKeywords: _categoryCollections
			});
		}
	}
});