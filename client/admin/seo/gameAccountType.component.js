angular.module('gameShopApp').directive('adminSeoGameAccountType', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/admin/seo/gameAccountType.html',
		controllerAs: 'vm',
		controller: function ($scope, $stateParams, $reactive, $controller) {
			$reactive(this).attach($scope);

			/**
			 * Angular extend base controller
			 */
			angular.extend(this, $controller('baseCtrl', {
				$scope: $scope
			}));

			this.subscribe("gameAccountType");

			_gameAccountTypesCollections = () => {
				return GameAccountType.find({});
			};

			/**
			 * Inject collection to view
			 */
			this.helpers({
				gameAccountTypes: _gameAccountTypesCollections
			});
		}
	}
});