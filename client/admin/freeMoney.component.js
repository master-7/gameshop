angular.module('gameShopApp').directive('freeMoney', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/admin/freeMoney.html',
		controllerAs: 'vm',
		controller: function ($scope, $reactive, $controller, $timeout) {
			$reactive(this).attach($scope);

			/**
			 * Limit new items
			 * @type {number}
			 */
			this.limit = {
				count: 0
			};

			/**
			 * Search conditions
			 */
			this.search = {
				status: FreeMoney.status.active,
				transactionId: null
			};

			/**
			 * Subscription handles
			 * @type {{}}
			 */
			this.subscriptionHandles = [];

			/**
			 * Inject subscription handles to baseCtrl
			 * @type {{}}
			 */
			$scope.subscriptionHandles = this.subscriptionHandles;

			/**
			 * Inject collection name to baseCtrl
			 * @type {string}
			 */
			$scope.collectionObj = FreeMoney;

			/**
			 * Angular extend base controller
			 */
			angular.extend(this, $controller('baseCtrl', {
				$scope: $scope
			}));

			/**
			 * FreeMoney subscription
			 */
			this.subscriptionHandles.push(
				this.subscribe("freeMoney",
					() => {
						return [
							this.getReactively('limit.count'),
							{
								status: this.getReactively('search.status'),
								transactionId: this.getReactively('search.transactionId')
							}
						]
					},
					{
						onReady: () => {
							$scope.mongoQueryReady = true;
						},
						onStop: () => {
							$scope.mongoQueryReady = true;
						}
					}
				)
			);

			/**
			 * FreeMoney collection subscribe
			 * @private
			 */
			_freeMoneyCollection = () => {
				return FreeMoney.find({});
			};

			/**
			 * Inject collection to view
			 */
			this.helpers({
				freeMoney: _freeMoneyCollection
			});

			/**
			 * @type {Number}
			 */
			this.outSum = null;

			/**
			 * @param item
			 */
			this.changeActivity = (item) => {
				$timeout(() => {
					if(item.status) {
						this.outSum += item.sum;
					}
					else {
						this.outSum -= item.sum;
						if(this.outSum <= 1) {
							this.outSum = null;
							this.unSelectAll();
						}
					}
				});
			};

			/**
			 * @type {boolean}
			 */
			this.selectAllFlag = false;

			/**
			 * Select all action
			 */
			this.selectAll = () => {
				for(let index in this.freeMoney) {
					let item = this.freeMoney[index];
					if(typeof item == "object") {
						if(this.selectAllFlag) {
							item.status = false;
							this.outSum = null;
						}
						else {
							item.status = true;
							if(item.sum)
								this.outSum += item.sum;
						}
					}
				}
			};

			/**
			 * Unselect all action
			 */
			this.unSelectAll = () => {
				this.selectAllFlag = true;
				this.selectAll();
				this.selectAllFlag = false;
			};

			/**
			 * Payout action
			 */
			this.payout = () => {
				for(let index in this.freeMoney) {
					let item = this.freeMoney[index];
					if(typeof item == "object" && !Number.isInteger(item.status) && item.status) {
						FreeMoney.update(
							{
								_id: item._id
							},
							{
								$set: {
									status: FreeMoney.status.withdrawn
								}
							}
						)
					}
				}
				this.unSelectAll();
			};
		}
	}

});
