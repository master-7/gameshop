angular.module('gameShopApp').directive('adminGameDeal', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/gameDeal.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $reactive, $controller) {
            $reactive(this).attach($scope);

            /**
             * Only my deals flag
             * @type {boolean}
             */
            this.onlyMy = false;

            /**
             * Only free bid
             * @type {boolean}
             */
            this.onlyFree = false;

            /**
             * Subscription handles
             * @type {Array}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {Array}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Game account for money admin
             */
            this.subscriptionHandles.push(
                this.subscribe("gameAccountDealForModerator", 
                    () => {
                        return [
                            null,
                            this.getReactively('onlyMy'),
                            this.getReactively('onlyFree')
                        ];
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                        },
                        onStop: () => {
                            $scope.mongoQueryReady = true;
                        }
                    }
                )
            );

            /**
             * Deals collection subscribe
             * @private
             */
            _gameAccountsCollection = () => {
                return GameAccount.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccounts: _gameAccountsCollection
            });
        }
    }
});