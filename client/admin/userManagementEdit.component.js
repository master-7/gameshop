angular.module('gameShopApp').directive('userManagementEdit', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/userManagementEdit.html',
        controllerAs: 'vm',
        controller: function ($scope, $state, $stateParams, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Inject to view roles types
             * @type {Common.constants.roles.types|{admin, moderator, user}}
             */
            this.roleTypes = Object.keys(Common.constants.roles.types).
                map(function (key) {
                    return Common.constants.roles.types[key]
                }
            );

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount in status onVerification
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                _id: $stateParams.id
                            }
                        ];
                    }
                )
            );

            /**
             * Select gameAccount collections in status onVerification
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _userCollections = () => {
                return Meteor.users.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collections into view
             */
            this.helpers({
                user: _userCollections
            });

            /**
             * Check for is block user
             */
            this.userIsBlock = () => {
                return this.user && this.user.profile && this.user.profile.blocked;
            };

            /**
             * Change user action
             */
            this.change = (redirect = true) => {
                Meteor.call("adminChangeUserProfile", this.user, function (error, data) {
                    if (error) {
                        library.system.error.handlers(error);
                    }
                    if(redirect)
                        $state.go('admin/user-management');
                });
            };

            /**
             * Toggle user block
             */
            this.blockToggle = () => {
                if(this.userIsBlock()) {
                    delete this.user.profile.blocked;
                }
                else {
                    this.user.profile.blocked = true;
                }
                this.change(false);
            };
        }
    }
});