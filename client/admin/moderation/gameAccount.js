angular.module('gameShopApp').directive('moderationGameAccount', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/moderation/gameAccount.html',
        controllerAs: 'mga',
        controller: function ($scope, $location, $reactive, library) {
            $reactive(this).attach($scope);

            /**
             * Subscribe to gameAccount in status onVerification
             */
            this.subscribe("gameAccountOnVerification",
                () => {},
                {
                    onReady: () => {
                        $scope.mongoQueryReady = true;
                    },
                    onStop: () => {
                        $scope.mongoQueryReady = true;
                    }
                }
            );

            /**
             * Select gameAccount collections in status onVerification
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountOnVerificationCollections = () => {
                return GameAccount.find({});
            };

            /**
             * Inject collections into view
             */
            this.helpers({
                gameAccounts: _gameAccountOnVerificationCollections
            });

            /**
             * Get user by id
             * @param imageId
             * @returns {*}
             */
            this.getImageById = (imageId) => {
                return library.image.getImageLink(imageId);
            };
        }
    }
});