angular.module('gameShopApp').directive('moderationGameAccountView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/moderation/gameAccountView.html',
        controllerAs: 'mgav',
        controller: function ($scope, $stateParams, $reactive, $controller, $window, $location, library) {
            $reactive(this).attach($scope);

            /**
             * Data for validate
             */
            this.validateData = Common.constants.schema.validate.game;

            /**
             * If close the window or destroy controller toggle the status to onVerification
             */
            $scope.closePage = () => {
                //If status not changed
                let gameAccount = GameAccount.findOne({_id: this.gameAccount._id});
                if(gameAccount.status == Common.constants.game.statuses.moderatorIsWorking)
                    Meteor.call("changeGameAccountStatusForModerator",
                        this.gameAccount._id, Common.constants.game.statuses.onVerification,
                        (error, result) => {
                            if(error) {
                                library.system.error.handlers(error);
                            }
                        }
                    );
            };

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount in status onVerification
             */
            this.subscribe("gameAccountModeratorIsWorking",
                () => { },
                {
                    onReady: () => {
                        let gameAccount = GameAccount.findOne({_id: $stateParams.id});

                        if(gameAccount) {
                            if(gameAccount.status == Common.constants.game.statuses.onVerification)
                                Meteor.call("changeGameAccountStatusForModerator",
                                    $stateParams.id, Common.constants.game.statuses.moderatorIsWorking,
                                    (error, result) => {
                                        if(error) {
                                            library.system.error.handlers(error);
                                        }
                                    }
                                );
                            this.images = library.image.filterImageCollectionForNgGallery(gameAccount.images);
                        }
                        else
                            library.system.error.handlers(Common.constants.errorCode.http.notFound);
                    }
                }
            );

            /**
             * Game account on moderation to view
             * @returns {*|{}|T|any|Cursor|175}
             * @private
             */
            _gameAccountOnModeration = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * Init chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountOnModeration, commentKey: string}}
             */
            this.chat = {
                createCommentMethodName: "gameAccountCreateModeratorComment",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "gameAccountModeratorIsWorking",
                subscribeMethod: _gameAccountOnModeration,
                commentKey: "moderatorComments"
            };

            /**
             * Inject collections into view
             */
            this.helpers({
                gameAccount: _gameAccountOnModeration
            });

            /**
             * Change game account data for moderator
             */
            this.changeAccount = () => {
                this.queryProcess = true;
                GameAccount.update(
                    {
                        _id: this.gameAccount._id
                    },
                    {
                        $set: {
                            "gameAccount.login": this.gameAccount.gameAccount.login,
                            "gameAccount.pass": this.gameAccount.gameAccount.pass,
                            "dependentAccount.login": this.gameAccount.dependentAccount.login,
                            "dependentAccount.pass": this.gameAccount.dependentAccount.pass
                        }
                    },
                    (error, data) => {
                        this.queryProcess = false;
                        if(error)
                            library.system.error.handlers(error);
                    }
                );
            };

            /**
             * Moderation is success action
             */
            this.moderationSuccess = () => {
                library.system.dialog(
                    '/packages/gameshop-browser/client/admin/moderation/dialogs/confirm.dialog.html'
                ).then((success) => {
                    if(success) {
                        Meteor.call("changeGameAccountStatusForModerator",
                            $stateParams.id, Common.constants.game.statuses.isPublic,
                            (error, result) => {
                                if(error) {
                                    library.system.error.handlers(error);
                                }
                                else {
                                    $location.path("/admin/moderation-game-account");
                                }
                            }
                        );
                    }
                }, function() {
                    $scope.status = false;
                });
            };

            /**
             * Return account to fix error
             */
            this.returnAccount = () => {
                library.system.dialog(
                    '/packages/gameshop-browser/client/admin/moderation/dialogs/return.dialog.html'
                ).then((success) => {
                    if(success) {
                        Meteor.call("changeGameAccountStatusForModerator",
                            $stateParams.id, Common.constants.game.statuses.verificationFail,
                            (error, result) => {
                                if(error) {
                                    library.system.error.handlers(error);
                                }
                                else {
                                    $location.path("/admin/moderation-game-account");
                                }
                            }
                        );
                    }
                }, function() {
                    $scope.status = false;
                });
            };

            /**
             * Block GameAccount and User
             */
            this.blockAccount = () => {
                library.system.dialog(
                    '/packages/gameshop-browser/client/admin/moderation/dialogs/block.dialog.html'
                ).then((success) => {
                    if(success) {
                        Meteor.call("moderationBlockGameAccountAndUser",
                            $stateParams.id, Common.constants.game.statuses.isBlocked,
                            (error, result) => {
                                if(error) {
                                    library.system.error.handlers(error);
                                }
                                else {
                                    $location.path("/admin/moderation-game-account");
                                }
                            }
                        );
                    }
                }, function() {
                    $scope.status = false;
                });
            };
        }
    }
});