angular.module('gameShopApp').directive('adminMoneyOutView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/moneyOutView.html',
        controllerAs: 'vm',
        controller: function ($scope, $state, $stateParams, $reactive, $controller, $window, library) {
            $reactive(this).attach($scope);

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * If close the window or destroy controller toggle the status to onVerification
             */
            $scope.closePage = () => {
                //If status not changed
                if (!changeStatus && this.notFinalStatus()) {
                    changeStatus = true;
                    PayOutBid.update(
                        {_id: this.userPayOutBid._id},
                        {
                            $set: {status: Common.constants.payOutBid.statuses.hasCreated}
                        }
                    );
                }
            };

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * user's payOutBids subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userPayOutBidsForAdmin",
                    () => {
                        return [
                            null,
                            {
                                _id: $stateParams.id
                            }
                        ];
                    },
                    {
                        onReady: () => {
                            let payOutBid = PayOutBid.findOne({_id: $stateParams.id});

                            if (payOutBid) {
                                if(payOutBid.status == Common.constants.payOutBid.statuses.hasCreated)
                                    PayOutBid.update(
                                        {_id: $stateParams.id},
                                        {
                                            $set: {status: Common.constants.payOutBid.statuses.inProcess}
                                        }
                                    );
                            }
                            else
                                library.system.error.handlers(Common.constants.errorCode.http.notFound);
                        }
                    }
                )
            );

            /**
             * Payout collection subscribe
             * @private
             */
            _userPayOutBidCollection = () => {
                return PayOutBid.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                userPayOutBid: _userPayOutBidCollection
            });

            /**
             * Not final status detected
             * @returns {*|boolean}
             */
            this.notFinalStatus = () => {
                return this.userPayOutBid &&
                    [
                        Common.constants.payOutBid.statuses.hasCreated,
                        Common.constants.payOutBid.statuses.inProcess
                    ].inArray(this.userPayOutBid.status);
            };

            /**
             * Change status flag
             * @type {boolean}
             */
            let changeStatus = false;

            /**
             * Toggle status on success
             */
            this.success = () => {
                if (!changeStatus) {
                    changeStatus = true;
                    PayOutBid.update(
                        {_id: $stateParams.id},
                        {
                            $set: {status: Common.constants.payOutBid.statuses.success}
                        }
                    );
                }
                $state.go('admin/money-out');
            };

            /**
             * Toggle status on error
             */
            this.error = () => {
                if (!changeStatus) {
                    changeStatus = true;
                    PayOutBid.update(
                        {_id: $stateParams.id},
                        {
                            $set: {status: Common.constants.payOutBid.statuses.verificationFail}
                        }
                    );
                }
                $state.go('admin/money-out');
            }
        }
    }
});