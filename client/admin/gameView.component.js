angular.module('gameShopApp').directive('adminGameView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/gameView.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $reactive, $controller) {
            $reactive(this).attach($scope);

            /**
             * Subscription handles
             * @type {Array}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {Array}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Game account for money admin
             */
            this.subscriptionHandles.push(
                this.subscribe("gameAccountForMoneyAdmin",
                    () => {
                        return [
                            $stateParams.id
                        ];
                    }
                )
            );

            /**
             * Deals collection subscribe
             * @private
             */
            _gameAccountCollection = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccount: _gameAccountCollection
            });
        }
    }
});