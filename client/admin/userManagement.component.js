angular.module('gameShopApp').directive('userManagement', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/userManagement.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Search conditions
             * @type {{_id: null, username: null}}
             */
            this.search = {
                _id: null,
                username: null
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = Meteor.users;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount in status onVerification
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                _id: this.getReactively('search._id'),
                                username: this.getReactively('search.username')
                            }
                        ];
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                        },
                        onStop: () => {
                            $scope.mongoQueryReady = true;
                        }
                    }
                )
            );

            /**
             * Select gameAccount collections in status onVerification
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _usersCollections = () => {
                let condition = {};

                let id = this.getReactively('search._id');
                if(id) {
                    condition._id = id;
                }

                let username = this.getReactively('search.username');
                if(username) {
                    condition.username = new RegExp(username, "g");
                }

                return Meteor.users.find(condition);
            };

            /**
             * Inject collections into view
             */
            this.helpers({
                users: _usersCollections
            });
        }
    }
});