angular.module('gameShopApp').directive('deals', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/deals.html',
        controllerAs: 'vm',
        controller: function ($scope, $reactive, $controller) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Search conditions
             */
            this.search = {
                buyer: null,
                seller: null,
                status: null
            };

            /**
             * Autocomplete search seller
             * @param seller
             */
            this.selectedSellerChange = (seller) => {
                if (seller && seller._id) {
                    this.search.seller = seller._id;
                }
                else {
                    this.search.seller = null;
                }
            };

            /**
             * Autocomplete search buyer
             * @param buyer
             */
            this.selectedBuyerChange = (buyer) => {
                if (buyer && buyer._id){
                    this.search.buyer = buyer._id;
                }
                else {
                    this.search.buyer = null;
                }
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;
            
            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = Deals;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Deals subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("dealsTrackingForModerator",
                    () => {
                        return [
                            this.getReactively('limit.count'),
                            {
                                buyer: this.getReactively('search.buyer'),
                                seller: this.getReactively('search.seller'),
                                status: parseInt(this.getReactively('search.status'))
                            }
                        ];
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                        },
                        onStop: () => {
                            $scope.mongoQueryReady = true;
                        }
                    }
                )
            );

            /**
             * User for management subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                username: [
                                    this.getReactively('userLoginSeller'),
                                    this.getReactively('userLoginBuyer')
                                ]
                            }
                        ];
                    }
                )
            );

            /**
             * Deals collection subscribe
             * @private
             */
            _dealsCollection = () => {
                return Deals.find({});
            };

            /**
             * Users collection subscribe
             * @private
             */
            _usersCollection = () => {
                return Meteor.users.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                deals: _dealsCollection,
                users: _usersCollection
            });
        }
    }

});