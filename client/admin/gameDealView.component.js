angular.module('gameShopApp').directive('adminGameDealView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/gameDealView.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $reactive, $controller, $location, gettext, library) {
            $reactive(this).attach($scope);

            /**
             * Subscription handles
             * @type {Array}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {Array}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Game account for money admin
             */
            this.subscriptionHandles.push(
                this.subscribe("gameAccountDealForModerator",
                    () => {
                        return [
                            $stateParams.id
                        ];
                    }
                )
            );

            /**
             * Deals id
             * @type {null}
             */
            this.dealsId = null;

            /**
             * Game account for money admin
             */
            this.subscriptionHandles.push(
                this.subscribe("dealsTrackingForModerator",
                    () => {
                        return [
                            null,
                            {
                                targetForSale: this.getReactively("gameAccount._id"),
                                buyer: this.getReactively("gameAccount.buyer"),
                                seller: this.getReactively("gameAccount.creator"),
                                status: Common.constants.deal.status.inProcess
                            }
                        ];
                    },
                    {
                        onReady: () => {
                            this.dealsId = Deals.findOne()._id;
                        }
                    }
                )
            );

            /**
             * Deals collection subscribe
             * @private
             */
            _gameAccountCollection = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccount: _gameAccountCollection
            });

            /**
             * @type {{common: *, withManager: *}}
             */
            this.chatType = {
                common: gettext("Общий"),
                chatWithBuyer: gettext("С покупателем"),
                chatWithSeller: gettext("С продавцом")
            };

            /**
             * Init chat data
             */
            this.chat = {
                createCommentMethodName: "gameAccountSalesCreateComment",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "userSales",
                subscribeMethod: _gameAccountCollection,
                commentKey: "chatSelling",
                commentType: Common.constants.complaint.objectType.commentType.game
            };

            /**
             * Init private chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
             */
            this.chatWithBuyer = {
                createCommentMethodName: "gameAccountSalesBuyerManager",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "userPurchases",
                subscribeMethod: _gameAccountCollection,
                commentKey: "chatSellingBuyerManager",
                commentType: Common.constants.complaint.objectType.commentType.game
            };
            
            /**
             * Init private chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
             */
            this.chatWithSeller = {
                createCommentMethodName: "gameAccountSalesSellerManager",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "userSales",
                subscribeMethod: _gameAccountCollection,
                commentKey: "chatSellingSellerManager",
                commentType: Common.constants.complaint.objectType.commentType.game
            };

            /**
             * Set success status on deal
             */
            this.successDeal = (ev) => {
                library.system.confirmDialog(
                    gettext('Вы уверены?'),
                    gettext('Подтвердите действие, сделка точно прошла успешно?')
                ).then(() => {
                    Deals.update(
                        {
                            _id: this.dealsId
                        },
                        {
                            $set: {
                                status: Common.constants.deal.status.success
                            }
                        },
                        (err, data) => {
                            if(!err) {
                                $location.path("/game-deal");
                            }
                            else
                                library.system.error.handlers(err);
                        }
                    );
                }, function() {});
            };

            /**
             * Set fail status on deal
             */
            this.failDeal = (ev) => {
                library.system.confirmDialog(
                    gettext('Вы уверены?'),
                    gettext('Подтвердите действие, сделка точно провалена?')
                ).then(() => {
                    Deals.update(
                        {
                            _id: this.dealsId
                        },
                        {
                            $set: {
                                status: Common.constants.deal.status.fail
                            }
                        },
                        (err, data) => {
                            if(!err) {
                                $location.path("/game-deal");
                            }
                            else
                                library.system.error.handlers(err);
                        }
                    );
                }, function() {});
            };

        }
    }
});