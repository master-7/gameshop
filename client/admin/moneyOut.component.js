angular.module('gameShopApp').directive('adminMoneyOut', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/moneyOut.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $reactive, $controller) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Search conditions
             * @type {{_id: null, username: null}}
             */
            this.search = {
                _id: null,
                creator: null,
                status: null
            };

            /**
             * Autocomplete search field
             * @param user
             */
            this.selectedItemChange = (user) => {
                if(user && user._id)
                    this.search.creator = user._id;
            };

            /**
             * User login for search
             * @type {null}
             */
            this.userLogin = null;

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = PayOutBid;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * user's payOutBids subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userPayOutBidsForAdmin",
                    () => {
                        return [
                            this.getReactively('limit.count'),
                            {
                                _id: this.getReactively('search._id'),
                                creator: this.getReactively('search.creator'),
                                status: parseInt(this.getReactively('search.status'))
                            }
                        ];
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                        },
                        onStop: () => {
                            $scope.mongoQueryReady = true;
                        }
                    }
                )
            );

            /**
             * user's payOutBids subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                username: this.getReactively('userLogin')
                            }
                        ];
                    }
                )
            );

            /**
             * Payout collection subscribe
             * @private
             */
            _userPayOutBidsCollection = () => {
                return PayOutBid.find({});
            };

            /**
             * Users collection subscribe
             * @private
             */
            _usersCollection = () => {
                return Meteor.users.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                userPayOutBids: _userPayOutBidsCollection,
                users: _usersCollection
            });
        }
    }
});