angular.module('gameShopApp').directive('adminCreateUser', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/createUser.html',
        controllerAs: 'vm',
        controller: function ($scope, $state, library) {

            /**
             * Inject to view roles types
             * @type {Common.constants.roles.types|{admin, moderator, user}}
             */
            this.roleTypes = Common.constants.roles.types;

            /**
             * Registration data
             * @type {{email: string, password: string, profile: {name: string, surname: string}}}
             */
            this.credentials = {
                username: '',
                registration: '',
                password: '',
                profile: {
                    name: '',
                    surname: ''
                },
                role: Common.constants.roles.types.user
            };

            /**
             * Registration action
             */
            this.create = () => {
                Meteor.call("createUserForAdmin", this.credentials, (err, data) => {
                    if(err) {
                        let formError = [];
                        if(err.error && err.error == Common.constants.errorCode.http.forbidden) {
                            switch (err.reason) {
                                case Common.constants.errorCode.registration.loginExist:
                                    formError.push({
                                        name: "username",
                                        type: Common.constants.schema.errorTypes.registration.username.userExist
                                    });
                                    break;
                            }
                            if(formError) {
                                library.system.error.formValidate(
                                    $scope.userForm,
                                    formError
                                )
                            }
                        }
                        library.system.error.handlers(err);
                        return false;
                    }
                    else {
                        $state.go('admin/user-management');
                    }
                });
            }
        }
    }
});