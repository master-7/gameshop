angular.module('gameShopApp').directive('dealsView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/admin/dealView.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $reactive, $controller) {
            $reactive(this).attach($scope);

            /**
             * Deal type inject to view
             */
            this.dealsType = Deals.types;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * user's payOutBids subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("dealsTrackingForModerator",
                    () => {
                        return [
                            null,
                            {
                                _id: $stateParams.id
                            }
                        ];
                    }
                )
            );

            /**
             * User's deals subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userForManagement",
                    () => {
                        return [
                            null,
                            {
                                username: [
                                    this.getReactively('deal.seller'),
                                    this.getReactively('deal.buyer')
                                ]
                            }
                        ];
                    }
                )
            );

            /**
             * Deals collection subscribe
             * @private
             */
            _dealCollection = () => {
                return Deals.findOne({});
            };

            /**
             * Users collection subscribe
             * @private
             */
            _usersCollection = () => {
                return Meteor.users.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                deal: _dealCollection,
                users: _usersCollection
            });
        }
    }
});