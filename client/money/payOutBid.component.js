angular.module('gameShopApp').directive('payOutBid', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/money/payOutBid.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive, $location, library) {
            $reactive(this).attach($scope);

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Flag query process
             * @type {boolean}
             */
            this.queryProcess = false;

            /**
             * moneyOutBid's structure (data)
             * @type {{sum: null, accountType: null, accountNumber: null}}
             */
            this.newBid = {
                accountType: null,
                accountNumber: null,
                sum: null
            };

            /**
             * Data for validate
             */
            this.validateData = Common.constants.schema.validate.payOutBid;

            /**
             * Selected wallet value
             * @type {null}
             */
            this.selectedWallet = null;

            /**
             * Wallet types
             */
            this.wallets = Common.constants.wallet;

            /**
             * Subscribe on moneyAccount
             */
            this.subscribe("moneyAccount");

            /**
             * Subscribe on wallets
             */
            this.subscribe("userWallets");

            /**
             * Getter for all user's wallet collection
             * @returns {T|*|{}|175|any|Mongo.Cursor}
             * @private
             */
            _getWallets = () => {
                return Wallets.find({}, {
                    sort: {
                        accountType: 1
                    }
                });
            };

            /**
             * Getter for user's yandex accounts collection only
             * @returns {T|*|{}|175|any|Mongo.Cursor}
             * @private
             */
            _getYandex = () => {
                return Wallets.find(
                    {
                        accountType: Common.constants.wallet.types.yandexMoney
                    },
                    {
                        sort: {
                            accountType: 1
                        }
                    }
                );
            };

            /**
             * Getter for user's robokassa accounts collection only
             * @returns {T|*|{}|175|any|Mongo.Cursor}
             * @private
             */
            _getRobokassa = () => {
                return Wallets.find(
                    {
                        accountType: Common.constants.wallet.types.robokassa
                    },
                    {
                        sort: {
                            accountType: 1
                        }
                    }
                );
            };

            /**
             * Getter for user's card accounts collection only
             * @returns {T|*|{}|175|any|Mongo.Cursor}
             * @private
             */
            _getCards = () => {
                return Wallets.find(
                    {
                        accountType: Common.constants.wallet.types.card
                    },
                    {
                        sort: {
                            accountType: 1
                        }
                    }
                );
            };

            /**
             * Money account collection
             * @returns {any}
             * @private
             */
            _moneyAccountData = () => {
                return MoneyAccount.findOne({});
            };

            /**
             * Inject collection cursor to view
             */
            this.helpers({
                userWallets: _getWallets,
                yandexCollection: _getYandex,
                robokassaCollection: _getRobokassa,
                cardCollection: _getCards,
                moneyAccount: _moneyAccountData
            });

            /**
             * If not exist money - redirect to money index
             */
            if(!this.moneyAccount || !this.moneyAccount.sum || this.moneyAccount.sum <= 0) {
                $location.path("/money/account/index").replace();
            }

            /**
             * Event handler in select wallets from dropdown list
             */
            this.bindValueOnBid = () => {
                if(this.selectedWallet) {
                    this.newBid.accountType = this.selectedWallet.accountType;
                    this.newBid.accountNumber = this.selectedWallet.accountNumber;
                }
            };

            /**
             * If user change wallets requisites clear dropdown list
             */
            this.clearSelect = () => {
                this.selectedWallet = null;
            };

            /**
             * Check wallets for exist
             * @returns {*}
             */
            this.checkWallets = () => {
                return this.notEmptySection(this.yandexCollection) || 
                    this.notEmptySection(this.robokassaCollection) || 
                    this.notEmptySection(this.cardCollection);
            };

            /**
             * Delete selected wallet action handler
             */
            this.deleteSelectedWallet = () => {
                Wallets.remove(
                    {
                        _id: this.selectedWallet._id
                    },
                    (error, result) => {
                        if(error) {
                            library.system.error.handlers(error);
                            return;
                        }
                        this.selectedWallet = null;
                        this.newBid.accountType = null;
                        this.newBid.accountNumber = null;
                    }
                );
            };

            /**
             * Creates new payOutBid
             */
            this.createOutMoneyBid = () => {
                this.queryProcess = true;
                if(!this.selectedWallet) {
                    Wallets.insert({
                        accountType: this.newBid.accountType,
                        accountNumber: this.newBid.accountNumber
                    });
                }
                PayOutBid.insert(this.newBid, (error, result) => {
                    this.queryProcess = false;
                    if (error) {
                        library.system.error.formValidate(
                            $scope.payOutBidForm,
                            PayOutBid.simpleSchema().namedContext().invalidKeys()
                        );
                    }
                    else {
                        library.system.toast('/packages/gameshop-browser/client/main/toast/payOutAddSuccess.toast.html');
                        $location.path('money/account/index').replace();
                    }
                });
            };
        }
    }
});