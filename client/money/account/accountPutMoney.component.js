angular.module('gameShopApp').directive('accountPutMoney', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/money/account/accountPutMoney.html',
        controllerAs: 'accountPutMoney',
        controller: function ($scope, $reactive, $window) {
            $reactive(this).attach($scope);

            this.subscribe("moneyAccount");

            _moneyAccountData = () => {
                return MoneyAccount.findOne({});
            };

            this.helpers({
                moneyAccount: _moneyAccountData
            });

            this.yandexMoneyAuth = () => {
                Meteor.call('getYandexMoneyClientCode', function (error, url) {
                    if(error) {
                        console.log('Error YandexMoney auth!');
                    }
                    $window.location.href = url;
                });
            };
        }
    }
});