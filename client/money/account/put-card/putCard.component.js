angular.module('gameShopApp').directive('putCard', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/money/account/paymentRequest.html',
        controllerAs: 'paymentRequest',
        controller: function ($scope, $state, $stateParams, $reactive, $window, enums) {
            $reactive(this).attach($scope);

            this.error = false;

            this.queryProcess = false;

            this.sum = 100;

            /**
             * If access token is true - start pay in
             */
            this.startPay = () => {
                Meteor.call('cardPay', this.sum, (error, result) => {
                    if(error) {
                        console.log('Error put card!', error);
                        this.error = error;
                    }
                    else {
                        if(result.data.status == Common.constants.payment.card.status.authRequired) {
                            let url = result.data.acs_uri + "?";
                            for(let key in result.data.acs_params) {
                                url += `${key}=${(result.data.acs_params[key])}&`;
                            }
                            $window.location.href = url;
                        }
                        else {
                            console.log('Error put card!', result);
                            this.error = result;
                        }
                    }
                });
            };
        }
    }
});