angular.module('gameShopApp').directive('putCardSuccess', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $state, $stateParams, $reactive) {
            $reactive(this).attach($scope);

            this.error = false;

            if($stateParams.transactionsId)
                Meteor.call("cardPayChangeStatusSuccess", $stateParams.transactionsId, (error, result) => {
                    if(error) {
                        library.system.error.handlers(error);
                        console.log('Error put card!', error);
                        this.error = error;
                    }
                    else {
                        $state.go('money/account/index');
                    }
                });
            else
                this.error = true;
        }
    }
});