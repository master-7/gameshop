angular.module('gameShopApp').directive('putCardFail', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/money/account/putCardFail.html',
        controller: function ($scope, $state, $stateParams, $reactive, $timeout) {
            $reactive(this).attach($scope);

            this.error = false;

            if($stateParams.transactionsId)
                Meteor.call("cardPayChangeStatusFail", $stateParams.transactionsId, (error, result) => {
                    if(error) {
                        console.log('Error put card!', error);
                        this.error = error;
                    }
                    else {
                        $timeout(() => {
                            $state('/money/account/index');
                        }, 5000);
                    }
                });
            else
                this.error = true;
        }
    }
});