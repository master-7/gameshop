angular.module('gameShopApp').directive('accountIndex', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/money/account/accountIndex.html',
        controllerAs: 'vm',
        controller: function ($scope, $reactive, $controller) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = PayOutBid;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe on moneyAccounts
             */
            this.subscriptionHandles.push(
                this.subscribe("moneyAccount")
            );

            /**
             * Money account collection
             * @returns {any}
             * @private
             */
            _moneyAccountData = () => {
                return MoneyAccount.findOne({});
            };

            /**
             * user's payOutBids subscription
             */
            this.subscriptionHandles.push(
                this.subscribe("userPayOutBids", 
                    () => {
                        return [
                            this.getReactively('limit.count')
                        ];
                    }
                )
            );

            /**
             * PayOutBids Getter
             * @private
             */
            _userPayOutBids = () => {
                return PayOutBid.find({});
            };

            this.helpers({
                moneyAccount: _moneyAccountData,
                userPayOutBids: _userPayOutBids
            });
        }
    }
});