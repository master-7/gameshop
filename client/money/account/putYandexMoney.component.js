angular.module('gameShopApp').directive('putYandexMoney', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/money/account/putYandexMoney.html',
        controllerAs: 'putYandexMoney',
        controller: function ($scope, $state, $stateParams, $reactive, enums) {
            $reactive(this).attach($scope);

            this.error = false;

            this.queryProcess = false;

            this.options = {
                "amount_due": "100"
            };

            this.access_token = false;

            /**
             * If code exist - check access_token
             * Else - get client code
             */
            if($stateParams.code) {
                Meteor.call('getYandexMoneyAccessToken', $stateParams.code, (error, data) => {
                    if(error) {
                        console.log('Error YandexMoney getAccessToken!', error);
                    }
                    else {
                        this.access_token = true;
                    }
                });
            }
            else {
                Meteor.call('getYandexMoneyClientCode', (error, data) => {
                    if(error) {
                        console.log('Error YandexMoney getYandexMoneyClientCode!', error);
                    }
                });
            }

            /**
             * If access token is true - start pay in
             */
            this.startPay = () => {
                if(this.access_token) {
                    Meteor.call('yandexMoneyPayin', this.options, (error, data) => {
                        if(error) {
                            library.system.error.handlers(error);
                            console.log('Error YandexMoney auth!', error);
                            this.error = true;
                        }
                        if(data.status != Common.constants.payment.transactionsStatus.success) {
                            library.system.toast('/packages/gameshop-browser/client/main/error/400.toast.html');
                            console.log('Error YandexMoney auth!', data);
                            this.error = true;
                        }
                        $state.go('money/account/index');
                    });
                }
                else {
                    $state.go('money/account/index');
                }
            };
        }
    }
});