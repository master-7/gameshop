angular.module('gameShopApp').directive('putRobokassa', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/money/account/putRobokassa.html',
        controllerAs: 'putRobokassa',
        controller: function ($scope, $state, $stateParams, $reactive, $window) {
            $reactive(this).attach($scope);

            this.error = false;

            this.queryProcess = false;

            this.sum = 100;

            /**
             * If access token is true - start pay in
             */
            this.startPay = () => {
                Meteor.call('rabokassaGenerateMerchantLink', this.sum, (error, data) => {
                    if(error) {
                        this.error = error;
                        return;
                    }
                    $window.location.href = data;
                });
            };
        }
    }
});