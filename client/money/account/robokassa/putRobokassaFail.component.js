angular.module('gameShopApp').directive('putRobokassaFail', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/money/account/putRobokassaFail.html',
		controllerAs: 'vm',
		controller: function ($scope, $state, $stateParams, $reactive) {
			$reactive(this).attach($scope);

			this.error = false;

			Meteor.call('robokassaFailPayment',
				$stateParams.InvId, $stateParams.OutSum, $stateParams.SignatureValue,
				(error, data) => {
					if(error) {
						this.error = error;
						return;
					}
					$state.go('money/account/index');
				}
			);
		}
	}
});