angular.module('gameShopApp').directive('restorePass', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/account/restorePass.html',
		controllerAs: 'vm',
		controller: function ($scope, $state, $stateParams, $reactive, library) {
			$reactive(this).attach($scope);

			/**
			 * @type {boolean}
			 */
			this.queryProcess = false;

			/**
			 * @type {boolean}
			 */
			this.tokenExist = false;

			if($stateParams.username && $stateParams.token) {
				this.tokenExist = true;

				/**
				 * @type {string}
				 */
				this.newPass = "";

				/**
				 * Change pass action
				 */
				this.changePassword = () => {
					this.queryProcess = true;
					Meteor.call("changePassByToken",
						$stateParams.username,
						$stateParams.token,
						this.newPass,
						(err, data) => {
							this.queryProcess = false;
							if(err)
								library.system.error.handlers(err);
							else {
								library.system.toast('/packages/gameshop-browser/client/main/toast/restoreReady.html');
								$state.go('login');
							}
						}
					)
				};
			}
			else {
				/**
				 * @type {string}
				 */
				this.username = "";

				/**
				 * Send on email restore pass notification
				 */
				this.sendEmail = () => {
					this.queryProcess = true;
					Meteor.call("createRestorePassQuery",
						this.username,
						(err, data) => {
							this.queryProcess = false;
							if(err)
								library.system.error.handlers(err);
							else {
								library.system.toast('/packages/gameshop-browser/client/main/toast/restorePassLinkSend.html');
								$state.go('public');
							}
						}
					)
				};
			}
		}
	}
});