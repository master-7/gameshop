angular.module('gameShopApp').directive('editAccount', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/account/edit.html',
        controllerAs: 'vm',
        controller: function ($scope, $reactive, $state, library) {
            $reactive(this).attach($scope);

            /**
             * Flag query process
             * @type {boolean}
             */
            this.queryProcess = false;

            /**
             * Change avatar object
             * @type {{images: string}}
             */
            this.changeAvatar = {
                images: ""
            };

            /**
             * Get current user scalar
             * @returns {any|159|*|{}}
             * @private
             */
            _currentUserObject = () => {
                let user = Meteor.user();
                if(user)
                    return user;
                else {
                    $state.go('login');
                }
            };

            /**
             * Inject avatars collection and changeable user's data to view
             */
            this.helpers({
                userData: _currentUserObject
            });

            /**
             * Save profile event handler
             */
            this.saveProfile = () => {
                Meteor.call("changeUserProfile", this.userData, (error, data) => {
                    if(error) {
                        if(error.error == 400)
                        library.system.error.formValidate(
                            $scope.userForm,
                            [{
                                name: "username",
                                type: "exist"
                            }]
                        );
                    }
                    else {
                        if(this.userData.password)
                            $state.go('login');
                        else
                            $state.go('public');
                    }
                });
            };

            /**
             * Check user email
             * @returns {boolean}
             */
            this.checkUserEmail = () => {
                return !!this.userData.profile.verifyEmail;
            };

            /**
             * Check verification message on user
             * @todo: drop it - is deprecated
             * @returns {boolean}
             */
            this.checkVerifyMsgSend = () => {
                return !!this.userData.profile.verifyEmailSend;
            };

            /**
             * Send the verify action user
             */
            this.sendEmailVerifyActions = () => {
                Meteor.call("createVerifyEmailQuery",
                    (error, data) => {
                        if(!error) {
                            library.system.toast(
                                '/packages/gameshop-browser/client/main/toast/verifyDataSendOnEmail.toast.html'
                            );
                        }
                        else {
                            library.system.error.handlers(error);
                        }
                    }
                );
            };

            /**
             * Watch for the change avatar
             */
            $scope.$watch(
                () => {
                    return this.changeAvatar.images;
                },
                (newId, oldId) => {
                    if(newId != oldId) {
                        Meteor.call("changeUserAvatar", newId, (error, data) => {
                            if (error) {
                                library.system.error.handlers(error);
                            }
                        });
                    }
                }
            );
        }
    }
});