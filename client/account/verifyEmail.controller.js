/**
 * Prototype controller
 */
angular.module('gameShopApp').controller('verifyEmailCtrl', ['$scope', '$state', '$stateParams', 'library',
    function ($scope, $state, $stateParams, library) {
        Meteor.call("verifyEmailForUser",
            $stateParams.username,
            $stateParams.verifyEmailHash,
            (error, data) => {
                if(!error) {
                    library.system.toast(
                        '/packages/gameshop-browser/client/main/toast/emailConfirmedSuccess.toast.html'
                    );
                    $state.go("account/edit");
                }
                else {
                    library.system.error.handlers(error);
                    $state.go("public");
                }
            }
        );
    }
]);
