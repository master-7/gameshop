angular.module("gameShopApp").config(($urlRouterProvider, $stateProvider, $locationProvider, gettext, enums) => {
    $locationProvider.html5Mode(true);

    /**
     * Check user permission for route
     */
    let checkUserPermission = {
        /**
         * Route permission auth required
         * @param $q
         * @returns {*}
         */
        authRequired: ($q) => {
            if (Meteor.userId() == null) {
                return $q.reject(enums.authErrors.authRequired);
            }
            else {
                return $q.resolve();
            }
        },
        /**
         * Route permission only for unauthorized
         * @param $q
         * @returns {*}
         */
        onlyForUnauthorized: ($q) => {
            if (Meteor.userId() == null) {
                return $q.resolve();
            }
            else {
                return $q.reject(enums.authErrors.onlyForUnauthorized);
            }
        },
        /**
         * Route permission only for admin
         * @param $q
         * @returns {*}
         */
        onlyForAdmin: ($q) => {
            if (Common.system.permissions.isAdmin(Meteor.userId())) {
                return $q.resolve();
            }
            else {
                return $q.reject(enums.authErrors.onlyForAdmin);
            }
        },
        /**
         * Route permission only for moderator
         * @param $q
         * @returns {*}
         */
        onlyForModerator: ($q) => {
            if (Common.system.permissions.isModerator(Meteor.userId())) {
                return $q.resolve();
            }
            else {
                return $q.reject(enums.authErrors.onlyForModerator);
            }
        },
        /**
         * Route permission only for site-manager
         * @param $q
         * @returns {*}
         */
        onlyForSiteManager: ($q) => {
            if (Common.system.permissions.isSiteManager(Meteor.userId())) {
                return $q.resolve();
            }
            else {
                return $q.reject(enums.authErrors.onlyForSiteManager);
            }
        },
        /**
         * Route permission only for moderator
         * @param $q
         * @returns {*}
         */
        onlyForMoneyAdmin: ($q) => {
            if (Common.system.permissions.isMoneyAdmin(Meteor.userId())) {
                return $q.resolve();
            }
            else {
                return $q.reject(enums.authErrors.onlyForModerator);
            }
        }
    };

    $stateProvider
        .state('public', {
            url: '/public?category&type',
            template: '<public-accounts></public-accounts>',
            ncyBreadcrumb: {
                label: gettext("Главная")
            }
        })
        .state('game/view', {
            url: '/game/view/:id?scrollTo',
            template: '<game-view></game-view>',
            ncyBreadcrumb: {
                parent: 'public',
                label: gettext("Просмотр аккаунта")
            }
        })
        .state('rules', {
            url: '/rules?scrollTo',
            template: '<rules></rules>',
            ncyBreadcrumb: {
                parent: 'public',
                label: gettext("Правила пользования")
            }
        })
        .state('faq', {
            url: '/faq?scrollTo',
            template: '<faq></faq>',
            ncyBreadcrumb: {
                parent: 'public',
                label: gettext("Часто задаваемые вопросы")
            }
        })
        .state('partnership', {
            url: '/partnership?scrollTo',
            template: '<partnership></partnership>',
            ncyBreadcrumb: {
                parent: 'public',
                label: gettext("Сотрудничество")
            }
        })
        .state('team', {
            url: '/team',
            template: '<team></team>',
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Наша комманда")
			}
        })
        .state('contacts', {
            url: '/contacts',
            template: '<contacts></contacts>',
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Контакты")
			}
        })
        .state('ticket', {
            url: '/ticket',
            template: '<ticket></ticket>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Обращения в техподдержку")
			}
        })
        .state('ticket/create', {
            url: '/ticket/create',
            template: '<ticket-create></ticket-create>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'ticket',
				label: gettext("Создать обращение в техподдержку")
			}
        })
        .state('ticket/edit', {
            url: '/ticket/edit/:id',
            template: '<ticket-edit></ticket-edit>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
                parent: 'ticket',
				label: gettext("Редактировать обращение")
			}
        })
        .state('ticket/view', {
            url: '/ticket/view/:id',
            template: '<ticket-view></ticket-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
                parent: 'ticket',
				label: gettext("Просмотр обращения")
			}
        })
        .state('marks/game', {
            url: '/marks/game',
            template: '<marks-game></marks-game>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Игры в закладках")
			}
        })
        .state('marks/thing', {
            url: '/marks/thing',
            template: '<marks-thing></marks-thing>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Вещи в закладках")
			}
        })
        .state('game/new', {
            url: '/game/new',
            template: '<new-game></new-game>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Продать игровой аккаунт")
			}
        })
        .state('game/edit', {
            url: '/game/edit/:id',
            template: '<edit-game></edit-game>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Редактировать аккаунт")
			}
        })
        .state('game/user-sales', {
            url: '/game/user-sales',
            template: "<user-sales></user-sales>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Мои продажи")
			}
        })
        .state('game/user-sales/view', {
            url: '/game/user-sales/view/:id?scrollTo',
            template: "<user-sales-view></user-sales-view>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
                parent: 'game/user-sales',
                label: gettext("Продаваемый аккаунт")
            }
        })
        .state('game/user-purchases', {
            url: '/game/user-purchases',
            template: "<user-purchases></user-purchases>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Мои покупки")
			}
        })
        .state('game/user-purchases/view', {
            url: '/game/user-purchases/view/:id?scrollTo',
            template: "<user-purchases-view></user-purchases-view>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'game/user-purchases',
				label: gettext("Просмотр покупки")
			}
        })
        .state('game/on-moderation', {
            url: '/game/on-moderation',
            template: "<game-account-on-moderation></game-account-on-moderation>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("На проверке")
			}
        })
        .state('game/on-moderation/view', {
            url: '/game/on-moderation/view/:id?scrollTo',
            template: "<game-account-on-moderation-view></game-account-on-moderation-view>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'game/on-moderation',
				label: gettext("Просмотр аккаунта")
			}
        })
        .state('game/sold', {
            url: '/game/sold',
            template: "<game-sold></game-sold>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Проданные игры")
			}
        })
        .state('game/returned', {
            url: '/game/returned',
            template: "<game-returned></game-returned>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Требуют правки")
			}
        })
        .state('game/returned/view', {
            url: '/game/returned/view/:id',
            template: "<game-returned-view></game-returned-view>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'game/returned',
				label: gettext("Просмотр аккаунта")
			}
        })
        .state('thing/public', {
            url:'/thing/public',
            template: '<thing-public></thing-public>',
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Вещи на продаже")
			}
        })
        .state('thing/new', {
            url: '/thing/new',
            template: '<new-thing></new-thing>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Создать вещь")
			}
        })
        .state('thing/sales', {
            url: '/thing/sales',
            template: "<thing-sales></thing-sales>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Вещи на продаже")
			}
        })
        .state('thing/view', {
            url: '/thing/view/:id?scrollTo',
            template: '<thing-view></thing-view>',
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Просмотр вещи")
			}
        })
        .state('money/account/index', {
            url: '/money/account/index',
            template: "<account-index></account-index>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Просмотр счета")
			}
        })
        .state('money/account/put-money', {
            url: '/money/account/put-money',
            template: "<account-put-money></account-put-money>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'money/account/index',
				label: gettext("Пополнить счет")
			}
        })
        .state('money/account/put-money/yandex', {
            url: '/money/account/put-money/yandex?code',
            template: "<put-yandex-money></put-yandex-money>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
                parent: 'money/account/put-money',
                label: gettext("Пополнить счет с yandex money")
            }
        })
        .state('money/account/put-money/robokassa', {
            url: '/money/account/put-money/robokassa',
            template: "<put-robokassa></put-robokassa>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
                parent: 'money/account/put-money',
                label: gettext("Пополнить счет с robokassa")
            }
        })
        .state('money/account/put-money/robokassa/success', {
            url: '/money/account/put-money/robokassa/success?InvId&OutSum&SignatureValue',
            template: "<put-robokassa-success></put-robokassa-success>",
            ncyBreadcrumb: {
                parent: 'money/account/put-money/robokassa',
                label: gettext("Robokassa success payment")
            }
        })
        .state('money/account/put-money/robokassa/fail', {
            url: '/money/account/put-money/robokassa/fail?InvId&OutSum&SignatureValue',
            template: "<put-robokassa-fail></put-robokassa-fail>",
            ncyBreadcrumb: {
                parent: 'money/account/put-money/robokassa',
                label: gettext("Robokassa fail payment")
            }
        })
        .state('money/account/put-money/card', {
            url: '/money/account/put-money/card',
            template: "<put-card></put-card>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
                parent: 'money/account/put-money',
                label: gettext("Пополнить счет с visa, mastercard")
            }
        })
        .state('money/account/put-money/card/success', {
            url: '/money/account/put-money/card/success/:transactionsId',
            template: "<put-card-success></put-card-success>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'money/account/index',
				label: gettext("Успешно!")
			}
        })
        .state('money/account/put-money/card/fail', {
            url: '/money/account/put-money/card/fail/:transactionsId?cps_context_id',
            template: "<put-card-fail></put-card-fail>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'money/account/index',
				label: gettext("Ошибка!")
			}
        })
        .state('money/addWallet', {
            url: '/money/add-wallet',
            template: "<add-wallet></add-wallet>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'money/account/index',
				label: gettext("Добавить кошелек")
			}
        })
        .state('money/payOutBid', {
            url: '/money/pay-out-bid',
            template: "<pay-out-bid></pay-out-bid>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'money/account/index',
				label: gettext("Заявки на вывод средств")
			}
        })
        .state('login', {
            url: '/login',
            template: '<login></login>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForUnauthorized($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Вход")
			}
        })
        .state('restore', {
            url: '/restore-pass?username&token',
            template: '<restore-pass></restore-pass>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForUnauthorized($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Восстановление пароля")
			}
        })
        .state('registration', {
            url: '/registration',
            template: '<registration></registration>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForUnauthorized($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Регистрация")
			}
        })
        .state('account/edit', {
            url: '/account/edit',
            template: '<edit-account></edit-account>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Изменить профиль")
			}
        })
        .state('admin/create-user', {
            url: '/admin/create-user',
            template: '<admin-create-user></admin-create-user>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Создать пользователя")
			}
        })
        .state('admin/user-management', {
            url: '/admin/user-management',
            template: '<user-management></user-management>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Управление пользователями")
			}
        })
        .state('admin/game-management', {
            url: '/admin/game-management',
            template: '<game-management></game-management>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForModerator($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Управление аккаунтами")
			}
        })
        .state('admin/user-management/edit', {
            url: '/admin/user-management/edit/:id',
            template: '<user-management-edit></user-management-edit>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Редактировать пользователя")
			}
        })
        .state('admin/ticket', {
            url: '/admin/ticket',
            template: '<admin-ticket></admin-ticket>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Техподдержка")
			}
        })
        .state('admin/ticket/view', {
            url: '/admin/ticket/view/:id',
            template: '<admin-ticket-view></admin-ticket-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'admin/ticket',
				label: gettext("Просмотр обращения")
			}
        })
        .state('admin/free-money', {
            url: '/admin/free-money',
            template: '<free-money></free-money>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Свободные средства")
			}
        })
        .state('admin/deals', {
            url: '/admin/deals',
            template: '<deals></deals>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Сделки")
			}
        })
        .state('admin/deals/view', {
            url: '/admin/deals/view/:id',
            template: '<deals-view></deals-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'admin/deals',
				label: gettext("Просмотр сделки")
			}
        })
        .state('admin/game/view', {
            url: '/admin/game/view/:id',
            template: '<admin-game-view></admin-game-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Просмотр игры")
			}
        })
        .state('admin/game-deal', {
            url: '/admin/game-deal',
            template: '<admin-game-deal></admin-game-deal>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Просмотр игры")
			}
        })
        .state('admin/game-deal/view', {
            url: '/admin/game-deal/view/:id?scrollTo',
            template: '<admin-game-deal-view></admin-game-deal-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'admin/game-deal',
				label: gettext("Просмотр игры")
			}
        })
        .state('admin/transactions', {
            url: '/admin/transactions',
            template: '<transactions></transactions>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Транзакции")
			}
        })
        .state('admin/transaction/view', {
            url: '/admin/transaction/view/:id',
            template: '<transaction-view></transaction-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'admin/transactions',
				label: gettext("Просмотр транзакции")
			}
        })
        .state('admin/money-out', {
            url: '/admin/money-out',
            template: '<admin-money-out></admin-money-out>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Вывод средств")
			}
        })
        .state('admin/money-out/view', {
            url: '/admin/money-out/view/:id',
            template: '<admin-money-out-view></admin-money-out-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForMoneyAdmin($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'admin/money-out',
				label: gettext("Просмотр заявки на вывод средств")
			}
        })
        .state('admin/moderation-game-account', {
            url: '/admin/moderation-game-account',
            template: '<moderation-game-account></moderation-game-account>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForModerator($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Модерация игровых аккаунтов")
			}
        })
        .state('admin/moderation-game-account/view', {
            url: '/admin/moderation-game-account/view/:id?scrollTo',
            template: '<moderation-game-account-view></moderation-game-account-view>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForModerator($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'admin/moderation-game-account',
				label: gettext("Просмотр игрового аккаунта")
			}
        })
        .state('admin/category', {
            url: '/admin/category',
            template: '<admin-seo-category></admin-seo-category>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForSiteManager($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Ключевые слова в категориях")
			}
        })
        .state('/admin/category/upsert', {
            url: '/admin/category/upsert?id',
            template: '<admin-seo-category-upsert></admin-seo-category-upsert>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForSiteManager($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'admin/category',
				label: gettext("Добавить ключевые слова в категорию")
			}
        })
        .state('/admin/tags', {
            url: '/admin/tags',
            template: '<admin-seo-game-account-type></admin-seo-game-account-type>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForSiteManager($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Ключевые слова в тегах")
			}
        })
        .state('/admin/tags/upsert', {
            url: '/admin/tags/upsert?id',
            template: '<admin-seo-game-account-type-upsert></admin-seo-game-account-type-upsert>',
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.onlyForSiteManager($q);
                }
            },
            ncyBreadcrumb: {
				parent: '/admin/tags',
				label: gettext("Добавить ключевые слова в тег")
			}
        })
        .state('user/games', {
            url: '/user/games?id',
            template: "<user-games></user-games>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Игры пользователя")
			}
        })
        .state('user/things', {
            url: '/user/things?id',
            template: "<user-things></user-things>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Вещи пользователя")
			}
        })
        .state('user/profile', {
            url: '/user/profile?id',
            template: "<user-profile><user-profile>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Профиль пользователя")
			}
        })
        .state('user-tracker-jump', {
            url: '/user-tracker-jump/:id',
            template: "<user-tracker-jump></user-tracker-jump>",
            resolve: {
                currentUser: ($q) => {
                    return checkUserPermission.authRequired($q);
                }
            },
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Переход по ссылке...")
			}
        })
        .state('user/verify-email', {
            url: '/user/verify-email/:username/:verifyEmailHash',
            controller: "verifyEmailCtrl",
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("Проверяем...")
			}
        })
        .state('404', {
            url: '/404',
            templateUrl: "/packages/gameshop-browser/client/main/error-pages/404.html",
            ncyBreadcrumb: {
				parent: 'public',
				label: gettext("404")
			}
        })
        .state('otherwise', {
            url: '/',
            template: `
                <div layout="column" layout-align="center center" style="width: 100%; height: 80%;">
                    <md-progress-circular md-mode="indeterminate" md-diameter="128"></md-progress-circular>
                </div>
            `,
            controller($location, $timeout) {
                $timeout(() => {
                    if (Meteor.userId() == null) {
                        if($location.url() == "/")
                            window.location.replace(Meteor.absoluteUrl('landing/index.html'));
                    }
                    else {
                        //Check user role and get route
                        let userRoles = Roles.getRolesForUser(Meteor.userId(), Roles.GLOBAL_GROUP);
                        let url = function () {
                            if (userRoles.inArray(Common.constants.roles.types.user))
                                return "/public";
                            if (userRoles.inArray(Common.constants.roles.types.admin))
                                return "/admin/user-management";
                            if (userRoles.inArray(Common.constants.roles.types.moderator))
                                return "/admin/moderation-game-account";
                            return "public";
                        }();
                        $location.path(url).replace();
                    }
                }, 2000);
            },
            ncyBreadcrumb: {
                label: gettext("Переадресация")
            }
        });
    $urlRouterProvider.otherwise("/");
})
    .run(function ($rootScope, $state, library, enums) {
        $rootScope.$on('$stateChangeStart',
            (event, toState, toParams, fromState, fromParams, options) => {
                if (toParams && toParams.scrollTo) {
                    library.services.scrollToWithAnimation(toParams.scrollTo);
                }

                /**
                 * Init page global var
                 * @type {{}}
                 */
                $rootScope.page = { };

                /**
                 * Set page title if exist in route
                 */
                if (toState.ncyBreadcrumb)
                {
                    $rootScope.page = {
                        title: toState.ncyBreadcrumb.label
                    };
                }
            }
        );
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            switch (error) {
                case enums.authErrors.authRequired:
                    $state.go('login');
                    break;
                case enums.authErrors.onlyForUnauthorized:
                    $state.go('public');
                    break;
                case enums.authErrors.onlyForAdmin:
                case enums.authErrors.onlyForModerator:
                case enums.authErrors.onlyForSiteManager:
                    library.system.error.handlers(Common.constants.errorCode.http.forbidden);
                    $state.go('public');
                    break;
            }
        });
    });
