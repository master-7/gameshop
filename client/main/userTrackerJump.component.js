angular.module('gameShopApp').directive('userTrackerJump', function () {
    return {
        restrict: 'E',
        controllerAs: 'vm',
        template: `<div translate>Переходим на событие...</div>`,
        controller: function ($scope, $stateParams, $reactive, $location) {
            $reactive(this).attach($scope);

            /**
             * Subscribe active user tracker event
             */
            this.subscribe("userTrackerForCreator",
                () => {
                    return [$stateParams.id];
                },
                {
                    onReady: () => {
                        let userTracker = UserTracker.findOne({
                            _id: $stateParams.id
                        });

                        let path = "/public";

                        if(userTracker) {
                            UserTracker.update(
                                {
                                    _id: $stateParams.id
                                },
                                {
                                    $set: {
                                        status: Common.constants.userTracker.status.viewed
                                    }
                                }
                            );
                            switch (userTracker.type) {
                                case Common.constants.userTracker.type.moderation.createdGame:
                                    path = `/game/on-moderation/view/${userTracker.data._id}`;
                                    break;
                                case Common.constants.userTracker.type.moderation.success:
                                    path = `/game/view/${userTracker.data._id}`;
                                    break;
                                case Common.constants.userTracker.type.moderation.returnGame:
                                    path = `/game/on-moderation/view/${userTracker.data._id}`;
                                    break;
                                case Common.constants.userTracker.type.moderation.fail:
                                    path = `/game/on-moderation/view/${userTracker.data._id}`;
                                    break;
                                case Common.constants.userTracker.type.moderation.message:
                                    path = `/game/on-moderation/view/${userTracker.data._id}?scrollTo=comment-${userTracker.data.commentId}`;
                                    break;
                                case Common.constants.userTracker.type.ticket.message:
                                    path = `/ticket/view/${userTracker.data._id}`;
                                    break;
                                case Common.constants.userTracker.type.deal.created:
                                    path = `/game/user-sales/view/${userTracker.data._id}`;
                                    break;
                                case Common.constants.userTracker.type.deal.message:
                                    if (Common.system.permissions.isModerator(Meteor.userId())) {
                                        path = `/admin/game-deal/view/${userTracker.data._id}?scrollTo=comment-${userTracker.data.commentId}`;
                                    }
                                    else {
                                        if (userTracker.data.creator == Meteor.userId()) {
                                            path = `/game/user-sales/view/${userTracker.data._id}?scrollTo=comment-${userTracker.data.commentId}`;
                                        }
                                        if (userTracker.data.buyer == Meteor.userId()) {
                                            path = `/game/user-purchases/view/${userTracker.data._id}?scrollTo=comment-${userTracker.data.commentId}`;
                                        }
                                    }
                                    break;
                            }
                        }
                        location.replace(path);
                    }
                }
            );
        }
    }
});
