angular.module('gameShopApp').controller("errorToastCtrl", ($scope, $mdDialog, $mdToast, error) => {
    /**
     * Close toast action
     */
    $scope.closeToast = () => {
        $mdToast.hide();
    };
    /**
     * Create error event on system
     */
    Meteor.call("sendError", error, (error, data) => {
        $scope.eventId = data;
    });
    /**
     * Open more info about errors
     */
    $scope.openMoreInfo = () => {
        $scope.closeToast();
        $mdDialog.show({
            controller: ($scope, $state, library, enums, eventId) => {
                $scope.msg = "";
                /**
                 * Close dialog action
                 */
                $scope.closeDialog = () => {
                    $mdDialog.hide();
                };
                /**
                 * Create user explanations
                 */
                $scope.sendError = () => {
                    $scope.closeDialog();
                    Meteor.call("userMsg", eventId, $scope.msg, (error, data) => {
                        library.system.toast('/packages/gameshop-browser/client/main/toast/thanksForTheHelp.toast.html');
                    });
                    $state.go("public");
                };
            },
            templateUrl: '/packages/gameshop-browser/client/main/error/main.dialog.html',
            parent: angular.element(document.body),
            locals: {
                eventId: $scope.eventId
            }
        });
    };
});