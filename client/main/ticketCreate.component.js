angular.module('gameShopApp').directive('ticketCreate', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/ticketCreate.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive, $location, $timeout, library) {
            $reactive(this).attach($scope);

            /**
             * New ticket form data
             */
            this.newTicket = {
                subject: "",
                description: "",
                category: "",
                images: []
            };

            /**
             * Validate data inject to view
             */
            this.validateData = Common.constants.schema.validate.ticket;

            /**
             * Ticket category inject to view
             */
            this.ticketCategory = Common.constants.ticket.category;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * If image add start to rebuild directive
             * @type {boolean}
             */
            $scope.imageAdd = false;

            $scope.$watchCollection(
                () => {
                    return this.newTicket.images;
                },
                (newVal, oldVal) => {
                    //Rebuild
                    if(newVal) {
                        $scope.imageAdd = true;
                        $timeout(() => {
                            $scope.imageAdd = false;
                        }, 50);
                    }
                }
            );
            
            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Getter images collections
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _imagesCollections = () => {
                return Thumbs.find({});
            };

            /**
             * Subscribe to images collection
             */
            this.subscriptionHandles.push(
                this.subscribe("thumbs", () => {
                    return [
                        this.getCollectionReactively("newTicket.images")
                    ]
                })
            );
            
            /**
             * Inject collection to view
             */
            this.helpers({
                images: _imagesCollections
            });

            /**
             * Create new ticket
             */
            this.createTicket = () => {
                this.queryProcess = true;
                this.newTicket.images = this.images.map((item) => {
                    return {
                        _id: item._id,
                        name: item.name,
                        store: item.store,
                        originalStore: item.originalStore,
                        originalId: item.originalId
                    };
                });
                Ticket.insert(this.newTicket, (err, data) => {
                    if(!err) {
                        this.queryProcess = false;
                        $location.path("/ticket");
                    }
                    else
                        library.system.error.handlers(err);
                });
            };

            /**
             * Remove image event handler
             * @param id
             */
            this.removeImage = (id) => {
                this.newTicket.images.splice(
                    this.newTicket.images.indexOf(id), 1
                );
                Meteor.call("removeImage", id, this.newTicket,
                    (error, result) => {
                        if(error) {
                            this.queryProcess = false;
                            library.system.error.handlers(error);
                        }
                    }
                );
            };
        }
    }
});