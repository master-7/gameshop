angular.module('gameShopApp').directive('ticket', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/ticket.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Inject on baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = Ticket;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(
                this.subscribe("ticketForCreator",
                    () => {
                        return [
                            this.getReactively('limit.count')
                        ];
                    },
                    {
                        onReady: () => {
                            $scope.mongoQueryReady = true;
                        },
                        onStop: () => {
                            $scope.mongoQueryReady = true;
                        }
                    }
                )
            );

            /**
             * Select ticket collection
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _ticketCollection = () => {
                return Ticket.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                tickets: _ticketCollection
            });
        }
    }
});