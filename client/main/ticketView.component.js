angular.module('gameShopApp').directive('ticketView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/ticketView.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $stateParams, $reactive, $location, library) {
            $reactive(this).attach($scope);

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(
                this.subscribe("ticketForCreator", 
                    () => {
                        return [
                            null,
                            $stateParams.id
                        ];
                    },
                    {
                        onReady: () => {
                            let ticket = Ticket.findOne({_id: $stateParams.id});
                            if(ticket) {
                                this.images = library.image.filterImageCollectionForNgGallery(ticket.images);
                            }
                        }
                    }
                )
            );

            /**
             * Select ticket collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _ticketCollections = () => {
                return Ticket.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                ticket: _ticketCollections
            });

            /**
             * Init chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
             */
            this.chat = {
                createCommentMethodName: "ticketCreateComment",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "ticketForCreator",
                subscribeMethod: _ticketCollections,
                commentKey: "comments",
                commentType: Common.constants.complaint.objectType.commentType.ticket
            };

            /**
             * Close the ticket
             */
            this.close = () => {
                Ticket.update({_id: $stateParams.id},
                    {
                        $set: {
                            status: Common.constants.ticket.status.close
                        }
                    },
                    (err, data) => {
                        if(!err) {
                            $location.path("/ticket");
                        }
                        else
                            library.system.error.handlers(err);
                    }
                )
            };
        }
    }
});