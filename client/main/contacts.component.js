angular.module('gameShopApp').directive('contacts', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/main/contacts.html',
		controllerAs: 'vm',
		controller: function ($scope, $controller, $stateParams, $location, $reactive, library) {
			$reactive(this).attach($scope);
			
		}
	};
});