angular.module('gameShopApp').directive('emptyPage', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/emptyPage.html',
        controllerAs: 'vm',
        controller: function ($scope, $rootScope) {
            $rootScope.$watch("pageLoad", (newVal, oldVal) => {
                $scope.pageLoad = newVal;
            });
        }
    }
});