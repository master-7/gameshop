angular.module('gameShopApp').controller("clientErrorToastCtrl", ($scope, $mdToast, enums, error) => {
    /**
     * Close toast action
     */
    $scope.closeToast = () => {
        $mdToast.hide();
    };

    switch (error) {
        case enums.system.error.image.isTooSmall:
            $scope.imageIsTooSmall = true;
            break;
        default:
            $scope.error = true;
            break;
    }
});