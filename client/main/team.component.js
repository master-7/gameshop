angular.module('gameShopApp').directive('team', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/team.html',
        controllerAs: 'vm',
        controller: function ($scope, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Init like directive data
             */
            this.like = {
                "_dr.evil": "our-team-object-dr.evil",
                _sergmit: "our-team-object-sergmit",
                _afederigo: "our-team-object-afederigo",
                _anzor: "our-team-object-anzor",
                _julia: "our-team-object-julia",
                _murad: "our-team-object-murad",
                _artem: "our-team-object-artem",
                objectType: Common.constants.like.type.game,
                isFavorites: false
            };
        }
    };
});