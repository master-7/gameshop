angular.module('gameShopApp').directive('main', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/main.html',
        controllerAs: 'main',
        controller: function ($rootScope, $scope, $location, $mdSidenav, library) {

            /**
             * If user is blocked - logout
             */
            Accounts.onLogin(() => {
                if(Meteor.user() && Meteor.user().profile.blocked) {
                    library.system.dialog(
                        '/packages/gameshop-browser/client/main/dialog/accountIsBlocked.html'
                    );
                }
            });

            /**
             * Current user object
             * @returns {any}
             */
            this.currentUser = () => {
                return Meteor.user();
            };

            /**
             * Check login user
             * @returns {boolean}
             */
            this.isLoggedIn = () => {
                return Meteor.userId() !== null;
            };

            /**
             * Check is user
             * @returns {*}
             */
            this.isUser = () => {
                if(this.isAdmin() || this.isMoneyAdmin() || this.isModerator())
                    return false;
                return Common.system.permissions.isUser(Meteor.userId());
            };

            /**
             * Check is admin user
             * @returns {*}
             */
            this.isAdmin = () => {
                return Common.system.permissions.isAdmin(Meteor.userId());
            };

            /**
             * Check is money admin user
             * @returns {*}
             */
            this.isMoneyAdmin = () => {
                if(this.isAdmin())
                    return false;
                return Common.system.permissions.isMoneyAdmin(Meteor.userId());
            };

            /**
             * Check is site-manager
             * @returns {*}
             */
            this.isSiteManager = () => {
                if(this.isAdmin() || this.isMoneyAdmin())
                    return false;
                return Common.system.permissions.isSiteManager(Meteor.userId());
            };

            /**
             * Check is user moderator
             * @returns {*}
             */
            this.isModerator = () => {
                if(this.isAdmin() || this.isMoneyAdmin())
                    return false;
                return Common.system.permissions.isModerator(Meteor.userId());
            };

            /**
             * Logout action
             */
            this.logout = () => {
                $location.path('/public');
                Accounts.logout();
            };

            /**
             * Sidenav show/hide
             * @param menuId
             */
            this.toggleSidenav = (menuId) => {
                $mdSidenav(menuId).toggle();
            };

            /**
             * If location change success close the sidenav
             */
            $rootScope.$on('$locationChangeSuccess', (event) => {
                $mdSidenav('left').close();
            });
        }
    }
});
