angular.module('gameShopApp').directive('faq', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/main/faq.html',
		controllerAs: "vm",
		controller: function ($scope, $timeout, $location) {

			/**
			 * Scroll top flag
			 * @type {boolean}
			 */
			$scope.showScrollTop = false;

			/**
			 * Scroll event listener
			 */
			$('#main-value').scroll(function () {
				let elem = $(this)[0];

				let showScrollTop = elem.scrollTop > 300;

				if(!showScrollTop)
					$location.path('/faq').search('');

				$scope.showScrollTop = showScrollTop;
				$scope.$apply();
			});

			/**
			 * Scroll top action handler
			 */
			this.scrollTopAction = (scrollTo) => {
				$location.path('/faq').search('scrollTo='+scrollTo);
				$timeout(() => {
					let params = $location.path('/faq').search();
					if (params && params.scrollTo && params.scrollTo == "contents")
						$location.path('/faq').search('');
				});
			};

			/**
			 * Drop event listener if controller destroy
			 */
			$scope.$on('$destroy', () => {
				$('#main-value').off("scroll")
			});
		}
	};
});