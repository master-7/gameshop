angular.module('gameShopApp').directive('userTracker', function () {
    return {
        restrict: 'E',
        controllerAs: 'ut',
        controller: function ($scope, $reactive, enums, library) {
            $reactive(this).attach($scope);

            /**
             * Viewed notification _id
             * @type {Array}
             */
            let viewerNotification = [];

            /**
             * Subscribe active user tracker event
             */
            this.subscribe("userTrackerPublic");

            /**
             * User collection subscribe condition
             * @returns {DOMElement|*|{}|T|any|Cursor}
             * @private
             */
            _userTrackerCollections = () => {
                return UserTracker.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                userTracker: _userTrackerCollections
            });

            /**
             * Show notification by event
             * @param notificationData
             */
            this.showNotification = (notificationData) => {
                let checkNotification = (notificationData.status == Common.constants.userTracker.status.created)
                    && !viewerNotification.inArray(notificationData._id);
                if(checkNotification) {
                    //Only one notification
                    UserTracker.update(
                        {
                            _id: notificationData._id
                        },
                        {
                            $set: {
                                status: Common.constants.userTracker.status.viewed
                            }
                        }
                    );
                    viewerNotification.push(notificationData._id);
                    library.system.getNotificationByEvent(
                        notificationData,
                        {
                            click: (notification) => {
                                UserTracker.update(
                                    {
                                        _id: notificationData._id
                                    },
                                    {
                                        $set: {
                                            status: Common.constants.userTracker.status.viewed
                                        }
                                    }
                                );
                                notification.close();
                            },
                            close: () => {
                                UserTracker.update(
                                    {
                                        _id: notificationData._id
                                    },
                                    {
                                        $set: {
                                            status: Common.constants.userTracker.status.close
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            }
        }
    }
});
