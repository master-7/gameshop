angular.module('gameShopApp').directive('getIconByGameStatus', function ($compile, library) {
    return {
        restrict: 'E',
        scope: {
            status : '='
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("status", (newVal, oldVal) => {
                if (scope.status >= Common.constants.game.statuses.onVerification) {
                    let img = '<md-icon md-svg-icon="action:ic_alarm_24px" class="md-primary"></md-icon>';
                    switch (parseInt(scope.status)) {
                        case Common.constants.game.statuses.moderatorIsWorking:
                            img = '<md-icon md-svg-icon="action:ic_autorenew_24px" class="md-primary"></md-icon>';
                            break;
                        case Common.constants.game.statuses.verificationFail:
                            img = '<md-icon md-svg-icon="alert:ic_warning_24px" class="md-warn"></md-icon>';
                            break;
                        case Common.constants.game.statuses.isBlocked:
                            img = '<md-icon md-svg-icon="content:ic_block_24px" class="md-warn"></md-icon>';
                            break;
                    }
                    element.html($compile(img)(scope));
                }
            });
        }
    };
});