angular.module('gameShopApp').directive('avatar', function ($compile, library) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            userId: "=",
            imageId: "=",
            imageClass: "@"
        },
        link: function (scope, element, attrs) {
            let getImageWithLink = (img) => {
                return `<a href="/user/profile?id=${scope.userId}">${img}</a>`;
            };
            let img = '<md-icon class="md-avatar" aria-label="admin-deals" md-svg-icon="action:ic_face_24px"></md-icon>';
            scope.$watch("imageId", function (newVal) {
                if (typeof newVal == "string") {
                    let imgClass = "md-user-avatar";
                    if(scope.imageClass) {
                        imgClass += " " + scope.imageClass;
                    }
                    let imageLink = library.image.getImageLink(newVal);
                    library.image.checkImage(
                        imageLink,
                        () => {
                            img = `<img class="${imgClass}"
                                ng-src="${imageLink}"
                                alt="user avatar">`;
                            element.html(
                                $compile(getImageWithLink(img))(scope)
                            );
                        },
                        () => {
                            element.html(getImageWithLink(img));
                        }
                    );
                }
                else {
                    element.html(
                        $compile(
                            getImageWithLink(img)
                        )(scope)
                    );
                }
            });
        }
    };
});