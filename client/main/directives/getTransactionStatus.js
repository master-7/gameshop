angular.module('gameShopApp').directive('getTransactionStatus', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("value", () => {
                let status = '';
                if (scope.value !== '') {
                    switch (parseInt(scope.value)) {
                        case MoneyTransactions.statuses.fail:
                            status = gettext("Ошибка");
                            break;
                        case MoneyTransactions.statuses.createdRequest:
                            status = gettext("Создана");
                            break;
                        case MoneyTransactions.statuses.inProcess:
                            status = gettext("В процессе обработки");
                            break;
                        case MoneyTransactions.statuses.success:
                            status = gettext("Успешно");
                            break;
                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(status);
                    }
                    else {
                        element.html(
                            gettext("Статус: ") + status
                        );
                    }
                }
            });
        }
    };
});
