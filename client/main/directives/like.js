angular.module('gameShopApp').directive('like', function () {
    return {
        restrict: 'E',
        scope: {
            /**
             * Type object of like
             */
            objectType: '=',
            /**
             * Object id model
             */
            objectId: '=',
            /**
             * Show tooltip
             */
            isFavorites: '='
        },
        templateUrl: '/packages/gameshop-browser/client/main/directives/like.html',
        controllerAs: 'vm',
        controller: function ($scope, $reactive) {
            $reactive(this).attach($scope);
            
            if($scope.isFavorites == undefined)
                this.isFavorites = true;
            else
                this.isFavorites = $scope.isFavorites;

            /**
             * Subscribe to like collection
             */
            this.subscribe("likePublish", () => {
                return [
                    $scope.objectType,
                    $scope.objectId
                ];
            });

            /**
             * @returns {*|{}|any|Mongo.Cursor|DOMElement|T}
             * @private
             */
            _countLike = () => {
                return Like.find({
                    objectType: $scope.objectType,
                    objectId: $scope.objectId
                }).count() || null;
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                countLike: _countLike
            });

            /**
             * Like toggle action
             */
            this.likeToggle = () => {
                let condition = {
                    objectType: $scope.objectType,
                    objectId: $scope.objectId,
                    creator: Meteor.userId()
                };

                let existsLike = Like.findOne(condition);

                if(existsLike) {
                    Like.remove({
                        _id: existsLike._id
                    });
                }
                else {
                    Like.insert(condition);
                }
            };
        }
    }
});