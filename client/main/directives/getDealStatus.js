angular.module('gameShopApp').directive('getDealStatus', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("value", () => {
                let status = '';
                if (scope.value !== '') {
                    switch (parseInt(scope.value)) {
                        case Common.constants.deal.status.created:
                            status = gettext("Создан");
                            break;
                        case Common.constants.deal.status.inProcess:
                            status = gettext("В обработке");
                            break;
                        case Common.constants.deal.status.success:
                            status = gettext("Продан");
                            break;
                        case Common.constants.deal.status.fail:
                            status = gettext("Ошибка");
                            break;
                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(status);
                    }
                    else {
                        element.html(
                            gettext("Статус: ") + status
                        );
                    }
                }
            });
        }
    };
});
