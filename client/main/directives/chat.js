angular.module('gameShopApp').directive('chat', function () {
    return {
        restrict: 'E',
        scope: {
            /**
             * Create comment method
             */
            createMethod: '=',
            /**
             * Create param (primary key model)
             */
            createParam: '=',
            /**
             * Subscribe on model-parent comment
             */
            commentsSubscribe: '=',
            /**
             * Subscribe inject method
             */
            subscribeMethod: '=',
            /**
             * Comment key in subscribe model
             */
            commentKey: '=',
            /**
             * Parent object id
             */
            objectId: '=',
            /**
             * Comment type
             * If null - not show
             */
            commentType: '='
        },
        templateUrl: '/packages/gameshop-browser/client/main/directives/chat.html',
        controllerAs: 'chat',
        controller: function ($scope, $reactive, $timeout, $mdDialog, library) {
            $reactive(this).attach($scope);

            /**
             * Comment param to create
             * @type {string}
             */
            this.comment = "";

            /**
             * Subcomment param to create
             * @type {string}
             */
            this.subcomment = "";

            /**
             * Comment id for to subcomment
             * @type {string}
             */
            this.subcommentId = "";

            /**
             * Comment key to model inject to view
             * @type {string|string|*}
             */
            this.commentKey = $scope.commentKey;

            /**
             * Data for validate chat
             */
            this.validateData = Common.constants.schema.validate.comment;

            /**
             * Complaint type inject to view
             * @type {string|*}
             */
            this.commentType = $scope.commentType || null;

            /**
             * Subscribe to chat model
             */
            this.subscribe($scope.commentsSubscribe);

            /**
             * Users id collections
             * @type {Array}
             */
            this.users = [];

            /**
             * Check and add user to array
             * @param userId
             */
            this.addUser = (userId) => {
                if(!this.users.inArray(userId))
                    this.users.push(userId);
            };

            /**
             * Subscribe to Meteor.users collection
             */
            this.subscribe("usersByIdInArray", () => {
                return [ this.getCollectionReactively('users') ];
            });

            /**
             * Select user collections
             * @returns {DOMElement|*|{}|T|any|Cursor}
             * @private
             */
            _usersCollection = () => {
                return Meteor.users.find({});
            };

            /**
             * Inject subscribe to view
             */
            this.helpers({
                commentsCollection: $scope.subscribeMethod,
                usersProfile: _usersCollection
            });

            /**
             * Show replay button
             * @param author_id
             * @returns {boolean}
             */
            this.showReply = (author_id) => {
                return author_id != Meteor.userId();
            };

            /**
             * Clear form validity status
             */
            this.clearFormValidity = () => {
                $timeout(() => {
                    $scope.createCommentForm['msg'].$setValidity("required", true);
                    $scope.createCommentForm.$setPristine(true);
                }, 100);
            };

            /**
             * Create comment method inject to view
             */
            this.createComment = () => {
                if(this.comment)
                    Meteor.call($scope.createMethod,
                        $scope.createParam, this.comment,
                        (error, data) => {
                            if (error) {
                                if(error.details)
                                    library.system.error.formValidate(
                                        $scope.createCommentForm,
                                        JSON.parse(error.details)
                                    );
                                else {
                                    library.system.error.handlers(error, Common.constants.errorCode.http.badRequest);
                                }
                            }
                            else {
                                this.comment = "";
                                this.clearFormValidity();
                            }
                        }
                    );
            };

            /**
             * Show subcomment form by id
             * @param comment
             * @param creator
             */
            this.showSubcommentForm = (comment, creator) => {
                this.subcommentId = comment._id;
                this.subcomment = `${creator.profile.name} ${creator.profile.surname}, `;
            };

            /**
             * Close subcomment form
             */
            this.closeSubcommentForm = () => {
                this.subcomment = "";
                this.subcommentId = null;
            };

            /**
             * Create subcomment method
             */
            this.createSubComment = () => {
                if(this.subcomment)
                    Meteor.call($scope.createMethod,
                        $scope.createParam, this.subcomment,
                        (error, data) => {
                            if (error) {
                                if(error.details)
                                    library.system.error.formValidate(
                                        $scope.createSubcommentForm,
                                        JSON.parse(error.details)
                                    );
                                else {
                                    library.system.error.handlers(error, Common.constants.errorCode.http.badRequest);
                                }
                            }
                            else {
                                this.subcomment = "";
                                this.subcommentId = "";
                                library.services.chatScrollToTop();
                                this.clearFormValidity();
                            }
                        }
                    );
            };

            /**
             * Create complaint on comment
             * @param commentId
             */
            this.complaint = (commentId) => {
                $mdDialog.show({
                    controller: ($scope, $mdDialog) => {
                        $scope.hide = () => {
                            $mdDialog.hide();
                        };
                        $scope.cancel = () => {
                            $mdDialog.cancel();
                        };
                        $scope.success = () => {
                            $mdDialog.hide($scope.types);
                        };
                        $scope.types = Common.constants.complaint.type.spam;
                    },
                    templateUrl: '/packages/gameshop-browser/client/main/dialog/complaintType.html',
                    parent: angular.element(document.body)
                }).
                then((type) => {
                    let complaint = Complaint.find({
                        objectId: $scope.objectId,
                        commentId: commentId,
                        type: type,
                        creator: Meteor.userId()
                    });
                    if(!library.system.checkCollection(complaint)) {
                        Complaint.insert({
                                objectType: Common.constants.complaint.objectType.comment,
                                objectId: $scope.objectId,
                                commentId: commentId,
                                commentType: this.commentType,
                                type: type
                            },
                            (err, data) => {
                                if (err) {
                                    library.system.error.handlers(err);
                                    return false;
                                }
                                library.system.toast(
                                    '/packages/gameshop-browser/client/main/toast/complaintAccepted.html'
                                );
                            });
                    }
                    else
                        library.system.toast(
                            '/packages/gameshop-browser/client/main/toast/complaintExists.html'
                        );
                });
            };

            /**
             * User cache var
             * @type {Array}
             */
            let users = [];

            /**
             * Get user by id
             * @param userId
             * @returns {*}
             */
            this.getUserById = (userId) => {
                if(users[userId]) {
                    return users[userId];
                }
                else {
                    let user = library.user.getProfileFilter(this.usersProfile, userId);
                    if(user)
                        users[userId] = user;
                    return users[userId];
                }
            };
        }
    }
});