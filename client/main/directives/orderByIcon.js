angular.module('gameShopApp').directive('orderByIcon', function ($compile, library) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            value: "="
        },
        link: function (scope, element, attrs) {
            scope.$watch("value", (newVal) => {
                let icon = "";
                if(newVal) {
                    switch (newVal) {
                        case 1:
                            icon = $compile('<md-icon aria-label="orderByIcon" md-svg-icon="hardware:ic_keyboard_arrow_up_24px"></md-icon>')(scope);
                            break;
                        case -1:
                            icon = $compile('<md-icon aria-label="orderByIcon" md-svg-icon="hardware:ic_keyboard_arrow_down_24px"></md-icon>')(scope);
                            break;
                    }
                }
                element.html(icon);
            });
        }
    };
});