angular.module('gameShopApp').directive('dealStatus', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("value", () => {
                let status = '';
                if (scope.value !== '') {
                    switch (parseInt(scope.value)) {
                        case Common.constants.deal.status.created:
                            status = gettext("Создана");
                            break;
                        case Common.constants.deal.status.inProcess:
                            status = gettext("В обработке");
                            break;
                        case Common.constants.deal.status.success:
                            status = gettext("Успешно");
                            break;
                        case Common.constants.deal.status.fail:
                            status = gettext("Провал");
                            break;

                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(status);
                    }
                    else {
                        element.html(
                            gettext("Статус: ") + status
                        );
                    }
                }
            });
        }
    };
});
