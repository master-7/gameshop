angular.module('gameShopApp').directive('getTicketStatusNameById', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            status: "=",
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {

            /**
             * Set status ticket
             * @param status
             */
            let setStatus = (status) => {
                if(Number.isInteger(parseInt(status))){
                    let categoryName = gettext("Ждем ответа");
                    switch (parseInt(status)) {
                        case Common.constants.ticket.status.close:
                            categoryName = gettext("Закрыто");
                            break;
                        case Common.constants.ticket.status.created:
                            categoryName = gettext("Создана");
                            break;
                        case Common.constants.ticket.status.moderatorAnswer:
                            categoryName = gettext("Вам ответ");
                            break;
                        case Common.constants.ticket.status.userAnswer:
                            categoryName = gettext("Ждем ответа");
                            break;
                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(
                            categoryName
                        );
                    }
                    else {
                        element.html(
                            gettext("Статус: ") + categoryName
                        );
                    }
                }
            };

            if (element[0].nodeName == 'MD-OPTION') {
                scope.$watch("value", (newVal, oldVal) => {
                    setStatus(newVal);
                });
            }
            else {
                scope.$watch("status", (newVal, oldVal) => {
                    setStatus(newVal);
                });
            }
        }
    };
});