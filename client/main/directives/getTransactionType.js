angular.module('gameShopApp').directive('getTransactionType', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("value", () => {
                let status = '';
                if (scope.value !== '') {
                    switch (parseInt(scope.value)) {
                        case MoneyTransactions.globalTypes.payIn:
                            status = gettext("Входящие средства");
                            break;
                        case MoneyTransactions.globalTypes.buy:
                            status = gettext("Покупка");
                            break;
                        case MoneyTransactions.types.payIn.yandex:
                            status = gettext("Входящие yandex");
                            break;
                        case MoneyTransactions.types.payIn.card:
                            status = gettext("Входящие банковская карта");
                            break;
                        case MoneyTransactions.types.payIn.robokassa:
                            status = gettext("Входящие робокасса");
                            break;
                        case MoneyTransactions.types.deal.game:
                        case MoneyTransactions.types.deal.gameWithMediator:
                            status = gettext("Покупка игры");
                            break;
                        case MoneyTransactions.types.deal.thing:
                            status = gettext("Покупка вещи");
                            break;
                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(status);
                    }
                    else {
                        element.html(
                            gettext("Тип: ") + status
                        );
                    }
                }
            });
        }
    };
});
