angular.module('gameShopApp').directive('scrolling', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            $('#main-value').scroll(function () {
                let elem = $(this)[0];
                let runScroll = (elem.scrollHeight - elem.scrollTop - elem.clientHeight) < 5;
                if (runScroll) {
                    scope.vm.loadItems();
                }
            });
        }
    }
});
