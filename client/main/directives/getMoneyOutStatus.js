angular.module('gameShopApp').directive('getMoneyOutStatus', function ($compile, gettext, library) {
    return {
        restrict: 'A',
        scope: {
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("value", () => {
                let status = '';
                if (scope.value !== '') {
                    switch (parseInt(scope.value)) {
                        case Common.constants.payOutBid.statuses.hasCreated:
                            status = gettext("Создана");
                            break;
                        case Common.constants.payOutBid.statuses.inProcess:
                            status = gettext("В обработке");
                            break;
                        case Common.constants.payOutBid.statuses.success:
                            status = gettext("Обработана");
                            break;
                        case Common.constants.payOutBid.statuses.verificationFail:
                            status = gettext("Ошибка");
                            break;

                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(status);
                    }
                    else {
                        element.html(
                            gettext("Статус заявки: ") + status
                        );
                    }
                }
            });
        }
    };
});
