angular.module('gameShopApp').directive('httpLoader', function() {
    return {
        restrict: 'EA',
        link: function(scope, element) {
            // Store original display mode of element
            var shownType = element.css('visibility');
            function hideElement() {
                element.css('visibility', 'hidden');
            }

            scope.$on('httpLoaderStart', function() {
                element.css('visibility', shownType);
            });

            scope.$on('httpLoaderEnd', hideElement);

            // Initially hidden
            hideElement();
        }
    };
});