angular.module('gameShopApp').directive('getAdsTypeNameByConts', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            adsType: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("adsType", (newVal, oldVal) => {
                if(Number.isInteger(parseInt(scope.adsType))){
                    let categoryName = gettext("Бесплатное");
                    switch (parseInt(scope.adsType)) {
                        case Common.constants.game.adsType.free:
                            categoryName = gettext("Бесплатное");
                            break;
                        case Common.constants.game.adsType.color:
                            categoryName = gettext("Цветное за топом ") +
                                Common.constants.payment.payAd.color + '<span class="fa fa-rub"></span>';
                            break;
                        case Common.constants.game.adsType.top:
                            categoryName = gettext("Цветное в топе ") +
                                Common.constants.payment.payAd.top + '<span class="fa fa-rub"></span>';
                            break;
                    }
                    element.html(
                        categoryName
                    );
                }
            });
        }
    };
});