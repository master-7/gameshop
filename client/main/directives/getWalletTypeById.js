angular.module('gameShopApp').directive('getWalletTypeById', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            accountType: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            let accountType = null;
            scope.$watch("accountType", () => {
                if (scope.accountType) {
                    let accountType = Common.constants.wallet.types.getTypeById[parseInt(scope.accountType)];
                    element.html(accountType);
                }
            });
        }
    };
});