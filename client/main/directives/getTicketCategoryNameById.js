angular.module('gameShopApp').directive('getTicketCategoryNameById', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            category: "=",
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {

            /**
             * Set category for element
             * @param category
             */
            let setCategory = (category) => {
                if(Number.isInteger(parseInt(category))){
                    let categoryName = gettext("Другое");
                    switch (parseInt(category)) {
                        case Common.constants.ticket.category.partnership:
                            categoryName = gettext("Сотрудничество");
                            break;
                        case Common.constants.ticket.category.technicalProblem:
                            categoryName = gettext("Техническая проблема");
                            break;
                        case Common.constants.ticket.category.offer:
                            categoryName = gettext("Предложение по улучшению");
                            break;
                        case Common.constants.ticket.category.money:
                            categoryName = gettext("Деньги");
                            break;
                        case Common.constants.ticket.category.game:
                            categoryName = gettext("Игра");
                            break;
                        case Common.constants.ticket.category.other:
                            categoryName = gettext("Другое");
                            break;
                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(
                            categoryName
                        );
                    }
                    else {
                        element.html(
                            gettext("Категория: ") + categoryName
                        );
                    }
                }
            };
            if (element[0].nodeName == 'MD-OPTION') {
                scope.$watch("value", (newVal, oldVal) => {
                    setCategory(newVal);
                });
            }
            else {
                scope.$watch("category", (newVal, oldVal) => {
                    setCategory(newVal);
                });
            }
        }
    };
});