angular.module('gameShopApp').directive('isActiveLink', function ($rootScope, $location) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            /**
             * Detect current location
             */
            let isCurrentLocation = () => {
                let className = 'md-primary';
                let isCurrentUrl = $location.url() && attr.href === $location.url();
                if (isCurrentUrl) {
                    element.addClass(className);
                } else {
                    element.removeClass(className);
                }
            };

            /**
             * On document ready start detect location
             */
            angular.element(document).ready(() => {
                isCurrentLocation();
            });

            /**
             * On locations change success detect location
             */
            $rootScope.$on('$locationChangeSuccess', (event) => {
                isCurrentLocation();
            });
        }
    }
});