angular.module('gameShopApp').directive('socialShare', [
    function () {
        return {
            restrict: 'E',
            scope: {
                socialshareText: "=",
                socialshareUrl: "@"
            },
            templateUrl: '/packages/gameshop-browser/client/main/directives/social-share.html',
            controllerAs: 'vm',
            controller: function ($scope, library) {
	            /**
                 * Show share dialog
                 */
                this.showShareDialog = () => {
                    library.system.dialog(
                        "/packages/gameshop-browser/client/main/dialog/social-share.html",
                        {
                            socialshareText: $scope.socialshareText,
                            socialshareUrl: Meteor.absoluteUrl($scope.socialshareUrl)
                        }
                    );
                };
            }
        }
    }
]);
