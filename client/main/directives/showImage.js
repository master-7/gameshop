angular.module('gameShopApp').directive('showImage', function ($compile, library) {
    return {
        scope: {
            image: '='
        },
        restrict: 'E',
        replace: true,
        link: function (scope, element, attrs) {
            element.replaceWith(
                $compile(
                    `<img class="${element[0].className}"
                        ng-src="${library.image.getImageLink(scope.image)}">`
                )(scope)
            );
        }
    };
});