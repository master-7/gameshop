angular.module('gameShopApp').directive('dealType', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("value", () => {
                let status = '';
                if (scope.value !== '') {
                    switch (parseInt(scope.value)) {
                        case Deals.types.game:
                            status = gettext("Игра");
                            break;
                        case Deals.types.thing:
                            status = gettext("Вещь");
                            break;
                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(status);
                    }
                    else {
                        element.html(
                            gettext("Тип: ") + status
                        );
                    }
                }
            });
        }
    };
});
