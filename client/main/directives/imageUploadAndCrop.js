angular.module('gameShopApp').directive('imageUploadAndCrop', function () {
    return {
        restrict: 'E',
        scope: {
            isArray: '=isArray',
            imageCollection: '=imgObj',
            imageCollectionKey: '@objKey',
            isIcon: '='
        },
        templateUrl: '/packages/gameshop-browser/client/main/directives/imageUploadAndCrop.html',
        controllerAs: 'imageUploadAndCrop',
        controller: function ($scope, $compile, $window, library, enums) {
            $scope.$watch("imageCollection", (newVal, oldVal) => {
                if(newVal) {
                    let key = 'images';
                    if($scope.imageCollectionKey) {
                        key = $scope.imageCollectionKey;
                    }

                    if(!$scope.imageCollection)
                        throw "Images property not exists in object!";
                    if($scope.isArray && !Array.isArray($scope.imageCollection[key]))
                        throw "Images is not array!";
                    if(!$scope.isArray && Array.isArray($scope.imageCollection[key]))
                        throw "Images is array!";

                    /**
                     * Min crop width and height
                     * @type {number}
                     */
                    this.cropWidthHeight = Common.constants.image.cropWidthHeight;

                    /**
                     * Crop img coordinates
                     * @type {{}}
                     */
                    this.cropImgSrcBounds = {};

                    /**
                     * Original image BASE 64
                     * @type {undefined}
                     */
                    $scope.originalImage = undefined;

                    /**
                     * Original image sizes
                     */
                    $scope.originalImageHeight = null;
                    $scope.originalImageWidth = null;

                    /**
                     * Scale canvas for the image
                     * @type {number}
                     */
                    $scope.scale = 1;

                    /**
                     * Scalable canvas sizes
                     */
                    $scope.canvasWidth = "";
                    $scope.canvasHeight = "";

                    /**
                     * Add image event handler
                     * @param files
                     */
                    this.addImage = (files) => {
                        if (files.length > 0) {
                            let reader = new FileReader();
                            reader.onload = (e) => {
                                let image = new Image();
                                image.onload = function() {
                                    let imagesTooBig = this.width > Common.constants.image.maxImageSize.width ||
                                            this.height > Common.constants.image.maxImageSize.height;
                                    if(imagesTooBig) {
                                        library.system.toast('/packages/gameshop-browser/client/main/error/imageTooLarge.toast.html');
                                        image.src = $scope.originalImage = null;
                                    }
                                    let uploadedImageIsTooSmall = this.width < Common.constants.image.originalMinWidthAndHeight ||
                                        this.height < Common.constants.image.originalMinWidthAndHeight;
                                    if(uploadedImageIsTooSmall) {
                                        image.src = $scope.originalImage = null;
                                        library.system.toast(
                                            '/packages/gameshop-browser/client/main/error/clientError.toast.html',
                                            'clientErrorToastCtrl',
                                            { error: enums.system.error.image.isTooSmall }
                                        );
                                    }
                                    else {
                                        $scope.originalImageWidth = this.width;
                                        $scope.originalImageHeight = this.height;
                                        $scope.canvasWidth = this.width;
                                        $scope.canvasHeight = this.height;
                                        let innerWidth = $window.innerWidth >= enums.system.width.panelOn ?
                                        $window.innerWidth - (enums.system.width.panelWidth + enums.system.width.widthMargin)
                                            : $window.innerWidth - enums.system.width.widthMargin;
                                        while (true) {
                                            let correctImageSize = $scope.canvasWidth < innerWidth &&
                                                $scope.canvasHeight < Common.constants.image.maxCanvasHeight;
                                            if (!correctImageSize) {
                                                $scope.scale -= Common.constants.image.scaleStep;
                                                $scope.canvasWidth *= $scope.scale;
                                                $scope.canvasHeight *= $scope.scale;
                                            }
                                            else {
                                                $scope.$apply();
                                                break;
                                            }
                                        }
                                    }
                                };
                                image.src = $scope.originalImage = e.target.result;
                            };
                            this.currentFile = files[0];
                            reader.readAsDataURL(files[0]);
                        }
                    };

	                /**
                     * Flag for image is action save in process
                     * @type {boolean}
                     */
                    $scope.imagesIsSave = false;

                    /**
                     * Save cropped image event handler
                     * @param $event
                     */
                    this.saveCroppedImage = ($event) => {
                        $scope.imagesIsSave = true;
                        $event.stopPropagation();
                        if ($scope.originalImage) {

                            this.currentFile.thumbnailBounds = {
                                widthAndHeight: this.cropImgSrcBounds.right - this.cropImgSrcBounds.left,
                                top: $scope.originalImageHeight - this.cropImgSrcBounds.top,
                                left: this.cropImgSrcBounds.left
                            };

                            if(!Array.isArray($scope.imageCollection[key]))
                                $scope.originalImage.isAvatar = true;
                            if($scope.isIcon) {
                                Icons.upload($scope.originalImage, this.currentFile,
                                    (file) => {
                                        $scope.$apply(() => {
                                            $scope.imagesIsSave = false;
                                            if ($scope.isArray)
                                                $scope.imageCollection[key].push(file._id);
                                            else
                                                $scope.imageCollection[key] = file._id;
                                            $scope.originalImage = undefined;
                                        });
                                    },
                                    (e) => {
                                        $scope.$apply(function() {
                                            $scope.imagesIsSave = false;
                                            console.log('Oops, something went wrong', e);
                                        });
                                    }
                                );
                            }
                            else {
                                Images.upload($scope.originalImage, this.currentFile,
                                    (file) => {
                                        $scope.$apply(() => {
                                            $scope.imagesIsSave = false;
                                            if ($scope.isArray)
                                                $scope.imageCollection[key].push(file._id);
                                            else
                                                $scope.imageCollection[key] = file._id;
                                            $scope.originalImage = undefined;
                                        });
                                    },
                                    (error) => {
                                        $scope.$apply(function() {
                                            $scope.imagesIsSave = false;
                                            console.log('Oops, something went wrong', error);
                                        });
                                        if(error.error == "file-too-large") {
                                            library.system.toast('/packages/gameshop-browser/client/main/error/imageTooLarge.toast.html');
                                        }
                                        else {
                                            library.system.error.handlers(error);
                                        }
                                    }
                                );
                            }
                            $scope.originalImageWidth = null;
                            $scope.originalImageHeight = null;
                            $scope.canvasWidth = "";
                            $scope.canvasHeight = "";
                        }
                    };
                }
            });
        }
    }
});