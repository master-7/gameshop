angular.module('gameShopApp').directive('gameStatus', function ($compile, gettext) {
    return {
        restrict: 'A',
        scope: {
            value: "="
        },
        replace: true,
        link: function (scope, element, attrs) {
            scope.$watch("value", () => {
                let status = '';
                if (scope.value !== '') {
                    switch (parseInt(scope.value)) {
                        case Common.constants.game.statuses.onVerification:
                            status = gettext("создано");
                            break;
                        case Common.constants.game.statuses.isPublic:
                            status = gettext("на продаже");
                            break;
                        case Common.constants.game.statuses.selling:
                            status = gettext("продается");
                            break;
                        case Common.constants.game.statuses.sold:
                            status = gettext("продано");
                            break;
                        case Common.constants.game.statuses.debatable:
                            status = gettext("на обсуждении");
                            break;
                        case Common.constants.game.statuses.failed:
                            status = gettext("продажа провалена");
                            break;
                        case Common.constants.game.statuses.moderatorIsWorking:
                            status = gettext("модератор работает");
                            break;
                        case Common.constants.game.statuses.verificationFail:
                            status = gettext("проверка провалена");
                            break;
                        case Common.constants.game.statuses.isBlocked:
                            status = gettext("заблокирована");
                            break;
                        case Common.constants.game.statuses.gameReturned:
                            status = gettext("пользователь вернул игру");
                            break;
                    }
                    if (element[0].nodeName == 'MD-OPTION') {
                        element.html(status);
                    }
                    else {
                        element.html(
                            gettext("Статус: ") + status
                        );
                    }
                }
            });
        }
    };
});
