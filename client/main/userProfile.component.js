angular.module('gameShopApp').directive('userProfile', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/userProfile.html',
        controllerAs: 'userProfile',
        controller: function ($scope, $stateParams, $controller, $reactive, gettext) {
            $reactive(this).attach($scope);

            /**
             * Inject user id to vm
             */
            this.userId = $stateParams.id || Meteor.userId();

            /**
             * Subscription handles
             * @type {Array}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {Array}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe on user collection
             */
            this.subscriptionHandles.push(
                this.subscribe("getUserDataById", () => {
                    return [ this.getReactively('userId') ];
                })
            );

            /**
             * Get current user data
             * @returns {any|*|{}|159}
             * @private
             */
            _currentUserObject = () => {
                return Meteor.users.findOne({
                    _id: this.getReactively('userId')
                });
            };

            /**
             * Inject avatars collection and changeable user's data to view
             */
            this.helpers({
                userData: _currentUserObject
            });

            /**
             * Tab labels
             */
            this.labels = {
                game: gettext("Игры"),
                thing: gettext("Вещи")
            };
        }
    }
});
