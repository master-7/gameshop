angular.module('gameShopApp').directive('rules', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/rules.html',
        controllerAs: "vm",
        controller: function ($scope, $timeout, $location) {

            /**
             * Scroll top flag
             * @type {boolean}
             */
            $scope.showScrollTop = false;

            /**
             * Scroll event listener
             */
            $('#main-value').scroll(function () {
                let elem = $(this)[0];

                let showScrollTop = elem.scrollTop > 300;

                if(!showScrollTop)
                    $location.path('/rules').search('');

                $scope.showScrollTop = showScrollTop;
                $scope.$apply();
            });

            /**
             * Scroll top action handler
             */
            this.scrollTopAction = (scrollTo) => {
                $location.path('/rules').search('scrollTo='+scrollTo);
                $timeout(() => {
                    let params = $location.path('/rules').search();
                    if (params && params.scrollTo && params.scrollTo == "contents")
                        $location.path('/rules').search('');
                });
            };

            /**
             * Drop event listener if controller destroy
             */
            $scope.$on('$destroy', () => {
                $('#main-value').off("scroll")
            });
        }
    };
});