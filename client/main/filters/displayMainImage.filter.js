angular.module('gameShopApp').filter('displayMainImage', function (library) {
	return function(images) {
		if (!images) {
			return images;
		}
		return Meteor.absoluteUrl(library.image.getImageLink(images[0]));
	};
});