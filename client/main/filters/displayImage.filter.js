angular.module('gameShopApp').filter('displayImage', function (library) {
	return function(image) {
		if (!image) {
			return image;
		}
		return Meteor.absoluteUrl(library.image.getImageLink(image));
	};
});