angular.module('gameShopApp').filter('cutWithEllipsis', function () {
    return function (value, max) {
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;
        value = value.substr(0, max);
        var lastSpace = value.lastIndexOf(' ');
        if (lastSpace != -1) {
            value = value.substr(0, lastSpace);
        }
        return value + " ...";
    };
});