angular.module('gameShopApp').directive('ticketEdit', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/ticketEdit.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $controller, $window, $location, $reactive, library) {
            $reactive(this).attach($scope);

            /**
             * Validate data inject to view
             */
            this.validateData = Common.constants.schema.validate.ticket;

            /**
             * Ticket category inject to view
             */
            this.ticketCategory = Common.constants.ticket.category;

            /**
             * Inject on baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = Ticket;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Condition for the subscriptions
             * @returns {{_id: {$in: *}}}
             * @private
             */
            _imagesSubscription = () => {
                return {
                    _id: {
                        $in: this.getCollectionReactively("editTicket.images")
                    }
                }
            };

            /**
             * Getter images collections
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _imagesCollections = () => {
                return Images.find(_imagesSubscription());
            };

            /**
             * Subscribe to images collection
             */
            this.subscriptionHandles.push(
                this.subscribe("images", () => {
                    return [_imagesSubscription()]
                })
            );

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(
                this.subscribe("ticketForCreator",
                    () => {
                        return [
                            null,
                            $stateParams.id
                        ];
                    },
                    {
                        onReady: () => {
                            let ticket = Ticket.findOne({_id: $stateParams.id});
                            if(ticket.creator != Meteor.userId()) {
                                library.system.error.handlers(Common.constants.errorCode.http.forbidden);
                                $window.history.back();
                            }
                            if(!library.system.checkCollection(ticket))
                                library.system.error.handlers(Common.constants.errorCode.http.notFound);
                        }
                    }
                )
            );

            /**
             * Select ticket collection
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _ticketCollection = () => {
                return Ticket.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                editTicket: _ticketCollection,
                images: _imagesCollections
            });

            /**
             * Create new ticket
             */
            this.editTicketAction = () => {
                this.queryProcess = true;
                Ticket.update({_id: this.editTicket._id},
                    {
                        $set: {
                            subject: this.editTicket.subject,
                            description: this.editTicket.description,
                            category: this.editTicket.category,
                            images: this.editTicket.images
                        }
                    },
                    (err, data) => {
                        if(!err) {
                            $location.path('/ticket/view/' + this.editTicket._id);
                        }
                        else
                            library.system.error.handlers(err);
                    }
                );
            };

            /**
             * Remove image event handler
             * @param id
             */
            this.removeImage = (id) => {
                this.editTicket.images.splice(
                    this.editTicket.images.indexOf(id), 1
                );
                Images.remove({_id: id});
            };
        }
    }
});