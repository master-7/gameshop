angular.module('gameShopApp').directive('partnership', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/main/partnership.html',
        controllerAs: "vm",
        controller: function ($scope, $timeout, $location) {

            /**
             * Scroll top flag
             * @type {boolean}
             */
            $scope.showScrollTop = false;

            /**
             * Link support inject ot view
             * @type {any}
             */
            this.linkOnSupport = Meteor.absoluteUrl('ticket/create');

            /**
             * Scroll event listener
             */
            $('#main-value').scroll(function () {
                let elem = $(this)[0];

                let showScrollTop = elem.scrollTop > 300;

                if(!showScrollTop)
                    $location.path('/partnership').search('');

                $scope.showScrollTop = showScrollTop;
                $scope.$apply();
            });

            /**
             * Scroll top action handler
             */
            this.scrollTopAction = (scrollTo) => {
                $location.path('/partnership').search('scrollTo='+scrollTo);
                $timeout(() => {
                    let params = $location.path('/partnership').search();
                    if (params && params.scrollTo && params.scrollTo == "contents")
                        $location.path('/partnership').search('');
                });
            };

            /**
             * Drop event listener if controller destroy
             */
            $scope.$on('$destroy', () => {
                $('#main-value').off("scroll")
            });
        }
    };
});