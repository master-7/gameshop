angular.module('gameShopApp').directive('userPurchasesView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/userPurchasesView.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $stateParams, $reactive, library, gettext) {
            $reactive(this).attach($scope);

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

	        /**
             * @type {Array}
             */
            this.images = [];

            /**
             * Subscribe to user sales collection
             */
            this.subscribe("userPurchases",
                () => {
                    return [{
                        _id: $stateParams.id
                    }];
                },
                {
                    onReady: () => {
                        let gameAccount = GameAccount.findOne({_id: $stateParams.id});
                        if(gameAccount) {
                            for(let index in gameAccount.images) {
                                let imageObj = gameAccount.images[index];
                                if(typeof imageObj == "object") {
                                    this.images.push({
                                        thumb: Meteor.absoluteUrl(library.image.getImageLink(imageObj)),
                                        img: Meteor.absoluteUrl(
                                            `ufs/${imageObj.originalStore}/${imageObj.originalId}/${imageObj.name}`
                                        )
                                    });
                                }
                            }
                            
                            if(gameAccount.status != Common.constants.game.statuses.sold) {
                                /**
                                 * @type {{common: *, withManager: *}}
                                 */
                                this.chatType = {
                                    common: gettext("Общий"),
                                    withManager: gettext("С менеджером")
                                };

                                /**
                                 * Init chat data
                                 * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
                                 */
                                this.chat = {
                                    createCommentMethodName: "gameAccountSalesCreateComment",
                                    createCommentParam: $stateParams.id,
                                    commentsSubscribe: "userPurchases",
                                    subscribeMethod: _userPurchaseCollections,
                                    commentKey: "chatSelling",
                                    commentType: Common.constants.complaint.objectType.commentType.game
                                };

                                /**
                                 * Init private chat data
                                 * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
                                 */
                                this.chatWithManager = {
                                    createCommentMethodName: "gameAccountSalesBuyerManager",
                                    createCommentParam: $stateParams.id,
                                    commentsSubscribe: "userPurchases",
                                    subscribeMethod: _userPurchaseCollections,
                                    commentKey: "chatSellingBuyerManager",
                                    commentType: Common.constants.complaint.objectType.commentType.game
                                };
                            }

                        }
                        else
                            library.system.error.handlers(Common.constants.errorCode.http.notFound);
                    }
                }
            );

            /**
             * Images subscribe
             */
            this.subscribe('imagesByIdInArray',
                () => {
                    return [
                        this.getCollectionReactively('userPurchase.images')
                    ]
                }
            );

            /**
             * Select user sales collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _userPurchaseCollections = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * @returns {*|{}|any|Mongo.Cursor|DOMElement|T}
             * @private
             */
            _imagesCollections = () => {
                return Images.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                userPurchase: _userPurchaseCollections,
                images: _imagesCollections
            });
        }
    }
});