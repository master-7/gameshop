angular.module('gameShopApp').directive('userSalesView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/userSalesView.html',
        controllerAs: 'vm',
        controller: function ($rootScope, $scope, $controller, $stateParams, $location, $reactive, $interval, $compile, library, gettext) {
            $reactive(this).attach($scope);

            /**
             * Flag query process
             * @type {boolean}
             */
            this.queryProcess = false;

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * @type {Array}
             */
            this.images = [];

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(
                this.subscribe("userSales",
                    () => {
                        return [{
                            _id: $stateParams.id
                        }]
                    },
                    {
                        onReady: () => {
                            let gameAccount = GameAccount.findOne({_id: $stateParams.id});
                            if(gameAccount) {
                                //Set page title
                                $rootScope.page.title = `${gameAccount.name} | ${gameAccount.price + gameAccount.commission}`;
                                for(let index in gameAccount.images) {
                                    let imageObj = gameAccount.images[index];
                                    if(typeof imageObj == "object") {
                                        this.images.push({
                                            thumb: Meteor.absoluteUrl(library.image.getImageLink(imageObj)),
                                            img: Meteor.absoluteUrl(
                                                `ufs/${imageObj.originalStore}/${imageObj.originalId}/${imageObj.name}`
                                            )
                                        });
                                    }
                                }
                            }
                            else
                                library.system.error.handlers(Common.constants.errorCode.http.notFound);
                        }
                    }
                )
            );

            /**
             * Select gameAccount collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountCollections = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccount: _gameAccountCollections
            });

            /**
             * @type {{common: *, withManager: *}}
             */
            this.chatType = {
                common: gettext("Общий"),
                withManager: gettext("С менеджером")
            };

            /**
             * Init chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
             */
            this.chat = {
                createCommentMethodName: "gameAccountSalesCreateComment",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "userSales",
                subscribeMethod: _gameAccountCollections,
                commentKey: "chatSelling",
                commentType: Common.constants.complaint.objectType.commentType.game
            };

            /**
             * Init private chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
             */
            this.chatWithManager = {
                createCommentMethodName: "gameAccountSalesSellerManager",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "userSales",
                subscribeMethod: _gameAccountCollections,
                commentKey: "chatSellingSellerManager",
                commentType: Common.constants.complaint.objectType.commentType.game
            };
        }
    }
});