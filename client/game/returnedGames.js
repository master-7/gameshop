angular.module('gameShopApp').directive('gameReturned', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/returnedGames.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount collection
             */
            this.subscribe("returnedGames",
                () => {},
                {
                    onReady: () => {
                        $scope.mongoQueryReady = true;
                    },
                    onStop: () => {
                        $scope.mongoQueryReady = true;
                    }
                }
            );

            /**
             * Select gameAccount collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountCollections = () => {
                return GameAccount.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccounts: _gameAccountCollections
            });
        }
    }
});
