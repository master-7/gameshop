angular.module('gameShopApp').directive('gameAccountOnModeration', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/moderation/gameAccount.html',
        controllerAs: 'gaom',
        controller: function ($scope, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Status inject into view
             */
            this.statuses = Common.constants.game.statuses;

            /**
             * Subscribe to gameAccount in status onVerification
             */
            this.subscribe("gameAccountOnVerificationForCreator", () => {},
                {
                    onReady: () => {
                        $scope.mongoQueryReady = true;
                    },
                    onStop: () => {
                        $scope.mongoQueryReady = true;
                    }
                }
            );

            /**
             * Select gameAccount collections in status onVerification
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountOnVerificationCollections = () => {
                return GameAccount.find({});
            };

            /**
             * Inject collections into view
             */
            this.helpers({
                gameAccounts: _gameAccountOnVerificationCollections
            });
        }
    }
});