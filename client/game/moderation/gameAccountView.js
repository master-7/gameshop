angular.module('gameShopApp').directive('gameAccountOnModerationView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/moderation/gameAccountView.html',
        controllerAs: 'mgav',
        controller: function ($scope, $stateParams, $reactive, $window, library) {
            $reactive(this).attach($scope);

            /**
             * Subscribe to gameAccount in status onVerification
             */
            this.subscribe("gameAccountOnVerificationForCreator",
                () => { },
                {
                    onReady: () => {
                        let gameAccount = GameAccount.findOne({_id: $stateParams.id});
                        if(!gameAccount)
                            library.system.error.handlers(Common.constants.errorCode.http.notFound);
                    }
                }
            );

            /**
             * Game account on moderation to view
             * @returns {*|{}|T|any|Cursor|175}
             * @private
             */
            _gameAccountOnModeration = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * Init chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountOnModeration, commentKey: string}}
             */
            this.chat = {
                createCommentMethodName: "gameAccountCreateModeratorComment",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "gameAccountModeratorIsWorking",
                subscribeMethod: _gameAccountOnModeration,
                commentKey: "moderatorComments"
            };

            /**
             * Inject collections into view
             */
            this.helpers({
                gameAccount: _gameAccountOnModeration
            });

	        /**
             * @type {Array}
             */
            this.images = [];

	        /**
	         * Image array prepare
             */
            $scope.$watch(
                () => {
                    return this.gameAccount;
                },
                (oldVal, newVal) => {
                    if(this.gameAccount && !this.images.length)
                        this.images = library.image.filterImageCollectionForNgGallery(this.gameAccount.images);
                }
            );

            /**
             * Check allow edit game account
             */
            this.allowEdit = (status) => {
                return status == Common.constants.game.statuses.verificationFail;
            };

            /**
             * Get image link function
             * @param imageId
             * @returns {*}
             */
            this.getImageLink = (imageId) => {
                if(imageId)
                    return library.image.getImageLink(imageId);
                else
                    return undefined;
            };
        }
    }
});