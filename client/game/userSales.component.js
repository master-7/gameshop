angular.module('gameShopApp').directive('userSales', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/userSales.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive, library) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = GameAccount;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to user sales collection
             */
            this.subscribe("userSales",
                () => {
                    return [
                        null,
                        this.getReactively('limit.count')
                    ];
                },
                library.system.mongoReady(this.userSales, $scope)
            );

            /**
             * Select user sales collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _userSalesCollections = () => {
                return GameAccount.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                userSales: _userSalesCollections
            });

            /**
             * Not final status deal
             * @param gameAccount
             * @returns {boolean}
             */
            this.sellingGameStatus = (gameAccount) => {
                if(gameAccount)
                    return gameAccount.status == Common.constants.game.statuses.selling;
                return false;
            };
        }
    }
});