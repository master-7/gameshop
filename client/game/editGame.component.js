let {Component, View, SetModule, Inject, MeteorReactive, LocalInjectables} = angular2now;

SetModule('gameshop.browser');

@Component({selector: 'editGame', controllerAs: 'vm'})
@View({
    templateUrl: () => {
        if (Meteor.isCordova) {
            return "";
        }
        return '/packages/gameshop-browser/client/game/editGame.html';
    }
})
@Inject(['$scope', '$state', '$stateParams', '$timeout', 'gettext', 'library'])
@MeteorReactive
@LocalInjectables
class editGame {

    /**
     * @type {Array}
     */
    imagesArray = [];

    constructor() {
        
        /**
         * Query process flag
         * @type {boolean}
         */
        this.queryProcess = false;

        /**
         * Game account without changes
         * @type {null}
         */
        this.gameAccountCanonical = null;

        /**
         * Validate gameAccount data
         */
        this.validateData = Common.constants.schema.validate.game;

        /**
         * Inject ads type to view
         */
        this.adsTypes = Common.constants.game.adsType;

        /**
         * System commission
         * @type {number}
         */
        this.commission = this.library.payment.getRateInPercent();

        /**
         * @type {number}
         */
        this.minCommission = Common.constants.payment.minCommission;

        /**
         * Inject to view calc the commission
         * @type {library.payment.calcCommission}
         */
        this.calcCommission = this.library.payment.calcCommission;

        /**
         * Tags placeholder variable
         */
        this.autocomplitePlaceholder = this.gettext("Введите название игры");

        /**
         * Search gameAccountTypeString
         * @type {String}
         */
        this.searchGameAccountType = null;

        /**
         * Select gameAccountType
         * @param item
         */
        this.newTypeSelected = (item) => {
            if(this.gameAccount && item)
                this.gameAccount.type = item._id;
        };

        /**
         * Subscribe to gameAccountType collection
         */
        this.subscribe("gameAccountType");

        /**
         * Subscribe to game account
         */
        this.subscribe("gameAccountEditAfterModeratorReturn",
            () => {
                return [this.$stateParams.id]
            },
            {
                onReady: () => {
                    let gameAccount = GameAccount.findOne({_id: this.$stateParams.id});
                    if(!gameAccount)
                        this.library.system.error.handlers(Common.constants.errorCode.http.notFound);
                }
            }
        );

        /**
         * Subscribe to images collection
         */
        this.subscribe("thumbs", () => {
            return [
                this.getCollectionReactively("imagesArray")
            ]
        });

        /**
         * Subscribe to categoryKeywords collection
         */
        this.subscribe("categoryKeywordsActive");

        /**
         * Inject collections to view
         */
        this.helpers({
            gameCategory: this._gameCategoryCollection,
            gameAccountType: this._gameAccountTypeCollection,
            selectedGameAccountType: this._selectedGameAccountTypeObj,
            gameAccount: this._gameAccountCollection,
            images: this._imagesCollection
        });

        this.$scope.$watch(
            () => {
                return this.gameAccount;
            },
            (newVal) => {
                if(!this.gameAccountCanonical && newVal) {
                    this.gameAccountCanonical = JSON.stringify(newVal);
                }
            }
        );

        /**
         * If image add start to rebuild directive
         * @type {boolean}
         */
        this.imageAdd = false;

        this.$scope.$watchCollection(
            () => {
                if(this.gameAccount)
                    return this.gameAccount.images;
            },
            (newVal) => {
                if(this.gameAccount)
                    this.gameAccount.images.forEach((item) => {
                        let id = typeof item == "object" ? item.originalId : item;
                        if(this.imagesArray.indexOf(id) == -1) {
                            this.imagesArray.push(id);
                        }
                    });
                //Rebuild
                if(newVal) {
                    this.imageAdd = true;
                    this.$timeout(() => {
                        this.imageAdd = false;
                    }, 50);
                }
            }
        );
    }

    /**
     * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
     * @private
     */
    _selectedGameAccountTypeObj = () => {
        return GameAccountType.findOne({
            _id: this.getReactively("gameAccount.type")
        });
    };

    /**
     * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
     * @private
     */
    _gameAccountTypeCollection = () => {
        return GameAccountType.find({
            name: new RegExp(this.getReactively("searchGameAccountType"), 'i')
        });
    };

    /**
     * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
     * @private
     */
    _gameCategoryCollection = () => {
        return CategoryKeywords.find({});
    };

    /**
     * Getter images collections
     * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
     * @private
     */
    _imagesCollection = () => {
        return Thumbs.find({
            originalStore: 'images',
            originalId: {
                $in: this.getCollectionReactively("imagesArray")
            }
        });
    };

    /**
     * Getter gameAccount collections
     * @returns {any|159|*|{}}
     * @private
     */
    _gameAccountCollection = () => {
        return GameAccount.findOne({});
    };

    /**
     * Save account action
     */
    saveAccount = () => {
        if(this.gameAccountCanonical && this.gameAccountCanonical == JSON.stringify(this.gameAccount)) {
            this.library.system.toast('/packages/gameshop-browser/client/main/error/formNotChanged.html');
            return;
        }
        this.queryProcess = true;
        this.gameAccount.images = this.images.map((item) => {
            return {
                _id: item._id,
                name: item.name,
                store: item.store,
                originalStore: item.originalStore,
                originalId: item.originalId
            };
        });
        this.gameAccount.category = {
            _id: this.gameAccount.category._id,
            name: this.gameAccount.category.name,
            image: this.gameAccount.category.image
        };
        Meteor.call("editGameAccountAfterModerate", this.gameAccount,
            (error, result) => {
                this.queryProcess = false;
                if(error) {
                    if (error.code == Common.constants.errorCode.http.badRequest) {
                        this.library.system.toast('/packages/gameshop-browser/client/main/toast/notAllowDeleteLastImage.html');
                    }
                    else
                        this.library.system.error.handlers(error);
                }
                else {
                    this.library.system.toast('/packages/gameshop-browser/client/main/toast/gameAddSuccess.toast.html');
                    this.$state.go('public');
                }
            }
        );
    };
    
    /**
     * Remove image event handler
     * @param id
     */
    removeImage = (id) => {
        Meteor.call("removeImage", id, this.gameAccount,
            (error, result) => {
                if(error) {
                    this.queryProcess = false;
                    this.library.system.error.handlers(error);
                }
                else {
                    this.gameAccount.images.splice(
                        this.gameAccount.images.indexOf(id), 1
                    );
                    this.imagesArray.splice(
                        this.imagesArray.indexOf(id), 1
                    );
                }
            }
        );
    };
}