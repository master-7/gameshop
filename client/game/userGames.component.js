angular.module('gameShopApp').directive('userGames', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/userGames.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $stateParams, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount collection
             */
            this.subscribe("userGames",
                () => {
                    return [
                        $stateParams.id || Meteor.userId()
                    ];
                },
                {
                    onReady: () => {
                        $scope.mongoQueryReady = true;
                    },
                    onStop: () => {
                        $scope.mongoQueryReady = true;
                    }
                }
            );

            /**
             * Select gameAccount collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountCollections = () => {
                return GameAccount.find({});
            };
            
            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccounts: _gameAccountCollections
            });

        }
    }
});
