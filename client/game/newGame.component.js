angular.module('gameShopApp').directive('newGame', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/newGame.html',
        controllerAs: 'newGame',
        controller: function ($scope, $reactive, $state, $timeout, gettext, library) {
            $reactive(this).attach($scope);

            /**
             * Flag query process
             * @type {boolean}
             */
            this.queryProcess = false;

            /**
             * Data for validate
             */
            this.validateData = Common.constants.schema.validate.game;

            /**
             * Inject ads type to view
             */
            this.adsTypes = Common.constants.game.adsType;

            /**
             * System commission
             * @type {number}
             */
            this.commission = library.payment.getRateInPercent();

            /**
             * @type {number}
             */
            this.minCommission = Common.constants.payment.minCommission;

            /**
             * Inject to view calc the commission
             * @type {library.payment.calcCommission}
             */
            this.calcCommission = library.payment.calcCommission;

            /**
             * gameAccount data
             */
            this.newAccount = {
                name: null,
                description: null,
                category: {},
                type: null,
                sellingMode: Common.constants.game.sellingMode.toMediate,
                adsType: Common.constants.game.adsType.free,
                price: null,
                videos: [],
                images: [],
                gameAccount: {
                    login: null,
                    pass: null
                },
                dependentAccount: {
                    login: null,
                    pass: null
                }
            };

	        /**
             * If image add start to rebuild directive
             * @type {boolean}
             */
            $scope.imageAdd = false;

            $scope.$watchCollection(
                () => {
                    return this.newAccount.images;
                },
                (newVal, oldVal) => {
                    //Rebuild
                    if(newVal) {
                        $scope.imageAdd = true;
                        $timeout(() => {
                            $scope.imageAdd = false;
                        }, 50);
                    }
                }
            );

            /**
             * Tags placeholder variable
             */
            this.autocomplitePlaceholder = gettext("Введите название игры");

	        /**
	         * Search gameAccountTypeString
             * @type {String}
             */
            this.searchGameAccountType = null;

	        /**
             * Select gameAccountType
             * @param item
             */
            this.newTypeSelected = (item) => {
                this.newAccount.type = item._id;
            };

	        /**
	         * Subscribe to gameAccountType collection
             */
            this.subscribe("gameAccountType", () => {
                return [
                    null,
                    this.getReactively("searchGameAccountType")
                ]
            });

	        /**
	         * Subscribe to categoryKeywords collection
             */
            this.subscribe("categoryKeywordsActive");

            /**
             * Subscribe to images collection
             */
            this.subscribe("thumbs", () => {
                return [
                    this.getCollectionReactively("newAccount.images")
                ]
            });

	        /**
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _gameAccountTypeCollection = () => {
                return GameAccountType.find({
                    name: new RegExp(this.getReactively("searchGameAccountType"), 'i')
                });
            };

	        /**
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _gameCategoryCollection = () => {
                return CategoryKeywords.find({});
            };

            /**
             * Getter images collections
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _imagesCollection = () => {
                return Thumbs.find({
                    originalStore: 'images',
                    originalId: {
                        $in: this.getCollectionReactively("newAccount.images")
                    }
                });
            };

            /**
             * Inject image collection to view
             */
            this.helpers({
                gameCategory: _gameCategoryCollection,
                gameAccountType: _gameAccountTypeCollection,
                images: _imagesCollection
            });

            /**
             * Save game account handler
             */
            this.saveAccount = () => {
                this.queryProcess = true;
                this.newAccount.images = this.images.map((item) => {
                    return {
                        _id: item._id,
                        name: item.name,
                        store: item.store,
                        originalStore: item.originalStore,
                        originalId: item.originalId
                    };
                });
                this.newAccount.category = {
                    _id: this.newAccount.category._id,
                    name: this.newAccount.category.name,
                    image: this.newAccount.category.image
                };
                GameAccount.insert(this.newAccount, (error, result) => {
                    this.queryProcess = false;
                    if(error) {
                        library.system.error.formValidate(
                            $scope.addGameAccountForm,
                            GameAccount.simpleSchema().namedContext().invalidKeys()
                        );
                    }
                    else {
                        if(this.newAccount.sellingMode == Common.constants.game.sellingMode.preApproval)
                            library.system.toast('/packages/gameshop-browser/client/main/toast/gameAddSuccess.toast.html');
                        else
                            library.system.toast('/packages/gameshop-browser/client/main/toast/gameAddModeratorMode.html');
                        $state.go('public');
                    }
                });
            };

            /**
             * Remove image event handler
             * @param id
             */
            this.removeImage = (id) => {
                this.newAccount.images.splice(
                    this.newAccount.images.indexOf(id), 1
                );
                Meteor.call("removeImage", id, this.newAccount,
                    (error, result) => {
                        if(error) {
                            this.queryProcess = false;
                            library.system.error.handlers(error);
                        }
                    }
                );
            };
        }
    }
});