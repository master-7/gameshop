angular.module('gameShopApp').directive('gameSold', function () {
	return {
		restrict: 'E',
		templateUrl: '/packages/gameshop-browser/client/game/gameSold.html',
		controllerAs: 'vm',
		controller: function ($scope, $controller, $stateParams, $reactive) {
			$reactive(this).attach($scope);

			/**
			 * Angular extend base controller
			 */
			angular.extend(this, $controller('baseCtrl', {
				$scope: $scope
			}));

			/**
			 * Subscribe to user sales collection
			 */
			this.subscribe("userSold",
				() => {},
				{
					onReady: () => {
						$scope.mongoQueryReady = true;
					},
					onStop: () => {
						$scope.mongoQueryReady = true;
					}
				}
			);

			/**
			 * Select user sales collections
			 * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
			 * @private
			 */
			_userSoldCollections = () => {
				return GameAccount.find({});
			};

			/**
			 * Inject collection to view
			 */
			this.helpers({
				userSold: _userSoldCollections
			});
		}
	}
});