angular.module('gameShopApp').directive('publicAccounts', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/public.html',
        controllerAs: 'vm',
        controller: function ($scope, $stateParams, $controller, $reactive, $location, library) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = GameAccount;

            /**
             * Filter object
             * @type {{}}
             */
            this.filter = {};

            /**
             * @type {Number|string}
             */
            this.filter.category = $stateParams.category || '';

            /**
             * @type {Number|string}
             */
            this.filter.typeId = $stateParams.type;
            /**
	         * @returns {Object}
	         */
            let getSearchParams = () => {
                let searchObj = {};
	            if(this.filter.category)
		            searchObj.category = this.filter.category;
	            if(this.filter.typeId)
		            searchObj.type = this.filter.typeId;
	            return searchObj;
            };

            /**
             * Watch on category and type change
             */
            $scope.$watchCollection(
                () => {
                    return [
                        this.filter.category,
                        this.filter.typeId
                    ];
                },
                (newVal, oldVal) => {
                    $location.search(Object.assign(getSearchParams(), $location.search()));
                }
            );

            /**
             * Return correct category bool
             */
            this.categoryIsCorrect = (category) => {
                return !!parseInt(category)
            };

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Complaint type
             * @type {number}
             */
            this.complaintType = Common.constants.complaint.objectType.game;

            /**
             * Subscribe to gameAccountType collection
             */
            this.subscribe("gameAccountType");

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(
                this.subscribe("gameAccountPublic",
                    () => {
                        return [
                            {
                                category: this.getReactively('filter.category'),
                                type: this.getReactively('filter.typeId')
                            },
                            this.getReactively('limit.count')
                        ];
                    },
                    library.system.mongoReady(this.gameAccounts, $scope)
                )
            );

            /**
             * Subscribe to categoryKeywords collection
             */
            this.subscribe("categoryKeywordsActive");

            /**
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _gameAccountTypeCollections = () => {
                return GameAccountType.find({
                    name: new RegExp(
                        this.getReactively('filter.typeName'),
                        'i'
                    )
                });
            };

            /**
             * Select gameAccount collection
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountCollection = () => {
                return GameAccount.find({});
            };

            /**
             * @returns {*|175|DOMElement|{}|any|Mongo.Cursor}
             * @private
             */
            _gameCategoryCollection = () => {
                return CategoryKeywords.find({});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameCategories: _gameCategoryCollection,
                gameAccountType: _gameAccountTypeCollections,
                gameAccounts: _gameAccountCollection
            });
        }
    }
});