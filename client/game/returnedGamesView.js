angular.module('gameShopApp').directive('gameReturnedView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/returnedGamesView.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $stateParams, $reactive, library) {
            $reactive(this).attach($scope);

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(this.subscribe("returnedGames",
                () => {
                    return [
                        $stateParams.id || null
                    ];
                },
                {
                    onReady: () => {
                        let gameAccount = GameAccount.findOne({_id: $stateParams.id});
                        if(gameAccount)
                            return gameAccount;
                        else
                            library.system.error.handlers(Common.constants.errorCode.http.notFound);
                    }
                }
            ));

            /**
             * Select gameAccount collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountCollection = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccount: _gameAccountCollection
            });
        }
    }
});
