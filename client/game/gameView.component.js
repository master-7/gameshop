angular.module('gameShopApp').directive('gameView', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/game/gameView.html',
        controllerAs: 'vm',
        controller: function ($rootScope, $scope, $controller, $stateParams, $location, $reactive, $interval, $compile, gettext, library) {
            $reactive(this).attach($scope);

            /**
             * Flag query process
             * @type {boolean}
             */
            this.queryProcess = false;

            /**
             * Data for validate
             */
            this.validateData = Common.constants.schema.validate.game;

            /**
             * System commission
             * @type {number}
             */
            this.commission = library.payment.getRateInPercent();

	        /**
	         * Min payment commission
             * @type {number}
             */
            this.minCommission = Common.constants.payment.minCommission;

            /**
             * Inject to view calc the commission
             * @type {library.payment.calcCommission}
             */
            this.calcCommission = () => {
                this.gameAccount.commission = Common.helpers.calcCommission(this.gameAccount.price);
                return library.payment.calcCommission(this.gameAccount.price);
            };

            /**
             * Subscription handles
             * @type {{}}
             */
            this.subscriptionHandles = [];

            /**
             * Inject subscription handles to baseCtrl
             * @type {{}}
             */
            $scope.subscriptionHandles = this.subscriptionHandles;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

	        /**
             * @type {Array}
             */
            this.images = [];

            /**
             * Subscribe to gameAccount collection
             */
            this.subscriptionHandles.push(
                this.subscribe("gameAccountPublic",
                    () => {
                        return [
                            {_id: $stateParams.id}
                        ]
                    },
                    {
                        onReady: () => {
                            let gameAccount = GameAccount.findOne({_id: $stateParams.id});
                            if(gameAccount) {
                                //Set page title
                                $rootScope.page.title = `${gameAccount.name} | ${gameAccount.price + gameAccount.commission}`;
                                this.images = library.image.filterImageCollectionForNgGallery(gameAccount.images);
                            }
                            else
                                library.system.error.handlers(Common.constants.errorCode.http.notFound);
                        }
                    }
                )
            );

            /**
             * Select gameAccount collections
             * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
             * @private
             */
            _gameAccountCollections = () => {
                return GameAccount.findOne({_id: $stateParams.id});
            };

            /**
             * Inject collection to view
             */
            this.helpers({
                gameAccount: _gameAccountCollections
            });

            /**
             * Init chat data
             * @type {{createCommentMethodName: string, createCommentParam: *, commentsSubscribe: string, subscribeMethod: _gameAccountCollections, commentKey: string}}
             */
            this.chat = {
                createCommentMethodName: "gameAccountCreateComment",
                createCommentParam: $stateParams.id,
                commentsSubscribe: "gameAccountPublic",
                subscribeMethod: _gameAccountCollections,
                commentKey: "comments",
                commentType: Common.constants.complaint.objectType.commentType.game
            };

            /**
             * Init like directive data
             * @type {{objectType: number, key: string}}
             */
            this.like = {
                objectType: Common.constants.like.type.game
            };

            /**
             * Creates new deal
             */
            this.createDeal = () => {
                //Show dialog success
                library.system.dialog(
                    '/packages/gameshop-browser/client/main/dialog/purchaseAgreement.html'
                ).then((success) => {
                    if(success) {
                        Meteor.call("payGame",
                            this.gameAccount._id,
                            (error, target) => {
                                if (error) {
                                    library.system.error.handlers(error);
                                }
                                else {
                                    library.system.toast(
                                        '/packages/gameshop-browser/client/main/toast/successBuy.toast.html'
                                    );
                                    $location.path('/game/user-purchases').replace();                                    
                                }
                            }
                        );
                    }
                }, function() {
                    $scope.status = false;
                });
            };

            /**
             * Get timer string
             * @param interval
             * @returns {string}
             */
            let getTimerString = (interval) => {
                /**
                 * Calculate the interval on units
                 * @param interval
                 * @param unitsType
                 * @param getWholeSec
                 * @returns {*}
                 */
                let countOnInterval = (interval, unitsType, getWholeSec = false) => {
                    let metric = null;
                    switch (unitsType) {
                        case "days":
                            metric = (1000 * 60 * 60 * 24);
                            break;
                        case "hours":
                            metric = (1000 * 60 * 60);
                            break;
                        case "minutes":
                            metric = (1000 * 60);
                            break;
                        case "seconds":
                            metric = 1000;
                            break;
                        default:
                            return false;
                            break;
                    }
                    if(getWholeSec) {
                        return interval * metric;
                    }
                    return Math.floor(interval / metric);
                };

                /**
                 * Metrics types
                 * @type {string[]}
                 */
                let metrics = [
                    "days",
                    "hours",
                    "minutes",
                    "seconds"
                ];

                let timerString = "";

                for(let index in metrics) {
                    let metric = metrics[index];
                    let countTime = countOnInterval(interval, metric);
                    if(countTime < 10 && index != 0) {
                        timerString += `0${Math.abs(countTime)}`;
                    }
                    else
                        timerString += Math.abs(countTime);
                    if(metrics[++index]) {
                        timerString += ":";
                        interval -= countOnInterval(countTime, metric, true);
                    }
                }

                return timerString;
            };

            /**
             * Update timer string on view
             */
            let promise = $interval(() => {
                if(this.gameAccount) {
                    let pastTime = (this.gameAccount.createdAt + 7 * (1000 * 60 * 60 * 24)) - Date.now();
                    if (pastTime > 0)
                        this.timeWaitOnReturn = getTimerString(pastTime);
                }
            }, 1000);

            /**
             * Return account to creator
             */
            this.returnAccount = () => {
                Meteor.call("returnGameAccountForCreator", this.gameAccount._id, (error, data) => {
                    if(!error) {
                        $location.path('/public').replace();
                    }
                    else
                        library.system.error.handlers(error);
                })
            };

            /**
             * Change gamePrice fro creator
             */
            this.changePrice = () => {
                Meteor.call("changeGamePriceForCreator", this.gameAccount._id, this.gameAccount.price, (error, data) => {
                    if(error) {
                        library.system.error.handlers(error);
                    }
                })
            };

	        /**
	         * Check on allow the delete account
             * @returns {*|boolean}
             */
            this.allowDeleteAccount = (gameAccount) => {
                return gameAccount.sellingMode == Common.constants.game.sellingMode.toMediate;
            };

	        /**
	         * Delete game account action
             */
            this.deleteAccount = () => {
                library.system.confirmDialog(
                    gettext('Вы уверены?'),
                    gettext('Если удалите аккаунт, действие нельзя будет отменить')
                ).then(() => {
                    Meteor.call("deleteGameAccountForCreator", this.gameAccount._id, (error, data) => {
                        if(error) {
                            library.system.error.handlers(error);
                        }
                        else {
                            $location.path('/public').replace();
                        }
                    })
                }, function() {});
            };
            
            /**
             * If destroy controller - cancel interval
             */
            $scope.$on('$destroy', () => {
                $interval.cancel(promise);
            });
        }
    }
});