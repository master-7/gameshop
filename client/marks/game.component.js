angular.module('gameShopApp').directive('marksGame', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/marks/game.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive, library) {
            $reactive(this).attach($scope);

            /**
             * Limit new items
             * @type {number}
             */
            this.limit = {
                count: 0
            };

            /**
             * Inject ot baseCtrl
             * @type {number}
             */
            $scope.limit = this.limit;

            /**
             * Inject collection name to baseCtrl
             * @type {string}
             */
            $scope.collectionObj = GameAccount;

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Like publish array
             * @type {null}
             */
            this.gameLike = [];

            /**
             * Game like subscribe
             */
            this.subscribe("gameLikePublishForCreator");

            /**
             * Game account by like obj
             */
            this.subscribe("gameAccountsByLikeObjArray",
                () => {
                    return [
                        this.getCollectionReactively('gameLike'),
                        this.getReactively('limit.count')
                    ];
                },
                library.system.mongoReady(this.games, $scope)
            );

            /**
             * Game like cursor
             * @returns {Cursor}
             * @private
             */
            _gameLike = () => {
                return Like.find({});
            };

            /**
             * Mark game cursor
             * @returns {Cursor}
             * @private
             */
            _gameMarkCollection = () => {
                return GameAccount.find({});
            };

            /**
             * Inject to view collections
             */
            this.helpers({
                gameLike: _gameLike,
                games: _gameMarkCollection
            });
        }
    }
});
