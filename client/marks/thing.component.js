angular.module('gameShopApp').directive('marksThing', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/marks/thing.html',
        controllerAs: 'vm',
        controller: function ($scope, $controller, $reactive) {
            $reactive(this).attach($scope);

            /**
             * Angular extend base controller
             */
            angular.extend(this, $controller('baseCtrl', {
                $scope: $scope
            }));

            /**
             * Like publish array
             * @type {null}
             */
            this.thingLike = [];

            /**
             * Game like subscribe
             */
            this.subscribe("thingLikePublishForCreator");

            /**
             * Game account by like obj
             */
            this.subscribe("thingByLikeObjArray", () => {
                return [
                    this.getCollectionReactively('thingLike')
                ];
            });

            /**
             * Thing like cursor
             * @returns {Cursor}
             * @private
             */
            _thingLike = () => {
                return Like.find({});
            };

            /**
             * Mark game cursor
             * @returns {Cursor}
             * @private
             */
            _thingMarkCollection = () => {
                return GameThing.find({});
            };

            /**
             * Inject to view collections
             */
            this.helpers({
                thingLike: _thingLike,
                things: _thingMarkCollection
            });
        }
    }
});
