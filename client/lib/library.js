/**
 * Frontend library
 */
angular.module('gameShopApp').factory("library", ($state, $location, $mdDialog, $mdToast, $filter, $timeout, $interval, gettext, enums) => {
    let library = {};

    /**
     * Payment library
     */
    library.payment = {
        /**
         * Get rate in percent
         * @returns {number}
         */
        getRateInPercent: () => {
            return Common.constants.payment.commission * 100;
        },
        /**
         * Calculate system commission
         * @param sum
         * @returns {*}
         */
        calcCommission: (sum) => {
            let sumWithCommission = Common.helpers.calcPriceWithCommission(sum);
            return sumWithCommission || 0;
        }
    };

    /**
     * User library
     */
    library.user = {
        /**
         * Get user profile by userId from userCollection
         * @param userCollection
         * @param userId
         * @returns {*}
         */
        getProfileFilter: (userCollection, userId) => {
            if(userCollection && userId) {
                let user = $filter('filter')(userCollection, {_id: userId})[0];
                if (user)
                    return user;
            }
            return null;
        }
    };

    /**
     * Image library
     */
    library.image = {
        /**
         * Filter image avatar collection
         * @param userCollection
         * @param userId
         * @returns {*}
         */
        filterImageAvatarCollection: (userCollection, userId) => {
            if(userId) {
                let user = $filter('filter')(userCollection, {_id: userId})[0];
                if(user && user.profile && user.profile.avatar)
                    return user.profile.avatar;
            }
            return undefined;
        },
        /**
         * Get image link helper
         * @param image
         * @returns {string}
         */
        getImageLink: (image) => {
            return Common.helpers.getImageLink(image);
        },
        /**
         * Check image url
         * @param url
         * @param foundCallback
         * @param notFoundCallback
         */
        checkImage: (url, foundCallback, notFoundCallback) => {
            let counter = 0;

            let testImage = (URL) => {
                let tester = new Image();
                tester.onload = foundCallback;
                tester.onerror = imageNotFound;
                tester.src = URL;
            };

            let imageNotFound = () => {
                if(counter < 10)
                    $timeout(() => {
                        testImage(url);
                    }, 1000);
                else {
                    notFoundCallback();
                }
            };

            testImage(url);
        },
	    /**
         * @param collection
         * @returns {Array}
         */
        filterImageCollectionForNgGallery: (collection) => {
            let images = [];
            for(let index in collection) {
                let imageObj = collection[index];
                if(typeof imageObj == "object") {
                    images.push({
                        thumb: Meteor.absoluteUrl(library.image.getImageLink(imageObj)),
                        img: Meteor.absoluteUrl(
                            `ufs/${imageObj.originalStore}/${imageObj.originalId}/${imageObj.name}`
                        )
                    });
                }
            }
            return images;
        }
    };

    /**
     * Usability services
     */
    library.services = {};

    /**
     * Scroll chat to top
     */
    library.services.chatScrollToTop = () => {
        document.querySelector("#main-value").scrollTop = 500;
    };

    /**
     * Scroll to element with jQuery element
     * @param element
     */
    library.services.scrollToWithElement = (element) => {
        library.system.getElementOnDomSave("#main-value", (mainValue) => {
            mainValue.scrollTop = element.offset().top - 200;
        });
    };

    /**
     * Scroll to element
     * @param eID
     */
    library.services.scrollTo = (eID) => {
        library.system.getElementOnDomSave(`#${eID}`, (element) => {
            library.services.scrollToWithElement(element);
        });
    };

    /**
     * Animate opacity with jQuery element
     * @param element
     * @param flag
     */
    library.services.animateOpacityWithElement = (element, flag) => {
        element.css({backgroundColor: "rgba(189, 195, 199, 0.1)"});
        let speed = 10;
        let i = speed;
        let animateFunctionVar = 1;
        let promise = $interval(() => {
            i -= (animateFunctionVar *= 1.01);
            let opacity = i / speed;
            if (opacity >= 0)
            {
                element.css({backgroundColor: "rgba(189, 195, 199, " + opacity + ")"});
            }
            else
            {
                element.css({backgroundColor: "rgba(189, 195, 199, " + 0 + ")"});
                $interval.cancel(promise);
                if (flag)
                {
                    flag.val = false;
                }
            }
        }, 100);
    };

    /**
     * Animate opacity for background
     * @param selector
     */
    library.services.animateOpacity = (selector) => {
        library.system.getElementOnDomSave(`#${selector}`, (element) => {
            element = $(element);
            library.services.animateOpacityWithElement(element);
        });
    };

    /**
     * Scroll to selector with animation
     * @param selector
     */
    library.services.scrollToWithAnimation = (selector) => {
        library.system.getElementOnDomSave(`#${selector}`, (element) => {
            element = $(element);
            library.services.scrollToWithElement(element);
            library.services.animateOpacityWithElement(element);
        });
    };

    /**
     * System library
     */
    library.system = {
        /**
         * Check element on the dom
         * @param selector
         * @param callback
         * @param countAttempts
         * @param timeStep
         */
        getElementOnDomSave: (selector, callback, countAttempts = 10, timeStep = 500) => {
            let element = null;
            let counter = 0;

            let testSelector = () => {
                if(counter < countAttempts) {
                    element = document.querySelector(selector);
                    if (element) {
                        callback(element);
                    }
                    else {
                        counter++;
                        $timeout(() => {
                            testSelector();
                        }, timeStep);
                    }
                }
                else
                    return false;
            };
            Common.system.runOnReady(() => {
                testSelector();
            });
        },
        /**
         * Show toast
         * @param templateUrl
         * @param ctrl
         * @param locals
         */
        toast: (templateUrl, ctrl = null, locals = {}) => {
            if(ctrl) {
                $mdToast.show({
                    hideDelay: enums.system.showTimeToast,
                    position: 'top right',
                    controller: ctrl,
                    locals: locals,
                    templateUrl: templateUrl
                });
            }
            else {
                $mdToast.show({
                    hideDelay: enums.system.showTimeToast,
                    position: 'top right',
                    controller: ($scope) => {
                        $scope.closeToast = () => {
                            $mdToast.hide();
                        };
                    },
                    templateUrl: templateUrl
                });
            }
        },
	    /**
         * Standard confirm dialog
         * @param templateUrl {string}
         * @param locals {Object}
         * @returns {promise}
	     */
        dialog: (templateUrl, locals = {}) => {
            return $mdDialog.show({
                controller: ($scope, $mdDialog) => {
                    if(locals) {
                        for(var index in locals) {
                            let item = locals[index];
                            $scope[index] = item;
                        }
                    }
                    $scope.hide = () => {
                        $mdDialog.hide();
                    };
                    $scope.cancel = () => {
                        $mdDialog.cancel();
                    };
                    $scope.success = () => {
                        $mdDialog.hide(true);
                    };
                },
                templateUrl: templateUrl,
                parent: angular.element(document.body)
            })
        },
        /**
         * Confirm dialog action
         * @param title
         * @param text
         * @param ariaLabel
         * @param okText
         * @param cancelText
         * @returns {promise}
         */
        confirmDialog: (title, text, okText = "ok", cancelText = "cancel", ariaLabel = "Confirm action") => {
            let confirm = $mdDialog.confirm()
                .title(
                    title
                )
                .textContent(
                    text
                )
                .ariaLabel(
                    ariaLabel
                )
                .ok(okText)
                .cancel(cancelText);
            return $mdDialog.show(confirm);
        },
        /**
         * Notification system component
         * @param data
         * @param events
         * @returns {boolean}
         */
        notification: (data, events = null) => {
            if(('Notification' in window) ) {
                if (!data || !data.title || !data.body || !data.icon) {
                    console.log("Fail of create the notification!");
                    return false;
                }
                Notification.requestPermission(function (permission) {
                    if (permission === "granted") {
                        let notification = new Notification(
                            data.title,
                            {
                                body: data.body,
                                icon: data.icon,
                                dir: data.dir || 'auto'
                            }
                        );
                        if(events) {
                            if(typeof events.click == "function") {
                                notification.onclick = events.click;
                            }
                            if(typeof events.close == "function") {
                                notification.onclose = events.close;
                            }
                        }
                    }
                    else {
                        console.log("Permission denied of notification!");
                        return false;
                    }
                });
            }
            else {
                console.log("Your browser doesn't support notification!");
                return false;
            }
        }
    };

    /**
     * Check collection on exist data
     * @param collection
     * @returns {boolean}
     */
    library.system.checkCollection = (collection) => {
        return Common.system.countElementOnCollection(collection) > 0;
    };

    /**
     * Get the notification by event data
     * @param eventData
     * @param eventsHandlers
     */
    library.system.getNotificationByEvent = (eventData, eventsHandlers) => {
        let data = {
            title: "",
            body: "",
            action: "",
            icon: Meteor.absoluteUrl(
                library.image.getImageLink(eventData.image)
            )
        };
        switch (parseInt(eventData.type)) {
            case Common.constants.userTracker.type.moderation.createdGame:
                data.title = gettext("Игра создана!");
                data.body = gettext("После проверки игра появиться в каталоге");
                break;
            case Common.constants.userTracker.type.moderation.success:
                data.title = gettext("Модерация пройдена");
                data.body = gettext("Игра прошла проверку!");
                break;
            case Common.constants.userTracker.type.moderation.returnGame:
                data.title = gettext("Проверка не пройдена");
                data.body = gettext("Модератор не пропустил вашу игру в каталог. Исправьте ошибки и попробуйте ещё раз");
                break;
            case Common.constants.userTracker.type.moderation.fail:
                data.title = gettext("Игра заблокирована");
                data.body = gettext("Ваш аккаунт заблокирован в связи с нарушением правил");
                break;
            case Common.constants.userTracker.type.moderation.message:
                data.title = gettext("Модератор оставил вам сообщение");
                data.body = gettext("Нажмите на уведомление, чтобы посмотреть сообщение");
                eventsHandlers.click = () => {
                    window.focus();
                    window.location.href = Meteor.absoluteUrl(
                        `/game/on-moderation/view/${eventData.data._id}?scrollTo=comment-${eventData.data.commentId}`
                    );
                    this.close();
                };
                break;
            case Common.constants.userTracker.type.ticket.message:
                data.title = gettext("Ответ на ваше обращение");
                data.body = gettext("Нажмите на уведомление, чтобы посмотреть сообщение");
                eventsHandlers.click = () => {
                    window.focus();
                    window.location.href = Meteor.absoluteUrl(
                        `/ticket/view/${eventData.data._id}`
                    );
                    this.close();
                };
                break;
            case Common.constants.userTracker.type.deal.created:
                data.title = gettext("Ваш аккаунт куплен");
                data.body = gettext("Нажмите на уведомление, чтобы начать процесс передачи аккаунта");
                eventsHandlers.click = () => {
                    window.focus();
                    window.location.href = Meteor.absoluteUrl(
                        `/game/user-sales/view/${eventData.data._id}`
                    );
                    this.close();
                };
                break;
            case Common.constants.userTracker.type.deal.message:
                data.title = gettext("Вам сообщение при заключении сделки!");
                data.body = gettext("Вам новое сообщение при заключении сделки, нажмите на уведомление, чтобы посмотреть сообщение");

                if (Common.system.permissions.isModerator(Meteor.userId())) {
                    eventsHandlers.click = () => {
                        window.focus();
                        window.location.href = Meteor.absoluteUrl(
                            `/admin/game-deal/view/${eventData.data._id}?scrollTo=comment-${eventData.data.commentId}`
                        );
                        this.close();
                    };
                }
                else {
                    if(eventData.data.creator == Meteor.userId()) {
                        eventsHandlers.click = () => {
                            window.focus();
                            window.location.href = Meteor.absoluteUrl(
                                `/game/user-sales/view/${eventData.data._id}?scrollTo=comment-${eventData.data.commentId}`
                            );
                            this.close();
                        };
                    }
                    if(eventData.data.buyer == Meteor.userId()) {
                        eventsHandlers.click = () => {
                            window.focus();
                            window.location.href = Meteor.absoluteUrl(
                                `/game/user-purchases/view/${eventData.data._id}?scrollTo=comment-${eventData.data.commentId}`
                            );
                            this.close();
                        };
                    }
                }
                break;
        }
        if(data.title && data.body && data.icon)
            library.system.notification(data, eventsHandlers);
    };

    /**
     * Create new complaint
     * @param objectType
     * @param objectId
     */
    library.system.complaint = (objectType, objectId) => {
        $mdDialog.show({
            controller: ($scope, $mdDialog) => {
                $scope.hide = () => {
                    $mdDialog.hide();
                };
                $scope.cancel = () => {
                    $mdDialog.cancel();
                };
                $scope.success = () => {
                    $mdDialog.hide($scope.types);
                };
                $scope.types = Common.constants.complaint.type.spam;
            },
            templateUrl: '/packages/gameshop-browser/client/main/dialog/complaintType.html',
            parent: angular.element(document.body)
        }).
        then((type) => {
            let complaint = Complaint.find({
                objectId: objectId,
                type: type,
                creator: Meteor.userId()
            });
            if(!library.system.checkCollection(complaint)) {
                Complaint.insert({
                    objectType: objectType,
                    objectId: objectId,
                    type: type
                },
                (err, data) => {
                    if (err) {
                        library.system.error.handlers(err);
                        return false;
                    }
                    library.system.toast(
                        '/packages/gameshop-browser/client/main/toast/complaintAccepted.html'
                    );
                });
            }
            else
                library.system.toast(
                    '/packages/gameshop-browser/client/main/toast/complaintExists.html'
                );
        });
    };

    /**
     * System error library
     */
    library.system.error = {
        /**
         * Error action handler
         * @param error
         * @param defaultVal
         */
        handlers: (error, defaultVal) => {
            let code = null;

            if (defaultVal)
                code = defaultVal;

            if (typeof error === 'object') {
                if (error.code)
                    code = error.code;
                if (error.error && typeof error.error === 'number')
                    code = error.error;
            }

            if (typeof error === 'number') {
                code = error;
            }

            if(code == null)
                code = Common.constants.errorCode.unknown;

            switch (code) {
                case Common.constants.errorCode.http.badRequest:
                    library.system.toast('/packages/gameshop-browser/client/main/error/400.toast.html');
                    break;
                case Common.constants.errorCode.http.unauthorized:
                    library.system.toast('/packages/gameshop-browser/client/main/error/401.toast.html');
                    break;
                case Common.constants.errorCode.http.paymentRequired:
                    library.system.toast('/packages/gameshop-browser/client/main/error/402.toast.html');
                    library.system.dialog('/packages/gameshop-browser/client/main/error/402.dialog.html');
                    break;
                case Common.constants.errorCode.http.forbidden:
                    library.system.toast('/packages/gameshop-browser/client/main/error/403.toast.html');
                    break;
                case Common.constants.errorCode.http.conflict:
                    library.system.toast('/packages/gameshop-browser/client/main/error/409.toast.html');
                    break;
                case Common.constants.errorCode.http.notFound:
                    $location.path('/404').replace();
                    break;
                case Common.constants.errorCode.http.internalServerError:
                    library.system.toast(
                        '/packages/gameshop-browser/client/main/error/main.toast.html',
                        'errorToastCtrl',
                        { error: error }
                    );
                    break;
                case Common.constants.errorCode.unknown:
                    library.system.toast('/packages/gameshop-browser/client/main/error/1000.toast.html');
                    break;
            }
        }
    };

    /**
     * @param form
     * @param invalidKeys
     */
    library.system.error.formValidate = (form, invalidKeys) => {
        //Set all fields validity is true
        for (let errorIndex in form.$error) {
            let errorByType = form.$error[errorIndex];
            if (form.$error.hasOwnProperty(errorIndex)) {
                for (let index in errorByType) {
                    if(errorByType.hasOwnProperty(index)) {
                        form[errorByType[index].$name].$setValidity(errorIndex, true);
                    }
                }
            }
        }
        form.$setPristine(true);
        if(invalidKeys) {
            library.system.error.handlers(Common.constants.errorCode.http.badRequest);
            for(let index in invalidKeys) {
                let error = invalidKeys[index];
                let name = error.name.replace(/^\w+\.\d+\./, "");
                switch (error.type) {
                    case Common.constants.schema.errorTypes.string.min:
                        form[name].$setValidity("minlength", false);
                        break;
                    case Common.constants.schema.errorTypes.string.max:
                        form[name].$setValidity("maxlength", false);
                        break;
                    case Common.constants.schema.errorTypes.number.min:
                        form[name].$setValidity("min", false);
                        break;
                    case Common.constants.schema.errorTypes.number.max:
                        form[name].$setValidity("max", false);
                        break;
                    case Common.constants.schema.errorTypes.regexp:
                        form[name].$setValidity("email", false);
                        form[name].$setValidity("pattern", false);
                        break;
                    default:
                        form[name].$setValidity(error.type, false);
                        break;
                }
            }
        }
    };

	/**
	 * Mongo subscribes ready|stop event handlers
     * @param object
     * @param scope
     * @returns {{onReady: (function()), onStop: (function())}}
     */
    library.system.mongoReady = (object, scope) => {
        return {
            onReady: () => {
                if(object)
                    $timeout(() => {
                        scope.mongoQueryReady = true;
                    }, 3000);
                else {
                    $timeout(() => {
                        scope.mongoQueryReady = true;
                    }, 5000);
                }
            },
            onStop: () => {
                if(object)
                    $timeout(() => {
                        scope.mongoQueryReady = true;
                    }, 3000);
                else {
                    $timeout(() => {
                        scope.mongoQueryReady = true;
                    }, 5000);
                }
            }
        };
    };

    return library;
});
