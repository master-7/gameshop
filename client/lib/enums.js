/**
 * apps constant
 */
angular.module('gameShopApp').constant('enums', {
    /**
     * Available auth errors
     */
    authErrors: {
        authRequired: 'AUTH_REQUIRED',
        onlyForUnauthorized: 'ONLY_FOR_UNAUTHORIZED',
        onlyForAdmin: 'ONLY_FOR_ADMIN',
        onlyForModerator: 'ONLY_FOR_MODERATOR',
        onlyForSiteManager: 'ONLY_FOR_SITE-MANAGER'
    },
    /**
     * System constants
     */
    system: {
        /**
         * Error types
         */
        error: {
            image: {
                isTooSmall: 1
            }
        },
        showTimeToast: 10000,
        notificationDelay: 10000,
        width: {
            panelOn: 960,
            panelWidth: 250,
            widthMargin: 50
        }
    }
});