/**
 * Prototype controller
 */
angular.module('gameShopApp').controller('baseCtrl', ['$scope', '$rootScope', '$filter', '$window', '$timeout', 'library',
    function ($scope, $rootScope, $filter, $window, $timeout, library) {
        /**
         * Page load flag
         * @type {boolean}
         */
        $rootScope.pageLoad = false;

        /**
         * Mongo query ready flag
         * @type {boolean}
         */
        $scope.mongoQueryReady = false;

        $rootScope.$on('httpLoaderEnd', function (event, data) {
            /**
             * If not answer - not found
             */
            $timeout(() => {
                $rootScope.pageLoad = true;
            }, 10000);
        });

        /**
         * If change mongoQueryReady - onReady is working
         */
        $scope.$watch("mongoQueryReady",
            (newVal, oldVal) => {
                if(newVal != oldVal)
                    $rootScope.pageLoad = true;
            }
        );

        $scope.subscribe("gameIconPublic");

        /**
         * Select gameIcon collections
         * @returns {*|T|any|Mongo.Cursor|{}|DOMElement}
         * @private
         */
        _gameIconCollection = () => {
            return GameIcon.find({});
        };

        /**
         * Inject collection to view
         */
        $scope.helpers({
            gameIcons: _gameIconCollection
        });

        /**
         * Count items to view
         * @type {number}
         */
        this.countItems = 0;
        
        /**
         * View action buttons if gameAccount creator is not current user
         * @param object
         * @returns {boolean}
         */
        this.visibilityActionButton = (object) => {
            if(object)
                return object.creator != Meteor.userId();
            return false;
        };

        /**
         * Show complaint form
         * @param objId
         * @param type
         */
        this.complaint = (objId, type) => {
            library.system.complaint(type, objId);
        };

        /**
         * Get category logo by categoryId
         * @param categoryId
         * @returns {*|null}
         */
        this.getCategoryLogo = (categoryId) => {
            let gameIcon = GameIcon.findOne({index: categoryId});
            if(gameIcon)
                return gameIcon["icon"];
            return null;
        };

        /**
         * Get main image helper
         * @param images
         * @returns {*}
         */
        this.getMainImage = (images) => {
            if(images && images[0])
                return library.image.getImageLink(images[0]);
            return undefined;
        };

        /**
         * Get image links array by array id
         * @param images
         * @returns {Array}
         */
        this.getImagesLinksById = (images) => {
            let imageLinkCollection = [];
            for (let index in images) {
                if(typeof images[index] == "string")
                    imageLinkCollection.push(
                        library.image.getImageLink(images[index])
                    )
            }
            return imageLinkCollection;
        };

        /**
         * Check collection on empty
         * @param collection
         * @returns {boolean}
         */
        this.notEmptySection = (collection) => {
            return library.system.checkCollection(collection);
        };

        /**
         * Toggle orderBy
         * @param name
         */
        this.orderByClick = (name) => {
            switch($scope.orderBy[name]) {
                case 0:
                    $scope.orderBy[name] = 1;
                    break;
                case 1:
                    $scope.orderBy[name] = -1;
                    break;
                case -1:
                    $scope.orderBy[name] = 0;
                    break;
            }
        };

        /**
         * Flag load new items
         * @type {boolean}
         */
        let loadIsRunning = false;

        /**
         * Load new items
         */
        this.loadItems = () => {
            if($scope.limit && !loadIsRunning) {
                loadIsRunning = true;
                let countItems = $scope.collectionObj.find().count();
                let addItemsFlag = this.countItems < countItems;
                this.countItems = countItems;
                if (addItemsFlag)
                    $scope.limit.count += Common.constants.loadParties.countItems;
                
                $timeout(() => {
                    loadIsRunning = false;
                }, 100);
            }
        };

        /**
         * View action buttons if collection creator is not current user
         * @returns {boolean}
         */
        this.visibilityActionButton = (collection) => {
            if(collection && collection.creator)
                return collection.creator != Meteor.userId();
            return false;
        };

        /**
         * Current user create the collections
         * @param collection
         * @returns {boolean}
         */
        this.isCreator = (collection) => {
            if(collection && collection.creator)
                return collection.creator == Meteor.userId();
            return false;
        };

        /**
         * Check user on moderator rules
         * @returns {*}
         */
        this.isModerator = () => {
            return Common.system.permissions.isModerator(Meteor.userId());
        };

        /**
         * User cache var
         * @type {Array}
         */
        let users = [];

        /**
         * Get user by id
         * @param userId
         * @param userCollection
         * @returns {*}
         */
        this.getUserById = (userId, userCollection) => {
            if(users[userId]) {
                return users[userId];
            }
            else {
                let user = library.user.getProfileFilter(userCollection, userId);
                if(user)
                    users[userId] = user;
                return users[userId];
            }
        };
        
        let destroySubscribe = () => {
            for(let index in $scope.subscriptionHandles) {
                let handler = $scope.subscriptionHandles[index];
                if(typeof handler == "object") {
                    handler.stop();
                }
            }
        };

        /**
         * On route change event
         */
        $rootScope.$on('$stateChangeStart', function () {
            destroySubscribe();
            if($scope.closePage)
                $scope.closePage();
        });
        
        /**
         * On close the windows or tab event
         * @param evt
         */
        $window.onbeforeunload = (evt) => {
            destroySubscribe();
            if($scope.closePage)
                $scope.closePage();
        };
    }
]);
