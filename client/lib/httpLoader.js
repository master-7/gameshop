angular.module('gameShopApp').factory('httpLoaderInterceptor', ['$q', '$rootScope', function($q, $rootScope) {
    // Active request count
    var requestCount = 0;

    function startRequest(config) {
        // If no request ongoing, then broadcast start event
        if( !requestCount ) {
            $rootScope.$broadcast('httpLoaderStart');
        }

        requestCount++;
        return config;
    }

    function endRequest(arg) {
        // No request ongoing, so make sure we don’t go to negative count
        if( !requestCount )
            return;

        requestCount--;
        // If it was last ongoing request, broadcast event
        if( !requestCount ) {
            $rootScope.$broadcast('httpLoaderEnd');
        }

        return arg;
    }

    function responseError (response) {
        // No request ongoing, so make sure we don’t go to negative count
        if( !requestCount )
            return;

        requestCount--;
        // If it was last ongoing request, broadcast event
        if (!requestCount) {
            // Hide loader
            $rootScope.$broadcast("httpLoaderEnd");
        }

        return $q.reject(response);
    }

    // Return interceptor configuration object
    return {
        'request': startRequest,
        'requestError': endRequest,
        'response': endRequest,
        'responseError': responseError
    };
}]);

angular.module('gameShopApp').config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('httpLoaderInterceptor');
}]);