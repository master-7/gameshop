let modulesToLoad = [
	'angular-meteor',
	'angular-meteor.auth',
	'ui.router',
	'ngMaterial',
	'ngMessages',
	'ngFileUpload',
	'ngImgCrop',
	'angular-img-cropper',
	'xeditable',
	'gettext',
	'720kb.socialshare',
	'jkuri.gallery',
	'ncy-angular-breadcrumb'
];

if (Meteor.isCordova) {
	modulesToLoad = modulesToLoad.concat(['gameshop.mobile']);
}
else {
	modulesToLoad = modulesToLoad.concat(['gameshop.browser']);
}

angular.module('gameShopApp', modulesToLoad);

angular.module('gameShopApp').config(($mdIconProvider) => {
	$mdIconProvider
		.iconSet("social", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-social.svg")
		.iconSet("action", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-action.svg")
		.iconSet("alert", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-alert.svg")
		.iconSet("communication", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-communication.svg")
		.iconSet("content", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-content.svg")
		.iconSet("toggle", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-toggle.svg")
		.iconSet("navigation", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-navigation.svg")
		.iconSet("editor", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-editor.svg")
		.iconSet("image", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-image.svg")
		.iconSet("hardware", "/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-hardware.svg")
})
.config(function($mdThemingProvider) {
	$mdThemingProvider.theme('default')
		.backgroundPalette('grey', {
			'default': '50'
		});
});

function onReady() {
	angular.bootstrap(document, ['gameShopApp'], {
		strictDi: true
	});
}

if (Meteor.isCordova)
	angular.element(document).on('deviceready', onReady);
else
	angular.element(document).ready(onReady);
