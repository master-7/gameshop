/**
 * User complaint model
 */
Complaint = new Mongo.Collection("complaint");

/**
 * Complaint schema
 * @type {SimpleSchema}
 */
Complaint.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        objectType: {
            type: Number,
            label: "Complaint object type",
            min: Common.constants.schema.validate.complaint.minObjectType,
            max: Common.constants.schema.validate.complaint.maxObjectType
        },
        objectId: {
            type: String,
            label: "Complaint object id",
            index: true
        },
        commentId: {
            type: Meteor.Collection.ObjectID,
            label: "Complaint comment id",
            optional: true,
            index: true
        },
        commentType: {
            type: Number,
            label: "Complaint comment id",
            optional: true
        },
        type: {
            type: Number,
            label: "Complaint type",
            min: Common.constants.schema.validate.complaint.minType,
            max: Common.constants.schema.validate.complaint.maxType,
            index: true
        },
        status: {
            type: Number,
            label: "Complaint status",
            min: Common.constants.schema.validate.complaint.minStatus,
            max: Common.constants.schema.validate.complaint.maxStatus,
            optional: true
        },
        creator: {
            type: String,
            label: "Complaint creator id",
            optional: true,
            index: true
        },
        moderator: {
            type: String,
            label: "Complaint moderator id",
            optional: true,
            index: true
        }
    },
    Common.system.schemas.commonFields
));

Complaint.attachSchema(Complaint.schema);

/**
 * Allow actions rules
 */
Complaint.allow({
    insert: function (userId, doc) {
        if(userId) {
            let complaint = Complaint.find({
                objectId: doc.objectId,
                type: doc.type,
                creator: Meteor.userId()
            });
            if(Common.system.countElementOnCollection(complaint) == 0)
                return true;
            else
                throw new Meteor.Error(409, "Complaint exist!");
        }
        return false;
    },
    remove: function (userId, doc) {
        return false;
    },
    update: function (userId, doc) {
        return Common.system.permissions.isModerator(userId);
    }
});