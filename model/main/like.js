/**
 * Like collection
 * @type {Mongo.Collection}
 */
Like = new Mongo.Collection("like");

/**
 * Allow actions rules
 */
Like.allow({
    insert: function (userId) {
        return true;
    },
    remove: function (userId, doc) {
        return userId && doc.creator == userId;
    },
    update: function (userId, account) {
        return false;
    }
});

/**
 * GameAccount schema
 * @type {SimpleSchema}
 */
Like.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        objectType: {
            type: Number,
            label: "Like object type",
            min: Common.constants.schema.validate.like.minObjectType,
            max: Common.constants.schema.validate.like.maxObjectType,
            index: true
        },
        objectId: {
            type: String,
            label: "Like objectId",
            index: true
        },
        creator: {
            type: String,
            label: "Like creator",
            index: true
        }
    },
    Common.system.schemas.commonFields
));

GameAccount.attachSchema(GameAccount.schema);