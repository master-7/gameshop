Sequence = new Mongo.Collection("sequence");

/**
 * Allow actions
 */
Sequence.allow({
    insert: function (userId) {
        return false;
    },
    remove: function (userId, doc) {
        return false;
    },
    update: function (userId, doc) {
        return false;
    }
});