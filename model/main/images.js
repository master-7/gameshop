Images = new Mongo.Collection('images');

Thumbs = new Mongo.Collection('thumbs');

Images.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, image) {
        return userId && image.creator == userId;
    },
    update: function (userId, image) {
        return userId && image.creator == userId;
    }
});

Thumbs.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, image) {
        return userId && image.creator == userId;
    },
    update: function (userId, image) {
        return userId && image.creator == userId;
    }
});

let ThumbsStore = new UploadFS.store.GridFS({
    collection: Thumbs,
    name: 'thumbs',
    path: `${Common.constants.image.path}/thumbs`,
    mode: '0755',
    writeMode: '0755',
    transformWrite(from, to, fileId, file) {
        if(file.thumbnailBounds) {
            gm(from, file.name).
            crop(
                file.thumbnailBounds.widthAndHeight,
                file.thumbnailBounds.widthAndHeight,
                file.thumbnailBounds.left,
                file.thumbnailBounds.top
            ).
            resize(
                Common.constants.image.cropWidthHeight,
                Common.constants.image.cropWidthHeight,
                '!'
            ).
            stream().
            pipe(to);
        }
    }
});

let ImagesStore = new UploadFS.store.GridFS({
    collection: Images,
    name: 'images',
    path: `${Common.constants.image.path}/images`,
    mode: '0755',
    writeMode: '0755',
    filter: new UploadFS.Filter({
        minSize: 1,
        maxSize: Common.constants.image.maxImageSize.width * Common.constants.image.maxImageSize.height,
        contentTypes: ['image/*'],
        extensions: ['jpg', 'jpeg', 'png']
    }),
    copyTo: [
        ThumbsStore
    ]
});

/**
 * Converts DataURL to Blob object
 *
 * https://github.com/ebidel/filer.js/blob/master/src/filer.js#L137
 *
 * @param  {String} dataURL
 * @return {Blob}
 */
Images.dataURLToBlob = (dataURL) => {
    let BASE64_MARKER = ';base64,';

    if (dataURL.indexOf(BASE64_MARKER) === -1) {
        let parts = dataURL.split(',');
        let contentType = parts[0].split(':')[1];
        let raw = decodeURIComponent(parts[1]);

        return new Blob([raw], {type: contentType});
    }

    let parts = dataURL.split(BASE64_MARKER);
    let contentType = parts[0].split(':')[1];
    let raw = window.atob(parts[1]);
    let rawLength = raw.length;
    let uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: contentType});
};

/**
 * Converts Blob object to ArrayBuffer
 *
 * @param  {Blob}       blob          Source file
 * @param  {Function}   callback      Success callback with converted object as a first argument
 * @param  {Function}   errorCallback Error callback with error as a first argument
 */
Images.blobToArrayBuffer = (blob, callback, errorCallback) => {
    let reader = new FileReader();

    reader.onload = (e) => {
        callback(e.target.result);
    };

    reader.onerror = (e) => {
        if (errorCallback) {
            errorCallback(e);
        }
    };

    reader.readAsArrayBuffer(blob);
};

/**
 * Uploads a new file
 *
 * @param  {String} dataUrl   [description]
 * @param  {Object} fileMeta  [description]
 * @param  {Function}   resolve   [description]
 * @param  {Function}   reject    [description]
 */
Images.upload = (dataUrl, fileMeta, resolve, reject) => {
	// convert to Blob
	let blob = Images.dataURLToBlob(dataUrl);
	blob.name = fileMeta.name;

	// pick from an object only: name, type and size
	let file = _.pick(blob, 'name', 'type', 'size');
	file.thumbnailBounds = fileMeta.thumbnailBounds;

	// convert to ArrayBuffer
	Images.blobToArrayBuffer(blob, (data) => {
		let upload = new UploadFS.Uploader({
			data,
			file,
			store: ImagesStore,
			onError: reject,
			onComplete: resolve
		});

		upload.start();
	}, reject);
};
