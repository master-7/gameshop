/**
 * Support model
 */
Ticket = new Mongo.Collection("ticket");

/**
 * Allow actions rules
 */
Ticket.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, doc) {
        return false;
    },
    update: function (userId, doc) {
        if(!Common.system.permissions.isModerator(userId)) {
            return doc.creator == userId;
        }
        return true;
    }
});

/**
 * Ticket schema
 * @type {SimpleSchema}
 */
Ticket.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        subject: {
            type: String,
            label: "Ticket subject",
            max: Common.constants.schema.validate.ticket.subjectMax
        },
        description: {
            type: String,
            label: "Ticket description",
            max: Common.constants.schema.validate.ticket.descriptionMax
        },
        category: {
            type: Number,
            label: "Ticket category",
            min: Common.constants.schema.validate.ticket.categoryMin,
            max: Common.constants.schema.validate.ticket.categoryMax,
            index: true
        },
        status: {
            type: Number,
            label: "Ticket status",
            min: Common.constants.schema.validate.ticket.statusMin,
            max: Common.constants.schema.validate.ticket.statusMax,
            optional: true,
            index: true
        },
        images: {
            type: Array,
            label: "Ticket images"
        },
        'images.$': {
            type: Object,
            label: "Image data",
            blackbox: true
        },
        creator: {
            type: String,
            label: "Ticket creator id",
            optional: true,
            index: true
        },
        moderator: {
            type: Array,
            label: "Ticket moderator id Array",
            optional: true
        },
        "moderator.$": {
            type: String,
            optional: true
        }
    },
    Common.system.schemas.comments("comments", false),
    Common.system.schemas.commonFields
));

Ticket.attachSchema(Ticket.schema);