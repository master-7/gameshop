Icons = new Mongo.Collection('icons');

Icons.allow({
	insert: function (userId) {
		return Common.system.permissions.isSiteManager(userId);
	},
	remove: function (userId, image) {
		return Common.system.permissions.isSiteManager(userId);
	},
	update: function (userId, image) {
		return Common.system.permissions.isSiteManager(userId);
	}
});

IconsThumb = new Mongo.Collection('iconsThumb');

IconsThumb.allow({
	insert: function (userId) {
		return Common.system.permissions.isSiteManager(userId);
	},
	remove: function (userId, image) {
		return Common.system.permissions.isSiteManager(userId);
	},
	update: function (userId, image) {
		return Common.system.permissions.isSiteManager(userId);
	}
});

let IconsThumbStore = new UploadFS.store.GridFS({
	collection: IconsThumb,
	name: 'iconsThumb',
	path: `${Common.constants.image.path}/iconsThumb`,
	mode: '0755',
	writeMode: '0755',
	transformWrite(from, to, fileId, file) {
		if(file.thumbnailBounds) {
			gm(from, file.name).
			crop(
				file.thumbnailBounds.widthAndHeight,
				file.thumbnailBounds.widthAndHeight,
				file.thumbnailBounds.left,
				file.thumbnailBounds.top
			).
			resize(64, 64).
			stream().
			pipe(to);
		}
	}
});

let IconsStore = new UploadFS.store.GridFS({
	collection: Icons,
	name: 'icons',
	path: `${Common.constants.image.path}/icons`,
	mode: '0755',
	writeMode: '0755',
	filter: new UploadFS.Filter({
		minSize: 1,
		maxSize: 1024 * 1000,
		contentTypes: ['image/*'],
		extensions: ['jpg', 'png']
	}),
	copyTo: [
		IconsThumbStore
	]
});

/**
 * Uploads a new file
 *
 * @param  {String} dataUrl   [description]
 * @param  {Object} fileMeta  [description]
 * @param  {Function}   resolve   [description]
 * @param  {Function}   reject    [description]
 */
Icons.upload = (dataUrl, fileMeta, resolve, reject) => {
	// convert to Blob
	let blob = Images.dataURLToBlob(dataUrl);
	blob.name = fileMeta.name;

	// pick from an object only: name, type and size
	let file = _.pick(blob, 'name', 'type', 'size');
	file.thumbnailBounds = fileMeta.thumbnailBounds;

	// convert to ArrayBuffer
	Images.blobToArrayBuffer(blob, (data) => {
		let upload = new UploadFS.Uploader({
			data,
			file,
			store: IconsStore,
			onError: reject,
			onComplete: resolve
		});

		upload.start();
	}, reject);
};