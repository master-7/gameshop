/**
 * This model is a fixed user events
 */
UserTracker = new Mongo.Collection("userTracker");

/**
 * Allow actions rules
 */
UserTracker.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, doc) {
        return false;
    },
    update: function (userId, doc) {
        return userId && doc.creator == userId;
    }
});