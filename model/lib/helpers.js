/**
 * Server helpers
 */
Common.helpers = {
    /**
     * Calc commission sum
     * @param sum
     * @returns {number}
     */
    calcCommission: (sum) => {
        let commission = sum * Common.constants.payment.commission;
        if(commission < Common.constants.payment.minCommission)
            return Common.constants.payment.minCommission;
        return commission;
    }
};

/**
 * Calc the commission
 * @param sum
 * @returns {number}
 */
Common.helpers.calcPriceWithCommission = (sum) => {
    return (sum + Common.helpers.calcCommission(sum));
};

/**
 * Generate random string function
 * @param mask
 * @returns {*|string}
 */
Common.helpers.generateRandomString = (mask) => {
    let maskString = mask || 'xxxxxxxx-xxxx-xxxy-yxxx-xxxxxxxxxxxx';
    maskString.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
    return maskString;
};

/**
 * @param image
 * @returns {string}
 */
Common.helpers.getImageLink = (image) => {
    return `ufs/${image.store}/${image._id}/${image.name}`;
};
