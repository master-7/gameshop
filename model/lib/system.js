/**
 * Common system lib
 */
Common.system = {};

/**
 * Run user function on document ready
 * @param func
 */
Common.system.runOnReady = (func) => {
    if (Meteor.isCordova)
        angular.element(document).on('deviceready', func);
    else
        angular.element(document).ready(func);
};

/**
 * Check user permissions
 */
Common.system.permissions = {
    isAdmin: (userId) => {
        return Roles.userIsInRole(
            userId,
            Common.constants.roles.types.admin,
            Roles.GLOBAL_GROUP
        );
    },
    isMoneyAdmin: (userId) => {
        return Roles.userIsInRole(
            userId,
            [
                Common.constants.roles.types.admin,
                Common.constants.roles.types.moneyAdmin
            ],
            Roles.GLOBAL_GROUP
        );
    },
    isSiteManager: (userId) => {
        return Roles.userIsInRole(
            userId,
            [
                Common.constants.roles.types.admin,
                Common.constants.roles.types.moneyAdmin,
                Common.constants.roles.types.manager
            ],
            Roles.GLOBAL_GROUP
        );
    },
    isModerator: (userId) => {
        return Roles.userIsInRole(
            userId,
            [
                Common.constants.roles.types.admin,
                Common.constants.roles.types.moneyAdmin,
                Common.constants.roles.types.manager,
                Common.constants.roles.types.moderator
            ],
            Roles.GLOBAL_GROUP
        );
    },
    isUser: (userId) => {
        return Roles.userIsInRole(
            userId,
            Common.constants.roles.types.user,
            Roles.GLOBAL_GROUP
        );
    }
};

/**
 * Check count element on collection
 * @param collection
 * @returns {*}
 */
Common.system.countElementOnCollection = (collection) => {
    if(Array.isArray(collection) && collection.length > 0)
        return collection.length;
    else {
        let result = false;
        try {
            result = collection.fetch().length;
        }
        catch (e) {
            result = false;
        }
        finally {
            return result;
        }
    }
};

Common.system.schemas = {
    /**
     * Primary key
     */
    primaryKey: {
        _id: {
            type: String,
            optional: true
        }
    },
    /**
     * Schema common fields
     */
    commonFields: {
        createdAt: {
            type: Number,
            label: "Date of creation object",
            autoValue: function() {
                if (this.isInsert) {
                    return Date.now();
                } else if (this.isUpsert) {
                    return {$setOnInsert: Date.now()};
                } else {
                    this.unset();
                }
            },
            index: true
        },
        modifiedAt: {
            type: Number,
            label: "Date of update object",
            autoValue: function() {
                return Date.now();
            }
        }
    }
};

/**
 * Get base scheme comments
 * @param name
 * @param required
 * @returns {*}
 */
Common.system.schemas.comments = (name, required = true) => {
    let baseComments = {};

    baseComments[name] = {
        type: Array
    };

    if(!required)
        baseComments[name].optional = true;

    baseComments[`${name}.$`] = {
        type: Object
    };

    baseComments[`${name}.$._id`] = {
        type: Meteor.Collection.ObjectID,
        optional: true
    };

    baseComments[`${name}.$.msg`] = {
        type: String,
        max: Common.constants.schema.validate.comment.msgMaxLength
    };

    baseComments[`${name}.$.createdAt`] = {
        type: Date,
        optional: true
    };

    baseComments[`${name}.$.author`] = {
        type: String
    };

    baseComments[`${name}.$.block`] = {
        type: Boolean,
        optional: true
    };

    return baseComments;
};
