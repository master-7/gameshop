/**
 * Game constants
 */
Common.constants.game = {
    statuses: {
        onVerification: 0,
        isPublic: 1,
        selling: 2,
        sold: 3,
        debatable: 4,
        failed: 5,
        moderatorIsWorking: 6,
        verificationFail: 7,
        isBlocked: 8,
        gameReturned: 9
    },
    sellingMode: {
        toMediate: 0,
        preApproval: 1
    },
    adsType: {
        free: 0,
        color: 1,
        top: 2
    }
};