/**
 * Server constants
 */
Common.constants = {
    image: {
        type: {
            images: "images",
            thumbs: "thumbs",
            icons: "icons",
            iconsThumbs: "iconsThumbs"
        },
        path: "/home/uploads",
        originalMinWidthAndHeight: 300,
        cropWidthHeight: 300,
        maxCanvasHeight: 300,
        maxImageSize: {
            width: 4096,
            height: 3112
        },
        scaleStep: 0.025
    },
    roles: {
        types: {
            admin: "admin",
            moneyAdmin: "moneyAdmin",
            manager: "manager",
            moderator: "moderator",
            user: "user"
        }
    },
    /**
     * Payments constants
     */
    payment: {
        commission: 0.1,
        minCommission: 100,
        payAd: {
            color: 10,
            top: 20
        },
        transactionsStatus: {
            fail: 0,
            createdRequest: 1,
            inProcess: 2,
            success: 3
        },
        /**
         * Yandex constant
         */
        yandex: {
            status: {
                success: "success",
                refused: "refused"
            },
            errors: {
                notEnoughMoney: "not_enough_funds",
                limitExceeded: "limit_exceeded",
                accountBlocked: "account_blocked",
                extActionRequired: "ext_action_required"
            }
        },
        card: {
            status: {
                authRequired: "ext_auth_required"
            }
        }
    },
    /**
     * User event tracker constants
     */
    userTracker: {
        type: {
            moderation: {
                createdGame: 0,
                success: 1,
                returnGame: 2,
                fail: 3,
                message: 4
            },
            ticket: {
                message: 5
            },
            deal: {
                created: 6,
                message: 7
            }
        },
        status: {
            close: 0,
            created: 1,
            viewed: 2
        }
    },
    complaint: {
        objectType: {
            game: 1,
            thing: 2,
            comment: 3,
            commentType: {
                game: 1,
                thing: 2,
                ticket: 3
            }
        },
        type: {
            spam: 1,
            insult: 2,
            adultContent: 3,
            drugPromotion: 4,
            childPornography: 5,
            extremistViolence: 6
        },
        status: {
            created: 1,
            accepted: 2,
            rejected: 3
        }
    },
    thing: {
        types: {
            steam: 1,
            origin: 2,
            "mail.ru": 3,
            gamenet: 4,
            lineage2: 8,
            aion: 9,
            tera: 10,
            blackDesert: 11,
            neverwinter: 12,
            pathOfExile: 16,
            diablo3: 17,
            browserGames: 19,
            other: 20
        },
        statuses: {
            onVerification: 0,
            isPublic: 1,
            selling: 2,
            sold: 3,
            debatable: 4,
            failed: 5,
            isBlock: 6
        }
    },
    wallet: {
        /**
         * Wallet types
         */
        types: {
            yandexMoney: 1,
            robokassa: 2,
            card: 3,
            getTypeById: {
                1: "yandexMoney",
                2: "robokassa",
                3: "card"
            }
        }
    },
    payOutBid: {
        /**
         * Payout bid statuses
         */
        statuses: {
            hasCreated: 0,
            inProcess: 1,
            success: 2,
            verificationFail: 3
        }
    },
    like: {
        type: {
            game: 1,
            thing: 2,
            team: 3
        },
        getObjectNameByType: {
            1: "GameAccount",
            2: "GameThing"
        },
        iconType: {
            up: "ic_thumb_up_24px",
            down: "ic_thumb_down_24px"
        }
    },
    ticket: {
        category: {
            partnership: 1,
            technicalProblem: 10,
            offer: 20,
            money: 30,
            game: 40,
            other: 50
        },
        status: {
            close: 0,
            created: 1,
            moderatorAnswer: 2,
            userAnswer: 3
        }
    },
    deal: {
        status: {
            created: 0,
            inProcess: 1,
            success: 2,
            fail: 3
        }
    },
    errorCode: {
        http: {
            badRequest: 400,
            unauthorized: 401,
            paymentRequired: 402,
            forbidden: 403,
            notFound: 404,
            methodNotAllowed: 405,
            notAcceptable: 406,
            proxyAuthenticationRequired: 407,
            requestTimeout: 408,
            conflict: 409,
            gone: 410,
            lengthRequired: 411,
            preconditionFailed: 412,
            requestEntityTooLarge: 413,
            requestURITooLarge: 414,
            unsupportedMediaType: 415,
            requestedRangeNotSatisfiable: 416,
            expectationFailed: 417,
            unprocessableEntity: 422,
            locked: 423,
            failedDependency: 424,
            unorderedCollection: 425,
            upgradeRequired: 426,
            preconditionRequired: 428,
            tooManyRequests: 429,
            requestHeaderFieldsTooLarge: 431,
            requestedHostUnavailable: 434,
            closeConnect: 444,
            retryWith: 449,
            unavailableForLegalReasons: 451,
            internalServerError: 500,
            notImplemented: 501,
            badGateway: 502,
            serviceUnavailable: 503,
            gatewayTimeout: 504,
            HTTPVersionNotSupported: 505,
            variantAlsoNegotiates: 506,
            insufficientStorage: 507,
            loopDetected: 508,
            bandwidthLimitExceeded: 509,
            notExtended: 510,
            networkAuthenticationRequired: 511
        },
        unknown: 1000,
        loginForm: {
            userNotFound: "User not found",
            userIsBaned: "User is baned",
            userNotConfirmedEmail: "User not confirmed email",
            incorrectPassword: "Incorrect password"
        },
        registration: {
            loginExist: "Username already exists."
        }
    },
    loadParties: {
        countItems: 5
    }
};
