/**
 * Common collections and methods
 */
Common = new Mongo.Collection("common");

Common.allow({
    insert: function (userId) {
        return false;
    },
    remove: function (userId, account) {
        return false;
    },
    update: function (userId, account) {
        return false;
    }
});