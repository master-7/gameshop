/**
 * Schema constant
 */
Common.constants.schema = {
    validate: {
        game: {
            nameMax: 50,
            descriptionMax: 1000,
            priceMax: 10000000,
            statusMin: Common.constants.game.statuses.onVerification,
            statusMax: Common.constants.game.statuses.gameReturned,
            adsTypeMin: Common.constants.game.adsType.free,
            adsTypeMax: Common.constants.game.adsType.top,
            sellingModeMin: Common.constants.game.sellingMode.toMediate,
            sellingModeMax: Common.constants.game.sellingMode.preApproval,
            gameAccount: {
                loginMin: 3,
                loginMax: 50,
                passMin: 6,
                passMax: 20
            },
            dependentsAccount: {
                passMin: 6,
                passMax: 20
            }
        },
        thing: {
            nameMax:25,
            descriptionMax: 255,
            priceMax: 1000000,
            statusMin: Common.constants.thing.statuses.isPublic,
            statusMax: Common.constants.thing.statuses.failed
        },
        comment: {
            msgMaxLength: 255
        },
        complaint: {
            minStatus: Common.constants.complaint.status.created,
            maxStatus: Common.constants.complaint.status.rejected,
            minType: Common.constants.complaint.type.spam,
            maxType: Common.constants.complaint.type.extremistViolence,
            minObjectType: Common.constants.complaint.objectType.game,
            maxObjectType: Common.constants.complaint.objectType.comment
        },
        payOutBid: {
            minSum: 100,
            walletNumber: {
                maxLength: 25
            }
        },
        like: {
            minObjectType: Common.constants.like.type.game,
            maxObjectType: Common.constants.like.type.thing
        },
        ticket: {
            subjectMax: 128,
            descriptionMax: 500,
            categoryMin: Common.constants.ticket.category.partnership,
            categoryMax: Common.constants.ticket.category.other,
            statusMin: Common.constants.ticket.status.close,
            statusMax: Common.constants.ticket.status.userAnswer
        }
    },
    errorTypes: {
        string: {
            min: "minString",
            max: "maxString"
        },
        number: {
            min: "minNumber",
            max: "maxNumber"
        },
        regexp: "regEx",
        required: "required",
        login: {
            username: {
                notFound: "notFound",
                userBan: "userBan",
                userNotConfirmedEmail: "userNotConfirmedEmail"
            },
            password: {
                notCorrect: "notCorrect"
            }
        },
        registration: {
            username: {
                userExist: "userExist"
            }
        }
    }
};