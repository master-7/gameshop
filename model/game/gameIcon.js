GameIcon = new Mongo.Collection("gameIcon");

/**
 * Allow actions rules
 */
GameIcon.allow({
    insert: function (userId) {
        return false;
    },
    remove: function (userId, account) {
        return false;
    },
    update: function (userId, account) {
        return false;
    }
});

/**
 * GameIcon schema
 * @type {SimpleSchema}
 */
GameIcon.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        index: {
            type: Number,
            label: "Index category",
            index: true
        },
        icon: {
            type: String,
            label: "Category image"
        }
    }
));

GameIcon.attachSchema(GameIcon.schema);