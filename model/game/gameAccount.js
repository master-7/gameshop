GameAccount = new Mongo.Collection("gameAccount");

/**
 * Allow actions rules
 */
GameAccount.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, account) {
        return false;
    },
    update: function (userId, account) {
        if(!Common.system.permissions.isModerator(userId)) {
            let isCreator = userId && account.creator == userId;
            if(isCreator) {
                if(account.status == Common.constants.game.statuses.onVerification)
                    return true;
            }
            return false;
        }
        return true;
    }
});

/**
 * GameAccount schema
 * @type {SimpleSchema}
 */
GameAccount.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        name: {
            type: String,
            label: "Game name",
            max: Common.constants.schema.validate.game.nameMax
        },
        description: {
            type: String,
            label: "Game description",
            max: Common.constants.schema.validate.game.descriptionMax
        },
        category: {
            type: Object,
            label: "Game category"
        },
        "category._id": {
            type: String,
            label: "Category id",
            index: true
        },
        "category.name": {
            type: String,
            label: "Category name"
        },
        "category.image": {
            type: Object,
            label: "Category image",
            blackbox: true
        },
        type: {
            type: String,
            label: "Game type",
            optional: true
        },
        price: {
            type: Number,
            label: "Game price",
            max: Common.constants.schema.validate.game.priceMax,
            decimal: true,
            index: true
        },
        sellingMode: {
            type: Number,
            label: "Game selling mode type",
            min: Common.constants.schema.validate.game.sellingModeMin,
            max: Common.constants.schema.validate.game.sellingModeMax,
            optional: true,
            index: true
        },
        adsType: {
            type: Number,
            label: "Game flag paid ad",
            min: Common.constants.schema.validate.game.adsTypeMin,
            max: Common.constants.schema.validate.game.adsTypeMax,
            optional: true,
            index: true
        },
        images: {
            type: Array,
            label: "Game images"
        },
        'images.$': {
            type: Object,
            label: "Image data",
            blackbox: true
        },
        videos: {
            type: Array,
            label: "Video info",
            optional: true
        },
        'videos.$': {
            type: String,
            label: "Game video item _id"
        },
        gameAccount: {
            type: Object,
            label: "Game account data for login",
            optional: true
        },
        "gameAccount.login": {
            type: String,
            min: Common.constants.schema.validate.game.gameAccount.loginMin,
            max: Common.constants.schema.validate.game.gameAccount.loginMax,
            optional: true
        },
        "gameAccount.pass": {
            type: String,
            optional: true
        },
        dependentAccount: {
            type: Object,
            label: "Account (email) data for login",
            optional: true
        },
        "dependentAccount.login": {
            type: String,
            regEx: SimpleSchema.RegEx.Email,
            optional: true
        },
        "dependentAccount.pass": {
            type: String,
            optional: true
        },
        commission: {
            type: Number,
            label: "Game commission",
            decimal: true,
            optional: true
        },
        status: {
            type: Number,
            label: "Game status",
            min: Common.constants.schema.validate.game.statusMin,
            max: Common.constants.schema.validate.game.statusMax,
            optional: true,
            index: true
        },
        transaction_id: {
            type: String,
            label: "Game account transaction",
            optional: true
        },
        creator: {
            type: String,
            label: "Game account creator id",
            optional: true,
            index: true
        },
        moderator: {
            type: String,
            label: "Game account moderator id",
            optional: true,
            index: true
        },
        buyer: {
            type: String,
            label: "Game account buyer id",
            optional: true,
            index: true
        }
    },
    Common.system.schemas.comments("comments", false),
    Common.system.schemas.comments("moderatorComments", false),
    Common.system.schemas.comments("chatSelling", false),
    Common.system.schemas.comments("chatSellingBuyerManager", false),
    Common.system.schemas.comments("chatSellingSellerManager", false),
    Common.system.schemas.commonFields
));

GameAccount.attachSchema(GameAccount.schema);