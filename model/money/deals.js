Deals = new Mongo.Collection("deals");

/**
 * Allow actions rules
 */
Deals.allow({
    insert: function (userId) {
        return false;
    },
    remove: function (userId, deal) {
        return false;
    },
    update: function (userId, deal) {
        return Common.system.permissions.isModerator(userId);
    }
});

/**
 * Deals type const
 * @type {{game: number, thing: number}}
 */
Deals.types = {
    game: 1000,
    thing: 2000,
    gameWithMediator: 3000
};