FreeMoney = new Mongo.Collection("freeMoney");

/**
 * Account status
 * @type {{block: number, active: number}}
 */
FreeMoney.status = {
	withdrawn: 0,
	active: 1
};

/**
 * Allow actions rules
 */
FreeMoney.allow({
	insert: function (userId) {
		return false;
	},
	remove: function (userId, account) {
		return false;
	},
	update: function (userId, account) {
		return Common.system.permissions.isMoneyAdmin(userId);
	}
});

/**
 * @type {SimpleSchema}
 */
FreeMoney.schema = new SimpleSchema(Object.assign({},
	Common.system.schemas.primaryKey,
	{
		transactionId: {
			type: String,
			label: "Free money transaction id"
		},
		sum: {
			type: Number,
			label: "Free money sum",
			decimal: true
		},
		status: {
			type: Number,
			label: "Status commission",
			optional: true
		}
	},
	Common.system.schemas.commonFields
));

FreeMoney.attachSchema(FreeMoney.schema);