PayOutBid = new Mongo.Collection("payOutBid");

PayOutBid.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        sum: {
            type: Number,
            label: "Money out sum",
            decimal: true,
            min: Common.constants.schema.validate.payOutBid.minSum
        },
        accountType: {
            type: Number,
            label: "Destination account's type",
            min: Common.constants.wallet.types.yandexMoney,
            max: Common.constants.wallet.types.card,
            index: true
        },
        accountNumber: {
            type: String,
            label: "Destination account's number",
            index: true
        },
        status: {
            type: Number,
            label: "Bid's status",
            min: Common.constants.payOutBid.statuses.hasCreated,
            max: Common.constants.payOutBid.statuses.verificationFail,
            optional: true,
            index: true
        },
        creator: {
            type: String,
            label: "Bid creator id",
            optional: true,
            index: true
        }
    },
    Common.system.schemas.commonFields
));

PayOutBid.attachSchema(PayOutBid.schema);