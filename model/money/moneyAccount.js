MoneyAccount = new Mongo.Collection("moneyAccount");

/**
 * Allow actions rules
 */
MoneyAccount.allow({
    insert: function (userId) {
        return false;
    },
    remove: function (userId, account) {
        return false;
    },
    update: function (userId, account) {
        return false;
    }
});

/**
 * MoneyAccount schema
 * @type {SimpleSchema}
 */
MoneyAccount.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        owner: {
            type: String,
            label: "Money account owner",
            unique: true,
            optional: true
        },
        sum: {
            type: Number,
            label: "Money account sum",
            decimal: true,
            optional: true
        },
        status: {
            type: Number,
            label: "Money account status",
            optional: true,
            index: true
        }
    },
    Common.system.schemas.commonFields
));

MoneyAccount.attachSchema(MoneyAccount.schema);