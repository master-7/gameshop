Wallets = new Mongo.Collection("wallets");

/**
 * Allow actions rules
 */
Wallets.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, bid) {
        return userId == bid.owner;
    },
    update: function (userId, bid) {
        return false;
    }
});

Wallets.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        accountType: {
            type: Number,
            label: "Destination account's type",
            min: Common.constants.wallet.types.yandexMoney,
            max: Common.constants.wallet.types.card
        },
        accountNumber: {
            type: String,
            label: "Destination account's number"
        },
        owner: {
            type: String,
            label: "Wallet's owner id",
            index: true,
            optional: true
        }
    },
    Common.system.schemas.commonFields
));

Wallets.attachSchema(Wallets.schema);