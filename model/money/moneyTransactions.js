MoneyTransactions = new Mongo.Collection("moneyTransactions");

/**
 * Allow actions rules
 */
MoneyTransactions.allow({
    insert: function (userId) {
        return false;
    },
    remove: function (userId, account) {
        return false;
    },
    update: function (userId, account) {
        return false;
    }
});

/**
 * Model enums
 * Money transactions statuses
 * @type {{fail: number, createdRequest: number, inProcess: number, success: number}}
 */
MoneyTransactions.statuses = {
    fail: 0,
    createdRequest: 1,
    inProcess: 2,
    success: 3
};

/**
 * Money transactions types
 * @type {{yandexPayIn: number}}
 */
MoneyTransactions.types = {
    payIn: {
        yandex: 100,
        robokassa: 200,
        card: 300,
        test: {
            100: "yandex",
            200: "robokassa",
            300: "card"
        }
    },
    deal: {
        game: 1000,
        thing: 2000,
        gameWithMediator: 3000,
        test: {
            1000: "game",
            2000: "thing",
            3000: "gameWithMediator"
        }
    },
    debit: {
        gamePayAds: 10000,
        test: {
            10000: "gamePayAds"
        }
    }
};

/**
 * Money transactions global types
 * @type {{yandexPayIn: number}}
 */
MoneyTransactions.globalTypes = {
    payIn: 1,
    buy: 2,
    payout: 3
};
