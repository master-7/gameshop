CategoryKeywords = new Mongo.Collection("categoryKeywords");

/**
 * Allow actions rules
 */
CategoryKeywords.allow({
	insert: function (userId) {
		return Common.system.permissions.isSiteManager(userId);
	},
	remove: function (userId, deal) {
		return Common.system.permissions.isSiteManager(userId);
	},
	update: function (userId, deal) {
		return Common.system.permissions.isSiteManager(userId);
	}
});

/**
 * Category keywords schema
 * @type {SimpleSchema}
 */
CategoryKeywords.schema = new SimpleSchema(Object.assign({},
	Common.system.schemas.primaryKey,
	{
		image: {
			type: Object,
			label: "Image data",
			blackbox: true
		},
		name: {
			type: String,
			label: "Search tag",
			index: true,
			unique: true
		},
		keywords: {
			type: String,
			label: "Keywords header",
			optional: true
		},
		metaTags: {
			type: String,
			label: "Page metatags",
			optional: true
		},
		isActive: {
			type: Boolean,
			label: "Activity flag"
		}
	},
	Common.system.schemas.commonFields
));

CategoryKeywords.attachSchema(CategoryKeywords.schema);