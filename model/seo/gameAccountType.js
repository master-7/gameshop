GameAccountType = new Mongo.Collection("gameAccountType");

/**
 * Allow actions rules
 */
GameAccountType.allow({
	insert: function (userId) {
		return Common.system.permissions.isSiteManager(userId);
	},
	remove: function (userId, doc) {
		return Common.system.permissions.isSiteManager(userId);
	},
	update: function (userId, doc) {
		return Common.system.permissions.isSiteManager(userId);
	}
});

/**
 * Tags keywords schema
 * @type {SimpleSchema}
 */
GameAccountType.schema = new SimpleSchema(Object.assign({},
	Common.system.schemas.primaryKey,
	{
		image: {
			type: Object,
			label: "Image data",
			blackbox: true
		},
		name: {
			type: String,
			label: "Game name",
			index: true,
			unique: true
		},
		keywords: {
			type: String,
			label: "Keywords header",
			optional: true
		},
		metaTags: {
			type: String,
			label: "Page metatags",
			optional: true
		},
		isActive: {
			type: Boolean,
			label: "Activity flag"
		}
	},
	Common.system.schemas.commonFields
));

GameAccountType.attachSchema(GameAccountType.schema);