GameThing = new Mongo.Collection("gameThing");

/**
 * Allow actions rules
 */
GameThing.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId, thing) {
        return userId && thing.creator == userId;
    },
    update: function (userId, thing) {
        return userId && thing.creator == userId;
    }
});

/**
 * GameThing schema
 * @type {SimpleSchema}
 */
GameThing.schema = new SimpleSchema(Object.assign({},
    Common.system.schemas.primaryKey,
    {
        name: {
            type: String,
            label: "Thing name",
            max: Common.constants.schema.validate.thing.nameMax
        },
        description: {
            type: String,
            label: "Thing description",
            max: Common.constants.schema.validate.thing.descriptionMax
        },
        category: {
            type: Number,
            label: "Thing category",
            index: true
        },
        price: {
            type: Number,
            label: "Thing price",
            decimal: true,
            max: Common.constants.schema.validate.thing.priceMax
        },
        images: {
            type: Array,
            label: "Thing images"
        },
        'images.$': {
            type: String,
            label: "Thing image item _id"
        },
        commission: {
            type: Number,
            label: "Thing commission",
            decimal: true,
            optional: true
        },
        status: {
            type: Number,
            label: "Thing status",
            min: Common.constants.schema.validate.thing.statusMin,
            max: Common.constants.schema.validate.thing.statusMax,
            optional: true,
            index: true
        },
        transaction_id: {
            type: String,
            label: "Thing transaction",
            optional: true
        },
        creator: {
            type: String,
            label: "Thing creator id",
            optional: true,
            index: true
        },
        buyer: {
            type: String,
            label: "Thing creator id",
            optional: true,
            index: true
        }
    },
    Common.system.schemas.comments("comments", false),
    Common.system.schemas.commonFields
));

GameThing.attachSchema(GameThing.schema);
