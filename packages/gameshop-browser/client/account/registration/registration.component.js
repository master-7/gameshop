angular.module('gameshop.browser').directive('registration', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/gameshop-browser/client/account/registration/registration.html',
        controllerAs: 'registration',
        controller: function ($scope, $state, library) {

            /**
             * Registration data
             * @type {{email: string, password: string, profile: {name: string, surname: string}}}
             */
            this.credentials = {
                email: '',
                password: '',
                profile: {
                    name: '',
                    surname: ''
                }
            };

            /**
             * Registration action
             */
            this.registration = () => {
                Accounts.createUser(this.credentials, (err) => {
                    if(err) {
                        let formError = [];
                        if(err.error && err.error == Common.constants.errorCode.http.forbidden) {
                            switch (err.reason) {
                                case Common.constants.errorCode.registration.loginExist:
                                    formError.push({
                                        name: "username",
                                        type: Common.constants.schema.errorTypes.registration.username.userExist
                                    });
                                    break;
                            }
                            if(formError) {
                                library.system.error.formValidate(
                                    $scope.userForm,
                                    formError
                                )
                            }
                        }
                        library.system.error.handlers(err);
                        return false;
                    }
                    else {
                        $state.go('public');
                    }
                });
            }
        }
    }
});