let {Component, View, SetModule, Inject, MeteorReactive, LocalInjectables} = angular2now;

SetModule('gameshop.browser');

@Component({selector: 'login'})
@View({
    templateUrl: () => {
        if (Meteor.isCordova) {
            return "";
        }
        return '/packages/gameshop-browser/client/account/login/login.html';
    }
})
@Inject(['$scope', '$state', '$window', 'library'])
@MeteorReactive
@LocalInjectables
class login {
    constructor() {
        this.credentials = {
            email: '',
            password: ''
        };
    }

    /**
     * Login with username and password
     */
    login() {
        Meteor.loginWithPassword({username: this.credentials.email}, this.credentials.password, (err) => {
            if (err) {
                let formError = [];
                if(err.error && err.error == Common.constants.errorCode.http.forbidden) {
                    switch (err.reason) {
                        case Common.constants.errorCode.loginForm.userNotFound:
                            formError.push({
                                name: "username",
                                type: Common.constants.schema.errorTypes.login.username.notFound
                            });
                            break;
                        case Common.constants.errorCode.loginForm.userIsBaned:
                            this.library.system.dialog(
                                '/packages/gameshop-browser/client/main/dialog/accountIsBlocked.html'
                            );
                            formError.push({
                                name: "username",
                                type: Common.constants.schema.errorTypes.login.username.userBan
                            });
                            break;
                        case Common.constants.errorCode.loginForm.userNotConfirmedEmail:
                            this.library.system.toast('/packages/gameshop-browser/client/main/toast/userNotConfirmedEmail.toast.html');
                            formError.push({
                                name: "username",
                                type: Common.constants.schema.errorTypes.login.username.userNotConfirmedEmail
                            });
                            break;
                        case Common.constants.errorCode.loginForm.incorrectPassword:
                            formError.push({
                                name: "password",
                                type: Common.constants.schema.errorTypes.login.password.notCorrect
                            });
                            break;
                    }
                    if(formError) {
                        this.library.system.error.formValidate(
                            this.$scope.entranceForm,
                            formError
                        )
                    }
                }
                else {
                    this.library.system.error.handlers(err);
                }
            }
            else {
                this.$window.history.back();
            }
        });
    };
}