let {Component, View, SetModule, Inject, MeteorReactive, LocalInjectables} = angular2now;

SetModule('gameshop.browser');

@Component({selector: 'social-auth', controllerAs: 'vm'})
@View({
    templateUrl: () => {
        if (Meteor.isCordova) {
            return "";
        }
        return '/packages/gameshop-browser/client/account/social-auth/socialAuth.html';
    }
})
@Inject('$state', '$window', 'library')
@MeteorReactive
@LocalInjectables
class socialAuth {
    constructor() {
        /**
         * Login error flag
         * @type {boolean}
         */
        this.error = false;
        /**
         * Flag query process
         * @type {boolean}
         */
        this.queryProcess = false;
    }

    /**
     * Event handlers login with external service
     */

    loginWithVK() {
        this.queryProcess = true;
        Meteor.loginWithVk({
            requestPermissions: ['email']
        }, (err) => {
            this.queryProcess = false;
            if (err) {
                this.error = true;
                this.library.system.error.handlers(err);
                throw new Meteor.Error("Logout failed");
            }
            else {
                this.$window.history.back();
            }
        })
    };

    loginWithFacebook() {
        this.queryProcess = true;
        Meteor.loginWithFacebook({
            requestPermissions: ['email']
        }, (err) => {
            this.queryProcess = false;
            if (err) {
                this.error = true;
                this.library.system.error.handlers(err);
                throw new Meteor.Error("Logout failed");
            }
            else {
                this.$window.history.back();
            }
        })
    };

    loginWithGoogle() {
        this.queryProcess = true;
        Meteor.loginWithGoogle({
            requestPermissions: ['email'],
            redirectUri: "http://salegame.net"
        }, (err) => {
            this.queryProcess = false;
            if (err) {
                this.error = true;
                this.library.system.error.handlers(err);
                throw new Meteor.Error("Logout failed");
            }
            else {
                this.$window.history.back();
            }
        })
    };
}